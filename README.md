![](ressources/logo_geo16crea.png)

# Mise en place de la gestion des espaces verts

<img src="ressources/illustration_1.jpeg" width="844" height="562.5" />

Ce projet recense l'ensemble des fichiers permettant la mise en place d'une base de données géographique de gestion des espaces verts.

Ce modèle est mis en oeuvre par l'ATD16 dans le cadre d'une utilisation sur le logiciel X'MAP développé par la société SIRAP. Il s'implémente dans un serveur de base de base de données PostgreSQL/PostGIS. 
Néanmoins il peut être adapté pour un usage sur un autre outil.

Il est basé sur 3 axes : l'inventaire des espaces verts, le recensement de leurs entretiens (et leurs fréquences), et la gestion des interventions (leurs dates et durées). À cela, s'ajoute une petite gestion des contrats d'entretien et le recensement des plantes mellifères.

Il est librement inspiré du [modèle de données de l'agglomération de Compiègne](https://github.com/sigagglocompiegne/espace_vert).

# Principe de fonctionnement

Des tables géographiques permettent d'**inventorier les éléments d'espaces verts**.
- zone de gestion : élément surfacique représentant les sites cohérents de gestion (stade, parc, jardin, quartier, etc.) comprenant un ou plusieurs éléments d'espaces verts parmi les suivants.
- zone boisée, alignement d'arbres, arbre isolé : élément surfacique, linéaire ou ponctuel représentant les arbres.
- massif arbustif, haie, arbuste isolé : élément surfacique, linéaire ou ponctuel représentant les arbustes.
- massif fleuri, fleuri isolé : élément surfacique ou ponctuel représentant les fleuris.
- espace enherbé : élément surfacique représentant les espaces enherbés.
- étendue d'eau : élément surfacique ou ponctuel représentant les étendues d'eau.
- cours d'eau : élément linéaire représentant les cours d'eau.
- arrivée d'eau : élément ponctuel représentant les robinets, fontaines et autres.
- espace de loisirs, loisir isolé : élément surfacique ou ponctuel représentant les loisirs.
- clôture : élément linéaire représentant les séparations physiques artificiels d'espaces.
- zone de circultaion : élément surfacique représentant les parkings et places.
- voie de circultation : élément linéaire représentant les voies de circulation telles que chemins, pistes, allées, etc.

Le **recensement des plantes mellifères** est fait sur chaque couche végétale par une case à cocher. Cela permet de dire si l'ensemble végétal produit une bonne quantité de nectar et pollen de façons à attirer les insectes pollinisateurs et particulièrement les abeilles. De même, une couche répertorie les mobiliers faunistiques et notamment les ruches et dorlotoires.

Ces couches d'inventaire sont liées automatiquement à une zone de gestion par intersection géographique.

Des tables non géographiques permettent de **recenser les entretiens et les interventions** sur les zones de gestion.
- entretien : liste les informations d'entretiens (type, fréquence, durée estimée, etc.) sur une zone de gestion
- intervention : liste les interventions sur une zone de gestion (type d'entretien, dates d'intervention, durée, etc.)

À partir des valeurs renseignées dans les entretiens et interventions, des informations se compilent automatiquement dans la couche des zones de gestion. On en déduit :
- la durée d'entretien planifiée annuelle estimée par zone de gestion
- la durée d'intervention totale annuelle par zone de gestion
- le nombre d'intervention par an par zone de gestion

# Génèse : quelques dates

* 2019 : mise en production d'une pseudo gestion des entretiens des espaces verts basée sur une seule couche géographique.
* mai 2021-octobre 2021 : développement du nouveau modèle de donnée (présenté ici) pour répondre aux besoins des collectivités.
* décembre 2021 : phase de bêta test pour deux communes charentaises.
* novembre 2021-février 2022 : correction et amélioration du modèle.

# Ressources du projet

- MCD [[pdf](donnees/bdd/xmap/mcd/mcd_espaces_verts.drawio.pdf)]
- Schéma applicatif [[pdf](ressources/schema_applicatif_espaces_verts.drawio.pdf)]
<br/>

/!\ le schéma de bdd "atd16_espaces_verts" est utilisé par l'ATD16 pour répondre aux exigences de son schéma applicatif. La modification de cette valeur nécessitera d'intervenir sur l'ensemble des fichiers de code.

- Script agrégé de l'ensemble de la structure [[sql](donnees/bdd/xmap/aggregat_atd16_espaces_verts.sql)]
- Schéma **atd16_espaces_verts** [[sql](donnees/bdd/xmap/000_create_schema_atd16_espaces_verts.sql)]
    * Fonctions-triggers génériques [[sql](donnees/bdd/xmap/001_create_function_trigger_generique.sql)]
    * Tables de listes déroulantes :
        * Table **lst_type_site** [[sql](donnees/bdd/xmap/010_lst_type_site.sql)]
        * Table **lst_type_arrivee_d_eau** [[sql](donnees/bdd/xmap/011_lst_type_arrivee_d_eau.sql)]
        * Table **lst_type_cloture** [[sql](donnees/bdd/xmap/012_lst_type_cloture.sql)]
        * Table **lst_type_cours_d_eau** [[sql](donnees/bdd/xmap/013_lst_type_cours_d_eau.sql)]
        * Table **lst_type_etendue_d_eau** [[sql](donnees/bdd/xmap/014_lst_type_etendue_d_eau.sql)]
        * Table **lst_type_voie_circu** [[sql](donnees/bdd/xmap/015_lst_type_voie_circu.sql)]
        * Table **lst_type_zone_circu** [[sql](donnees/bdd/xmap/016_lst_type_zone_circu.sql)]
        * Table **lst_arbre_danger** [[sql](donnees/bdd/xmap/017_lst_arbre_danger.sql)]
        * Table **lst_arbre_forme** [[sql](donnees/bdd/xmap/018_lst_arbre_forme.sql)]
        * Table **lst_arbre_hauteur** [[sql](donnees/bdd/xmap/019_lst_arbre_hauteur.sql)]
        * Table **lst_arbre_implantation** [[sql](donnees/bdd/xmap/020_lst_arbre_implantation.sql)]
        * Table **lst_arbre_sol** [[sql](donnees/bdd/xmap/021_lst_arbre_sol.sql)]
        * Table **lst_haie_type_saisie** [[sql](donnees/bdd/xmap/022_lst_haie_type_saisie.sql)]
        * Table **lst_position** [[sql](donnees/bdd/xmap/023_lst_position.sql)]
        * Table **lst_domanialite** [[sql](donnees/bdd/xmap/024_lst_domanialite.sql)]
        * Table **lst_qualite_domanialite** [[sql](donnees/bdd/xmap/025_lst_qualite_domanialite.sql)]
        * Table **lst_type_entretien** [[sql](donnees/bdd/xmap/026_lst_type_entretien.sql)]
        * Table **lst_nature_intervention** [[sql](donnees/bdd/xmap/027_lst_nature_intervention.sql)]
        * Table **lst_mobilier_faunistique** [[sql](donnees/bdd/xmap/028_lst_mobilier_faunistique.sql)]
        * Table **lst_type_alignement_arbres** [[sql](donnees/bdd/xmap/029_lst_type_alignement_arbres.sql)]
        * Table **lst_type_arbre** [[sql](donnees/bdd/xmap/030_lst_type_arbre.sql)]
        * Table **lst_type_forme_arbre_fruitier** [[sql](donnees/bdd/xmap/031_lst_type_forme_arbre_fruitier.sql)]
        * Table **lst_type_haie** [[sql](donnees/bdd/xmap/032_lst_type_haie.sql)]
    * Tables non géographiques communales :
        * Table **ngeo_contrat_com** [[sql](donnees/bdd/xmap/040_ngeo_contrat_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/240_trigger_ngeo_contrat_com.sql)]
        * Table **ngeo_entretien_type_zone_gestion_com** [[sql](donnees/bdd/xmap/046_ngeo_entretien_type_zone_gestion_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/246_trigger_ngeo_entretien_type_zone_gestion_com.sql)]
        * Table **ngeo_intervention_zone_gestion_com** [[sql](donnees/bdd/xmap/050_ngeo_intervention_zone_gestion_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/250_trigger_ngeo_intervention_zone_gestion_com.sql)]
    * Tables géographiques communales :
        * Table **geo_commune** [[sql](donnees/bdd/xmap/100_geo_commune.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_commune_contrat.json)]
        * Table **geo_zone_gestion_com** [[sql](donnees/bdd/xmap/110_geo_zone_gestion_com.sql)]
            * Fonction-trigger dépendante [[sql](donnees/bdd/xmap/111_function_trigger_depend_geo_zone_gestion_com.sql)]
            * Fonctions-triggers associées [[sql](donnees/bdd/xmap/310_trigger_geo_zone_gestion_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_zone_gestion.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_zone_gestion.sld)]
            * Représentation thématique par type de zone [[sld](donnees/representation_graphique/sld/atd16.ev_zone_gestion_thematique.sld)]
            * Représentation thématique des interventions en cours [[sld](donnees/representation_graphique/sld/atd16.ev_interv_en_cours_surf.sld)]
            * Représentation thématique des interventions à venir dans les 7 jours [[sld](donnees/representation_graphique/sld/atd16.ev_interv_a_venir_7_surf.sld)]
            * Représentation thématique des interventions à venir dans les 30 jours [[sld](donnees/representation_graphique/sld/atd16.ev_interv_a_venir_30_surf.sld)]
            * Représentation thématique des interventions sans date de début [[sld](donnees/representation_graphique/sld/atd16.ev_alert_sans_date_debut_surf.sld)]
            * Représentation thématique des interventions ayant une date de fin inférieure à la date de début [[sld](donnees/representation_graphique/sld/atd16.ev_alert_date_fin_inf_date_debut_surf.sld)]
            * Représentation thématique des interventions passées sans durée [[sld](donnees/representation_graphique/sld/atd16.ev_alert_interv_sans_duree_h.sld)]
            * Représentation thématique des entretiens sans périodicité annuelle [[sld](donnees/representation_graphique/sld/atd16.ev_alert_entretien_sans_periodicite_annuelle.sld)]
            * Représentation thématique des entretiens sans durée moyenne [[sld](donnees/representation_graphique/sld/atd16.ev_alert_entretien_sans_tps_passe_moyen.sld)]
            * Représentation textuelle de la durée planifiée annuelle estimée [[sld](donnees/representation_graphique/sld/atd16.ev_label_tps_planifie_estime.sld)]
            * Représentation textuelle de la durée d'intervention totale par an [[sld](donnees/representation_graphique/sld/atd16.ev_label_tps_intervention_passe_n.sld)]
        * Table **geo_zone_boisee_com** [[sql](donnees/bdd/xmap/118_geo_zone_boisee_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_zone_boisee.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_zone_boisee.sld)]
        * Table **geo_alignement_arbre_com** [[sql](donnees/bdd/xmap/122_geo_alignement_arbre_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_alignement_arbres.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_alignement_arbres.sld)]
        * Table **geo_arbre_isole_com** [[sql](donnees/bdd/xmap/126_geo_arbre_isole_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_arbre_isole.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_arbre_isole.sld)]
        * Table **geo_massif_arbustif_com** [[sql](donnees/bdd/xmap/130_geo_massif_arbustif_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_massif_arbustif.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_massif_arbustif.sld)]
        * Table **geo_haie_com** [[sql](donnees/bdd/xmap/134_geo_haie_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_haie.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_haie.sld)]
        * Table **geo_arbuste_isole_com** [[sql](donnees/bdd/xmap/138_geo_arbuste_isole_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_arbuste_isole.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_arbuste_isole.sld)]
        * Table **geo_espace_enherbe_com** [[sql](donnees/bdd/xmap/142_geo_espace_enherbe_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_espace_enherbe.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_espace_enherbe.sld)]
        * Table **geo_massif_fleuri_com** [[sql](donnees/bdd/xmap/146_geo_massif_fleuri_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_massif_fleuri.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_massif_fleuri.sld)]
        * Table **geo_fleuri_isole_com** [[sql](donnees/bdd/xmap/150_geo_fleuri_isole_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_fleuri_isole.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_fleuri_isole.sld)]
        * Table **geo_etendue_d_eau_com** [[sql](donnees/bdd/xmap/154_geo_etendue_d_eau_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_etendue_d_eau.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_etendue_d_eau.sld)]
        * Table **geo_cours_d_eau_com** [[sql](donnees/bdd/xmap/158_geo_cours_d_eau_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_cours_d_eau.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_cours_d_eau.sld)]
        * Table **geo_point_d_eau_com** [[sql](donnees/bdd/xmap/162_geo_point_d_eau_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_point_d_eau.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_point_d_eau.sld)]
        * Table **geo_arrivee_d_eau_com** [[sql](donnees/bdd/xmap/166_geo_arrivee_d_eau_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_arrivee_d_eau.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_arrivee_d_eau.sld)]
        * Table **geo_espace_loisir_com** [[sql](donnees/bdd/xmap/170_geo_espace_loisir_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_espace_loisir.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_espace_loisir.sld)]
        * Table **geo_loisir_isole_com** [[sql](donnees/bdd/xmap/174_geo_loisir_isole_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_loisir_isole.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_loisir_isole.sld)]
        * Table **geo_zone_circu_com** [[sql](donnees/bdd/xmap/178_geo_zone_circu_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_zone_circu.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_zone_circu.sld)]
        * Table **geo_voie_circu_com** [[sql](donnees/bdd/xmap/182_geo_voie_circu_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_voie_circu.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_voie_circu.sld)]
        * Table **geo_cloture_com** [[sql](donnees/bdd/xmap/186_geo_cloture_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_cloture.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_cloture.sld)]
        * Table **geo_mobilier_faunistique_com** [[sql](donnees/bdd/xmap/190_geo_mobilier_faunistique_com.sql)]
            * Représentation graphique [[json](donnees/representation_graphique/json/ev_mobilier_faunistique.json)] - [[sld](donnees/representation_graphique/sld/atd16.ev_mobilier_faunistique.sld)]
            * Représentation thématique des ruches [[sld](donnees/representation_graphique/sld/atd16.ev_mobilier_f_ruche.sld)]
            * Représentation thématique des dorlotoires [[sld](donnees/representation_graphique/sld/atd16.ev_mobilier_f_dorlotoire.sld)]
        * Représentation graphique des objets mellifères surfaciques [[sld](donnees/representation_graphique/sld/atd16.ev_surface_mellifere.sld)]
        * Représentation graphique des objets mellifères linéaires [[sld](donnees/representation_graphique/sld/atd16.ev_lineaire_mellifere.sld)]
        * Représentation graphique des objets mellifères ponctuels [[sld](donnees/representation_graphique/sld/atd16.ev_ponctuel_mellifere.sld)]

# Ressources annexes

* Projet Github de l'agglomération de Compiègne [[url](https://github.com/sigagglocompiegne/espace_vert)]
* Notice d'utilisation dans X'MAP [[pdf](ressources/_ATD16__Géo16Créa_-_Espaces_verts.pdf)]

# Ressources générales
