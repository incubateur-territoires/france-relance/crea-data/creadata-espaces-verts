-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/10/11 : SL / Modification des enregistrements

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                       Table non géographique : Domaine de valeur du type de clôture                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_cloture;

CREATE TABLE atd16_espaces_verts.lst_type_cloture 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type de clôture
    valeur varchar(80), --[ARC_typ3] Libellé du type de clôture
    CONSTRAINT pk_lst_type_cloture PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_cloture OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_cloture TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_cloture IS '[ARC_typ3] Domaine de valeur des codes du type de clôture';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_cloture.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_cloture.code IS '[ARC_typ3] Code du type de clôture';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_cloture.valeur IS '[ARC_typ3] Libellé du type de clôture';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_cloture 
    (code,valeur)
VALUES
    ('221','Mur'),
    ('222','Grillage'),
    ('223','Palissage'),
    ('229','Autre clôture');

