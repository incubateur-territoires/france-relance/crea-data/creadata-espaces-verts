-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/28 : SL / Création du fichier sur Git
-- 2021/05/11 : SL / Modification du nom du fichier (préfixe 000_)
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                    SCHEMA : Gestion des espaces verts v2                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP SCHEMA IF EXISTS atd16_espaces_verts;

CREATE SCHEMA atd16_espaces_verts;
ALTER SCHEMA atd16_espaces_verts OWNER TO sditecgrp;
COMMENT ON SCHEMA atd16_espaces_verts IS 'Gestion des espaces verts';



