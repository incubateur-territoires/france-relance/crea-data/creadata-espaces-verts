-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                  Table non géographique : Domaine de valeur décrivant la position des objets "espace vert" de type végétal                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_position;

CREATE TABLE atd16_espaces_verts.lst_position 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe décrivant la position des objets "espace vert" de type végétal
    valeur varchar(50), --[ARC] Valeur de la classe décrivant la position des objets "espace vert" de type végétal
    CONSTRAINT pk_lst_position PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_position OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_position TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_position IS '[ARC] Liste des valeurs décrivant la position des objets "espace vert" de type végétal';

COMMENT ON COLUMN atd16_espaces_verts.lst_position.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_position.code IS '[ARC] Code de la classe décrivant la position des objets "espace vert" de type végétal';
COMMENT ON COLUMN atd16_espaces_verts.lst_position.valeur IS 
    '[ARC] Valeur de la classe décrivant la position des objets "espace vert" de type végétal';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_position 
    (code,valeur)
VALUES
    ('10','Sol'),
    ('20','Hors-sol (non précisé)'),
    ('21','Pot'),
    ('22','Bac'),
    ('23','Jardinière'),
    ('24','Suspension'),
    ('29','Autre');

