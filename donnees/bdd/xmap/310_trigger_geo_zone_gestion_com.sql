-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/07 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idzone_2() et du trigger associé t_after_iud_maj_idzone_2
-- 2021/11/04 : SL / Modification du nom de la fonction f_maj_idzone_2_com() et du trigger associé t_after_iud_maj_idzone_2_com
--                 . Ajout de la fonction f_supp_intervention_com() et du trigger associé t_after_d_supp_intervention_com
--                 . Ajout de la fonction f_supp_entretien_type_com() et du trigger associé t_after_d_supp_entretien_type_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Fonctions triggers et triggers spécifiques à la table geo_zone_gestion_com                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                           Mise à jour du lien idzone lorsqu'une zone de gestion est créée, modifiée ou supprimée                           ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_idzone_2_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_idzone_2_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_espaces_verts.geo_zone_boisee_com AS a SET idzone = NULL WHERE st_intersects(OLD.the_geom, a.the_geom);
		UPDATE atd16_espaces_verts.geo_alignement_arbres_com AS b SET idzone = NULL WHERE st_intersects(OLD.the_geom, b.the_geom);
		UPDATE atd16_espaces_verts.geo_arbre_isole_com AS c SET idzone = NULL WHERE st_intersects(OLD.the_geom, c.the_geom);
		UPDATE atd16_espaces_verts.geo_massif_arbustif_com AS d SET idzone = NULL WHERE st_intersects(OLD.the_geom, d.the_geom);
		UPDATE atd16_espaces_verts.geo_haie_com AS e SET idzone = NULL WHERE st_intersects(OLD.the_geom, e.the_geom);
		UPDATE atd16_espaces_verts.geo_arbuste_isole_com AS f SET idzone = NULL WHERE st_intersects(OLD.the_geom, f.the_geom);
		UPDATE atd16_espaces_verts.geo_espace_enherbe_com AS g SET idzone = NULL WHERE st_intersects(OLD.the_geom, g.the_geom);
		UPDATE atd16_espaces_verts.geo_massif_fleuri_com AS h SET idzone = NULL WHERE st_intersects(OLD.the_geom, h.the_geom);
		UPDATE atd16_espaces_verts.geo_fleuri_isole_com AS i SET idzone = NULL WHERE st_intersects(OLD.the_geom, i.the_geom);
		UPDATE atd16_espaces_verts.geo_etendue_d_eau_com AS j SET idzone = NULL WHERE st_intersects(OLD.the_geom, j.the_geom);
		UPDATE atd16_espaces_verts.geo_cours_d_eau_com AS k SET idzone = NULL WHERE st_intersects(OLD.the_geom, k.the_geom);
		UPDATE atd16_espaces_verts.geo_point_d_eau_com AS l SET idzone = NULL WHERE st_intersects(OLD.the_geom, l.the_geom);
		UPDATE atd16_espaces_verts.geo_arrivee_d_eau_com AS m SET idzone = NULL WHERE st_intersects(OLD.the_geom, m.the_geom);
		UPDATE atd16_espaces_verts.geo_espace_loisir_com AS n SET idzone = NULL WHERE st_intersects(OLD.the_geom, n.the_geom);
		UPDATE atd16_espaces_verts.geo_loisir_isole_com AS o SET idzone = NULL WHERE st_intersects(OLD.the_geom, o.the_geom);
		UPDATE atd16_espaces_verts.geo_zone_circu_com AS p SET idzone = NULL WHERE st_intersects(OLD.the_geom, p.the_geom);
		UPDATE atd16_espaces_verts.geo_voie_circu_com AS q SET idzone = NULL WHERE st_intersects(OLD.the_geom, q.the_geom);
		UPDATE atd16_espaces_verts.geo_cloture_com AS r SET idzone = NULL WHERE st_intersects(OLD.the_geom, r.the_geom);
        -- Mise à jour du champ idzone avec la valeur NULL lorsque l'objet est impacté (intersecté) par la suppression d'une zone de gestion
    ELSE
		UPDATE atd16_espaces_verts.geo_zone_boisee_com AS a SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, a.the_geom);
		UPDATE atd16_espaces_verts.geo_alignement_arbres_com AS b SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, b.the_geom);
		UPDATE atd16_espaces_verts.geo_arbre_isole_com AS c SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, c.the_geom);
		UPDATE atd16_espaces_verts.geo_massif_arbustif_com AS d SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, d.the_geom);
		UPDATE atd16_espaces_verts.geo_haie_com AS e SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, e.the_geom);
		UPDATE atd16_espaces_verts.geo_arbuste_isole_com AS f SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, f.the_geom);
		UPDATE atd16_espaces_verts.geo_espace_enherbe_com AS g SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, g.the_geom);
		UPDATE atd16_espaces_verts.geo_massif_fleuri_com AS h SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, h.the_geom);
		UPDATE atd16_espaces_verts.geo_fleuri_isole_com AS i SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, i.the_geom);
		UPDATE atd16_espaces_verts.geo_etendue_d_eau_com AS j SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, j.the_geom);
		UPDATE atd16_espaces_verts.geo_cours_d_eau_com AS k SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, k.the_geom);
		UPDATE atd16_espaces_verts.geo_point_d_eau_com AS l SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, l.the_geom);
		UPDATE atd16_espaces_verts.geo_arrivee_d_eau_com AS m SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, m.the_geom);
		UPDATE atd16_espaces_verts.geo_espace_loisir_com AS n SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, n.the_geom);
		UPDATE atd16_espaces_verts.geo_loisir_isole_com AS o SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, o.the_geom);
		UPDATE atd16_espaces_verts.geo_zone_circu_com AS p SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, p.the_geom);
		UPDATE atd16_espaces_verts.geo_voie_circu_com AS q SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, q.the_geom);
		UPDATE atd16_espaces_verts.geo_cloture_com AS r SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, r.the_geom);
        -- Mise à jour du champ idzone avec la valeur gid (zone de gestion) lorsque l'objet est impacté (intersecté) 
        -- par la création ou la modification d'une zone de gestion
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_idzone_2_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_idzone_2_com() IS 
	'[ATD16] Mise à jour du champ idzone lorsqu''une zone de gestion est créée, modifiée ou supprimée';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_idzone_2_com
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.geo_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_2_com();
    
COMMENT ON TRIGGER t_after_iud_maj_idzone_2_com ON atd16_espaces_verts.geo_zone_gestion_com IS 
    '[ATD16] Mise à jour du champ idzone lorsqu''une zone de gestion est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                                   Suppression des interventions lorsqu'une zone de gestion est supprimée                                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_supp_intervention_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_supp_intervention_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com
	WHERE id_zone_gestion = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_supp_intervention_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_supp_intervention_com() IS 
	'[ATD16] Suppression des interventions lorsqu''une zone de gestion est supprimée';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_supp_intervention_com
    AFTER DELETE
    ON atd16_espaces_verts.geo_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_supp_intervention_com();
    
COMMENT ON TRIGGER t_after_d_supp_intervention_com ON atd16_espaces_verts.geo_zone_gestion_com IS 
    '[ATD16] Suppression des interventions lorsqu''une zone de gestion est supprimée';


-- ##################################################################################################################################################
-- ###                                  Suppression des entretiens types lorsqu'une zone de gestion est supprimée                                 ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_supp_entretien_type_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_supp_entretien_type_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com
	WHERE id_zone_gestion = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_supp_entretien_type_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_supp_entretien_type_com() IS 
	'[ATD16] Suppression des interventions lorsqu''une zone de gestion est supprimée';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_supp_entretien_type_com
    AFTER DELETE
    ON atd16_espaces_verts.geo_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_supp_entretien_type_com();
    
COMMENT ON TRIGGER t_after_d_supp_entretien_type_com ON atd16_espaces_verts.geo_zone_gestion_com IS 
    '[ATD16] Suppression des interventions lorsqu''une zone de gestion est supprimée';

