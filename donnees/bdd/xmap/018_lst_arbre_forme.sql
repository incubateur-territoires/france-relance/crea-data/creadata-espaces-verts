-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table non géographique : Domaine de valeur de la forme des arbres isolés                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_forme;

CREATE TABLE atd16_espaces_verts.lst_arbre_forme 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe de forme des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe de forme des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_forme PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_forme OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_forme TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_forme IS '[ARC] Liste permettant de décrire la classe de forme des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_forme.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_forme.code IS '[ARC] Code de la classe de forme des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_forme.valeur IS '[ARC] Valeur de la classe de forme des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_forme 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Rideau'),
    ('2','Taille de contrainte'),
    ('3','Taille douce'),
    ('4','Libre'),
    ('5','Tête de chat');

