-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/11 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout de la fonction f_maj_sup_m2()
-- 2021/09/07 : SL / Ajout de la fonction f_maj_perimetre()
--                 . Ajout de la fonction f_maj_XY_coord()
-- 2021/09/08 : SL / Ajout de la fonction f_maj_long_m()
-- 2021/12/01 : SL / Modification de la fonction f_maj_sup_m2()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_date_creation();

CREATE FUNCTION atd16_espaces_verts.f_date_creation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_creation = now(); -- À la création d'un objet, le champ date_creation prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_date_creation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_date_creation() IS 'Fonction trigger permettant l''initialisation du champ date_creation';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_date_maj();

CREATE FUNCTION atd16_espaces_verts.f_date_maj()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_maj = now(); -- À la modification d'un objet, le champ date_maj prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_date_maj() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_date_maj() IS 'Fonction trigger permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_sup_m2();

CREATE FUNCTION atd16_espaces_verts.f_maj_sup_m2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    IF NEW.sup_m2 IS NULL THEN
        NEW.sup_m2 = ST_Area(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- Calcul de la surface à partir de la géométrie 
        -- reprojection car en 3857 la surface est à peu près deux fois plus grande
    END IF;
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_sup_m2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_sup_m2() IS 'Fonction trigger permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ perimetre                                                       ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_perimetre();

CREATE FUNCTION atd16_espaces_verts.f_maj_perimetre()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.perimetre = ST_Perimeter(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- Calcul du périmètre à partir de la géométrie
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_perimetre() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_perimetre() IS 'Fonction trigger permettant la mise à jour du champ perimetre';


-- ##################################################################################################################################################
-- ###                                                 Récupération des coordonnées géographiques                                                 ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_XY_coord();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_XY_coord()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.x_l93 := ST_X(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le X, on le définit en 3857, puis on le met en 2154
    NEW.y_l93 := ST_Y(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le Y, on le définit en 3857, puis on le met en 2154
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_XY_coord() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_XY_coord() IS 
    'Fonction trigger permettant la récupération des coordonnées géographiques';


-- ##################################################################################################################################################
-- ###                                                  Mise à jour de la longueur (champ long_m)                                                 ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_long_m();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_long_m()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.long_m := ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); 
    -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_long_m() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_long_m() IS '[ATD16] Mise à jour de la longueur';

