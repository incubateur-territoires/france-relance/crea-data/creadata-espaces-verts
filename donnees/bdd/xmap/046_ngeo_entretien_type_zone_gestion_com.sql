-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/04 : SL / Création du fichier sur Git
--                 . Ajout des triggers t_before_i_init_date_creation et t_before_u_date_maj

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                         Table non géographique : Liste des entretiens types sur les zones de gestion de la commune                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com;

CREATE TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)
    ident varchar(254), --[ATD16] Identifiant de l'entretien
    type_entretien integer, --[FK][ATD16] Code du type d'entretien
    periodicite varchar, --[ATD16] Périodicité moyenne de l'entretien en jours
    periodicite_annuelle integer, --[ATD16] Périodicité annuelle de l'entretien en jours
    id_zone_gestion integer, --[FK][ATD16] Code de la zone de gestion (gid) (nécessaire pour paramétrer sw_custom_fiche)
    tps_passe_moyen double precision, --[ATD16] Durée moyenne de l'entretien en heure
    descript varchar(254), --[ATD16] Description de l'entretien, outils/moyens utilisés
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    CONSTRAINT pk_ngeo_entretien_type_zone_gestion_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    '[ATD16] Table non géographique listant les entretiens types effectués sur les espaces verts par zone de gestion';

COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.gid IS 
    '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.insee IS 
    '[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.ident IS '[ATD16] Identifiant de l''entretien';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.type_entretien IS '[FK][ATD16] Code du type d''entretien';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.periodicite IS 
    '[ATD16] Périodicité moyenne de l''entretien en jours';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.periodicite_annuelle IS 
    '[ATD16] Périodicité annuelle de l''entretien en jours';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.id_zone_gestion IS 
    '[FK][ATD16] Code de la zone de gestion (gid) (nécessaire pour paramétrer sw_custom_fiche)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.tps_passe_moyen IS '[ATD16] Durée moyenne de l''entretien en heure';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.descript IS 
    '[ATD16] Description de l''entretien, outils/moyens utilisés';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.date_maj IS 
    '[ATD16] Date de dernière mise à jour de l''objet (automatique)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

