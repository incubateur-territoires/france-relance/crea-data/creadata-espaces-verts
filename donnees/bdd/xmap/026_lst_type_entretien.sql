-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/13 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                          Table non géographique : Domaine de valeur sur le type d'entretien réalisé ou à réaliser                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_entretien;

CREATE TABLE atd16_espaces_verts.lst_type_entretien 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code du type d'entretien réalisé ou à réaliser
    valeur varchar(50), --[ARC] Valeur du type d'entretien réalisé ou à réaliser
    CONSTRAINT pk_lst_type_entretien PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_entretien OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_entretien TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_entretien IS '[ARC] Domaine de valeur ddu type d''entretien réalisé ou à réaliser';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_entretien.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_entretien.code IS '[ARC] Code du type d''entretien réalisé ou à réaliser';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_entretien.valeur IS '[ARC] Valeur du type d''entretien réalisé ou à réaliser';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_entretien 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Fauchage'),
    ('2','Débroussaillage'),
    ('3','Tonte'),
    ('4','Usage de produits phytosanitaires'),
    ('5','Taille'),
    ('6','Plantation'),
    ('7','Collecte de déchets'),
    ('8','Ramassage des branches et feuilles'),
    ('9','Recépage'),
    ('10','Élagage'),
    ('99','Autre');

