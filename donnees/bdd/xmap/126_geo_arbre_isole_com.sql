-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/04 : SL / Création du fichier sur Git
-- 2021/05/05 : SL / Suppression du champ idcontrat
--                 . Changement du nom de la table
-- 2021/05/06 : SL / Suppression du champ idsite
--                 . Ajout du champ insee_contrat
--                 . Ajout du champ idcontrat
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_arbre_isole_com
--                 . Ajout de la vue v_geo_arbre_isole_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_arbre_isole_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_arbre_isole_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_arbre_isole_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_arbre_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_arbre_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_arbre_isole_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/11/30 : SL / Ajout des champs age et date_plantation
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/02 : SL / Ajout de la vue v_geo_arbre_isole_mellifere_com
-- 2022/11/28 : SL / Ajout des champs type_arbre et type_forme_arbre_fr

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                 Table géographique : Arbre isolé (commune)                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_arbre_isole_com;

CREATE TABLE atd16_espaces_verts.geo_arbre_isole_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    position integer, --[FK][ARC_vegetal] Position de l'arbre
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Arbre isolé" / Champ nom pour [ARC_arbre]
    type_arbre integer, --[FK][ATD16] Type d'arbre
    type_forme_arbre_fr integer, --[FK][ATD16] Type de forme pour l'arbre fruitier
    flore_mellifere boolean, --[ATD16] Caractère mellifère de l'arbre
    genre varchar(20), --[ARC_arbre] 
    espece varchar(20), --[ARC_arbre] 
    age varchar(20), --[ATD16] Age de l'arbre
    date_plantation date, --[ATD16] Date de plantation de l'arbre
    hauteur integer, --[FK][ARC_arbre] Hauteur de l'arbre
    circonf double precision, --[ARC_arbre] 
    forme integer, --[FK][ARC_arbre] Forme de l'arbre
    etat_gen varchar(30), --[ARC_arbre] 
    implant integer, --[FK][ARC_arbre] Implantation de l'arbre
    remarq varchar(3), --[ARC_arbre] 
    malad_obs varchar(3), --[ARC_arbre] 
    malad_nom varchar(80), --[ARC_arbre] 
    danger integer, --[FK][ARC_arbre] Dangerosité
    natur_sol integer, --[FK][ARC_arbre] Nature du sol
    envnmt_obs varchar(254), --[ARC_arbre] 
    utilis_obs varchar(254), --[ARC_arbre] 
    cplt_fic_1 varchar(254), --[ARC_arbre] 
    cplt_fic_2 varchar(254), --[ARC_arbre] 
    gps_date date, --[ARC_arbre] 
    gnss_heigh double precision, --[ARC_arbre]
    vert_prec double precision, --[ARC_arbre]
    horz_prec double precision, --[ARC_arbre]
    northing double precision, --[ARC_arbre]
    easting double precision, --[ARC_arbre]
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_arbre_isole_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_arbre_isole_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_arbre_isole_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_arbre_isole_com IS '[ATD16] Table géographique contenant les arbres isolés';

COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.position IS '[ARC_vegetal] Position de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.x_l93 IS '[ARC_pct] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.y_l93 IS '[ARC_pct] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.ident IS 
    '[SIRAP] Identificaion de l''objet / Par défaut "Arbre isolé" / Champ nom pour [ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.type_arbre IS '[FK][ATD16] Type d''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.type_forme_arbre_fr IS '[FK][ATD16] Type de forme pour l''arbre fruitier';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.flore_mellifere IS '[ATD16] Caractère mellifère de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.genre IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.espece IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.age IS '[ATD16] Age de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.date_plantation IS '[ATD16] Date de plantation de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.hauteur IS '[FK][ARC_arbre] Hauteur de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.circonf IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.forme IS '[FK][ARC_arbre] Forme de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.etat_gen IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.implant IS '[FK][ARC_arbre] Implantation de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.remarq IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.malad_obs IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.malad_nom IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.danger IS '[FK][ARC_arbre] Dangerosité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.natur_sol IS '[FK][ARC_arbre] Nature du sol';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.envnmt_obs IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.utilis_obs IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.cplt_fic_1 IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.cplt_fic_2 IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.gps_date IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.gnss_heigh IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.vert_prec IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.horz_prec IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.northing IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.easting IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                     v_geo_arbre_isole_mellifere_com : vue des arbres isolés mellifères                                     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_arbre_isole_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_arbre_isole_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.type_arbre,
        a.type_forme_arbre_fr,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.genre,
        a.espece,
        a.age,
        a.date_plantation,
        a.hauteur,
        a.circonf,
        a.forme,
        a.etat_gen,
        a.implant,
        a.danger,
        a.natur_sol,
        a.remarq,
        a.the_geom
    FROM atd16_espaces_verts.geo_arbre_isole_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_arbre_isole_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_arbre_isole_mellifere_com IS 
    '[ATD16] Vue listant les arbres isolés mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_arbre_isole_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_arbre_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_arbre_isole_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_arbre_isole_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_arbre_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_arbre_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_arbre_isole_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_arbre_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_arbre_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_arbre_isole_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_arbre_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_arbre_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

