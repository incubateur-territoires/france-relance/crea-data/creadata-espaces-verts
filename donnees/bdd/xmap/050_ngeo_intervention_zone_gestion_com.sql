-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/13 : SL / Création du fichier sur Git
--                 . Ajout des triggers t_before_i_init_date_creation et t_before_u_date_maj
-- 2021/11/03 : SL / Modification du nom du fichier en 050_ngeo_intervention_zone_gestion_com.sql
--                 . Modification de l'ensemble du fichier (entretien -> intervention)
--                 . Ajout du champ duree_h qui permet de noter la durée de l'intervetion en heure

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                Table non géographique : Intervention sur les zones de gestion de la commune                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com;

CREATE TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)
    ident varchar(254), --[ATD16] Identifiant de l'intervention
    type_entretien integer, --[FK][ATD16] Code du type d'entretien pratiqué lors de l'intervention
    date_debut date, --[ATD16] Date de début de l'intervention
    date_fin date, --[ATD16] Date de fin de l'intervention
    id_zone_gestion integer, --[FK][ATD16] Code de la zone de gestion (gid) (nécessaire pour paramétrer sw_custom_fiche)
    periodicite integer, --[ATD16] Périodicité de l'intervention en jours (cela détermine une nouvelle intervention)
    duree_h double precision, --[ATD16] Durée de l'intervention en heure
    descript varchar(254), --[ATD16] Description de l'intervention, outils/moyens utilisés
    observation varchar(254), --[ATD16] Observation sur l'intervention
    nature_interv integer, --[FK][ATD16] Code de la nature de l'intervention
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    CONSTRAINT pk_ngeo_intervention_zone_gestion_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    '[ATD16] Table non géographique listant les interventions effectués sur les espaces verts';

COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.gid IS 
    '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.insee IS 
    '[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.ident IS '[ATD16] Identifiant de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.type_entretien IS 
    '[FK][ATD16] Code du type d''entretien pratiqué lors de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.date_debut IS '[ATD16] Date de début de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.date_fin IS '[ATD16] Date de fin de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.id_zone_gestion IS 
    '[FK][ATD16] Code de la zone de gestion (gid) (nécessaire pour paramétrer sw_custom_fiche)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.periodicite IS 
    '[ATD16] Périodicité de l''intervention en jours (cela détermine une nouvelle intervention)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.duree_h IS '[ATD16] Durée de l''intervention en heure';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.descript IS 
    '[ATD16] Description de l''intervention, outils/moyens utilisés';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.observation IS '[ATD16] Observation sur l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.nature_interv IS '[FK][ATD16] Code de la nature de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

