-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/09 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_insee() et du trigger associé t_before_iu_maj_insee
-- 2021/09/13 : SL / Transfert de la fonction f_maj_insee() dans le fichier 101_function_trigger_depend_geo_commune.sql
-- 2021/09/14 : SL / Ajout de la fonction f_maj_insee_contrat_com() et du trigger associé t_before_iu_maj_insee
-- 2021/11/16 : SL / Ajout de la fonction Ajout de la fonction f_supp_idcontrat_com() et du trigger associé t_after_d_maj_idcontrat_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                   Fonctions triggers et triggers spécifiques à la table ngeo_contrat_com                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                         Mise à jour du champ insee                                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_insee_contrat_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_insee_contrat_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    SELECT com.insee
    FROM atd16_espaces_verts.geo_commune AS com
    WHERE NEW.pk_commune = com.gid
    INTO NEW.insee;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_insee_contrat_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_insee_contrat_com() IS 
    'Fonction trigger permettant la mise à jour du champ insee à partir de la table geo_commune';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_insee 
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.ngeo_contrat_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_insee_contrat_com();
    
COMMENT ON TRIGGER t_before_iu_maj_insee ON atd16_espaces_verts.ngeo_contrat_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ insee à partir de la table geo_commune';


-- ##################################################################################################################################################
-- ###                                        Suppression du lien idcontrat lorsqu'un contrat est supprimé                                        ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_supp_idcontrat_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_supp_idcontrat_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    UPDATE atd16_espaces_verts.geo_zone_gestion_com AS a
    SET idcontrat = NULL
    WHERE a.idcontrat = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_supp_idcontrat_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_supp_idcontrat_com() IS 
	'[ATD16] Suppression de la valeur du champ idcontrat lorsqu''un contrat est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_maj_idcontrat_com
    AFTER DELETE
    ON atd16_espaces_verts.ngeo_contrat_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_supp_idcontrat_com();
    
COMMENT ON TRIGGER t_after_d_maj_idcontrat_com ON atd16_espaces_verts.ngeo_contrat_com IS 
    '[ATD16] Suppression de la valeur du champ idcontrat lorsqu''un contrat est supprimé';

