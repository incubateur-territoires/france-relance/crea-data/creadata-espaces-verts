-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/05 : SL / Création du fichier sur Git
-- 2021/05/11 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout du trigger t_before_iu_maj_sup_m2
-- 2021/09/16 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_zone_gestion_com
--                 . Ajout de la vue v_geo_zone_gestion_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_zone_gestion_com
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_zone_gestion_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_zone_gestion_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_zone_gestion_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_zone_gestion_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_zone_gestion_com
-- 2021/11/03 : SL / Modification des 4 vues (entretien -> intervention)
--                 . Ajout des champs tps_planifie_estime et tps_intervention_passe
--                 . Ajout du champ nb_intervention_an
-- 2021/11/04 : SL / Modification des champs tps_intervention_passe_n et nb_intervention_n
--                 . Ajout des champs tps_intervention_passe_n_1 et nb_intervention_n_1
--                 . Ajout du champ duree_h dans les vues v_geo_zone_gestion_com_interv_en_cours et v_geo_alert_interv_a_venir_30_zone_gestion_com
-- 2021/11/08 : SL / Ajout de la vue v_geo_alert_interv_sans_duree_h_zone_gestion_com
--                 . Ajout de la vue v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com
--                 . Ajout de la vue v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com
-- 2021/12/01 : SL / Ajout de la vue v_geo_alert_interv_a_venir_7_zone_gestion_com
-- 2022/02/02 : SL / Modification de la vue v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com (échange des tables jointes)
-- 2022/02/04 : SL / Modification de la vue v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com (échange des tables jointes)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Zone de gestion (commune)                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_zone_gestion_com;

CREATE TABLE atd16_espaces_verts.geo_zone_gestion_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(50), --[SIRAP] Identifiant SIRAP / Nom de la zone de gestion
    nom_zone varchar(50), --[ARC] Nom de la zone de gestion
    sup_m2 integer, --[ARC] Superficie en m²
    type_site integer, --[FK][ARC] Identifiant du type de site
    insee_contrat varchar(6), --[FK][ATD16] Code insee de la commune permettant de sélectionner les contrats de la commune concernée (liste liée)
    idcontrat integer, --[FK][ARC_objet] Identifiant du contrat
    tps_planifie_estime double precision, --[ATD16] Temps planifié estimé de tous les entretiens dans une année en heures
    tps_intervention_passe_n double precision, --[ATD16] Somme du temps d'intervention passé dans l'année en heures
    tps_intervention_passe_n_1 double precision, --[ATD16] Somme du temps d'intervention passé dans l'année n-1 en heures
    nb_intervention_n integer, --[ATD16] Somme du nombre d'intervention passé dans l'année
    nb_intervention_n_1 integer, --[ATD16] Somme du nombre d'intervention passé dans l'année n-1
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_gestion_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_zone_gestion_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_zone_gestion_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_zone_gestion_com IS 
    '[ARC] Table contenant la géométrie des zones de gestion des espaces vert (fusion avec les site de [ARC])';

COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.ident IS '[SIRAP] Identifiant SIRAP / Nom de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.nom_zone IS '[ARC] Nom de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.sup_m2 IS '[ARC] Superficie en m²';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.type_site IS '[FK][ARC_site] Identifiant du type de site';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.insee_contrat IS 
    '[FK][ATD16] Code insee de la commune permettant de sélectionner les contrats de la commune concernée (liste liée)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.idcontrat IS '[FK][ARC_objet] Identifiant du contrat';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.tps_planifie_estime IS 
    '[ATD16] Temps planifié estimé de tous les entretiens dans une année en heures';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.tps_intervention_passe_n IS 
    '[ATD16] Somme du temps d''intervention passé dans l''année en heures';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.tps_intervention_passe_n_1 IS 
    '[ATD16] Somme du temps d''intervention passé dans l''année n-1 en heures';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.nb_intervention_n IS '[ATD16] Somme du nombre d''intervention passé dans l''année';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.nb_intervention_n_1 IS '[ATD16] Somme du nombre d''intervention passé dans l''année n-1';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###  v_geo_alert_interv_sans_date_debut_zone_gestion_com : vue des interventions des zones de gestion communales n'ayant pas de date de début  ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_sans_date_debut_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_sans_date_debut_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    WHERE b.date_debut IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_sans_date_debut_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_sans_date_debut_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales n''ayant pas de date de début';


-- ##################################################################################################################################################
-- ###               v_geo_zone_gestion_com_interv_en_cours : vue listant les interventions des zones de gestion communales en cours              ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_zone_gestion_com_interv_en_cours;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_zone_gestion_com_interv_en_cours 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        c.valeur AS label_type_entretien,
        b.ident,
        b.type_entretien,
        b.date_debut,
        b.date_fin,
        b.duree_h,
        b.descript,
        b.observation,
        b.nature_interv,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    LEFT JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.date_debut <= current_date AND b.date_fin >= current_date
    OR b.date_debut <= current_date AND b.date_fin IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_zone_gestion_com_interv_en_cours OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_zone_gestion_com_interv_en_cours IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales en cours';


-- ##################################################################################################################################################
-- ###   v_geo_alert_interv_a_venir_7_zone_gestion_com : vue listant les interventions des zones de gestion communales à venir dans les 7 jours   ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_7_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_7_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        b.ident,
        b.type_entretien,
        c.valeur AS label_type_entretien,
        b.date_debut,
        b.date_fin,
        b.duree_h,
        b.descript,
        b.observation,
        b.nature_interv,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    LEFT JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.date_debut > current_date AND b.date_debut < current_date+8
    ORDER BY b.date_debut DESC;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_a_venir_7_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_7_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales à venir dans les 7 jours';


-- ##################################################################################################################################################
-- ###  v_geo_alert_interv_a_venir_30_zone_gestion_com : vue listant les interventions des zones de gestion communales à venir dans les 30 jours  ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_30_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_30_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        b.ident,
        b.type_entretien,
        c.valeur AS label_type_entretien,
        b.date_debut,
        b.date_fin,
        b.duree_h,
        b.descript,
        b.observation,
        b.nature_interv,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    LEFT JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.date_debut > current_date AND b.date_debut < current_date+31
    ORDER BY b.date_debut DESC;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_a_venir_30_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_30_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales à venir dans les 30 jours';


-- ##################################################################################################################################################
-- ###v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com : vue des interventions des zones de gestion communales dont date_fin < date_debut###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    WHERE b.date_fin < b.date_debut AND b.date_fin IS NOT NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales dont date_fin < date_debut';


-- ##################################################################################################################################################
-- ### v_geo_alert_interv_sans_duree_h_zone_gestion_com : vue des interventions passées des z. de gestion communales dont duree_h n'est pas rempli###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_sans_duree_h_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_sans_duree_h_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        c.valeur AS label_type_entretien,
        b.ident,
        b.type_entretien,
        b.date_debut,
        b.date_fin,
        b.duree_h,
        b.descript,
        b.observation,
        b.nature_interv,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    LEFT JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.date_fin <= current_date
    AND b.duree_h IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_sans_duree_h_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_sans_duree_h_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions passées des zones de gestion communales dont le champ duree_h n''est pas renseigné';


-- ##################################################################################################################################################
-- ###   v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com : vue des entretiens dont le champ periodicite_annuelle n'est pas rempli   ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_entretien,
        c.valeur AS label_type_entretien,
        b.ident,
        b.type_entretien,
        b.periodicite,
        b.periodicite_annuelle,
        b.tps_passe_moyen,
        b.descript,
        a.the_geom
    FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com b
    LEFT JOIN atd16_espaces_verts.geo_zone_gestion_com a ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.periodicite_annuelle IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com IS 
    '[ATD16] Vue listant les entretiens des zones de gestion communales dont le champ periodicite_annuelle n''est pas renseigné';


-- ##################################################################################################################################################
-- ###      v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com : vue des entretiens dont le champ tps_passe_moyen n'est pas rempli       ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_entretien,
        c.valeur AS label_type_entretien,
        b.ident,
        b.type_entretien,
        b.periodicite,
        b.periodicite_annuelle,
        b.tps_passe_moyen,
        b.descript,
        a.the_geom
    FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com b
    LEFT JOIN atd16_espaces_verts.geo_zone_gestion_com a ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.tps_passe_moyen IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com IS 
    '[ATD16] Vue listant les entretiens des zones de gestion communales dont le champ tps_passe_moyen n''est pas renseigné';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_zone_gestion_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_zone_gestion_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_zone_gestion_com;

CREATE TRIGGER t_before_iu_maj_sup_m2
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_sup_m2();
    
COMMENT ON TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';

