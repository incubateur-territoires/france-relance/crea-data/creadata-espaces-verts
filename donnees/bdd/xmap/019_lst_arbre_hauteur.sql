-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur de la hauteur des arbres isolés                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_hauteur;

CREATE TABLE atd16_espaces_verts.lst_arbre_hauteur 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe de hauteur des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe de hauteur des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_hauteur PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_hauteur OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_hauteur TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_hauteur IS '[ARC] Domaine de valeur permettant de décrire la classe de hauteur des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_hauteur.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_hauteur.code IS '[ARC] Code de la classe de hauteur des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_hauteur.valeur IS '[ARC] Valeur de la classe de hauteur des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_hauteur 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Moins de 1 mètre'),
    ('2','1 à 2 mètres'),
    ('3','2 à 5 mètres'),
    ('4','5 à 10 mètres'),
    ('5','10 à 15 mètres'),
    ('6','15 à 20 mètres'),
    ('7','Plus de 20 mètres');

