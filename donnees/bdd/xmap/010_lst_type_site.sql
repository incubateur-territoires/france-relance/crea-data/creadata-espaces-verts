-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/05/04 : SL / Modification de la longueur du varchar de "valeur" (de 50 à 80)
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/11/03 : SL / Suppression de la valeur "Accotements de voies" : à gérer dans la gestion de la voirie

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur du type de site d'espaces verts                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_site;

CREATE TABLE atd16_espaces_verts.lst_type_site 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code du type de site intégrant les objets des espaces verts
    valeur varchar(80), --[ARC] Libellé du type de site intégrant les objets des espaces verts
    CONSTRAINT pk_lst_type_site PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_site OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_site TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_site IS '[ARC] Domaine de valeur des codes du type de site intégrant les objets des espaces verts';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_site.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_site.code IS '[ARC] Code du type de site intégrant les objets des espaces verts';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_site.valeur IS '[ARC] Libellé du type de site intégrant les objets des espaces verts';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_site 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Parc, jardin, square'),
    ('3','Accompagnement de bâtiments publics'),
    ('4','Accompagnement d''habitations'),
    ('5','Accompagnement d''établissements industriels et commerciaux'),
    ('6','Enceinte sportive'),
    ('7','Cimetière'),
    ('11','Espace naturel aménagé');

