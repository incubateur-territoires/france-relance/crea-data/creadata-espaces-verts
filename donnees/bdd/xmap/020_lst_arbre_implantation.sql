-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                               Table non géographique : Domaine de valeur de l'implantation des arbres isolés                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_implantation;

CREATE TABLE atd16_espaces_verts.lst_arbre_implantation 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe d'implantation des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe d'implantation des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_implantation PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_implantation OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_implantation TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_implantation IS '[ARC] Liste permettant de décrire la classe d''implantation des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_implantation.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_implantation.code IS '[ARC] Code de la classe d''implantation des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_implantation.valeur IS '[ARC] Valeur de la classe d''implantation des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_implantation 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Alignement'),
    ('2','Groupe/Bosquet'),
    ('3','Solitaire');

