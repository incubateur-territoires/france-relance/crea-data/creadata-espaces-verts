-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur du type d'arrivée d'eau                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_arrivee_d_eau;

CREATE TABLE atd16_espaces_verts.lst_type_arrivee_d_eau 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type d'arrivée d'eau
    valeur varchar(80), --[ARC_typ3] Libellé du type d'arrivée d'eau
    CONSTRAINT pk_lst_type_arrivee_d_eau PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_arrivee_d_eau OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_arrivee_d_eau TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_arrivee_d_eau IS '[ARC_typ3] Domaine de valeur des codes du type d''arrivée d''eau';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_arrivee_d_eau.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_arrivee_d_eau.code IS '[ARC_typ3] Code du type d''arrivée d''eau';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_arrivee_d_eau.valeur IS '[ARC_typ3] Libellé du type d''arrivée d''eau';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_arrivee_d_eau 
    (code,valeur)
VALUES
    ('311','Fontaine'),
    ('312','Robinet'),
    ('319','Autre arrivée d''eau');

