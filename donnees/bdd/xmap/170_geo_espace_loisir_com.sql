-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_espace_loisir_com
--                 . Ajout de la vue v_geo_espace_loisir_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_espace_loisir_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_espace_loisir_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_espace_loisir_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_espace_loisir_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_espace_loisir_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_espace_loisir_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Espace de loisirs (commune)                                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_espace_loisir_com;

CREATE TABLE atd16_espaces_verts.geo_espace_loisir_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Espace de loisirs"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_espace_loisir_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_espace_loisir_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_espace_loisir_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_espace_loisir_com IS '[ATD16] Table géographique contenant les espaces de loisirs';

COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Espace de loisirs"';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_espace_loisir_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_espace_loisir_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_espace_loisir_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_espace_loisir_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_espace_loisir_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_espace_loisir_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_espace_loisir_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_espace_loisir_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_espace_loisir_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';

