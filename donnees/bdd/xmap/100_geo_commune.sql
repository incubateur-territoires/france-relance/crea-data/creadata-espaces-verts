-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/05/12 : SL / Modification de la structure de la table (devient une table géographique)
--                 . Modification du nom de la table
--                 . Ajout du champ the_geom
--                 . Suppression de l'INSERT des données
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                    Table géographique : Communes de Charente (permet la création des objets non géographiques "contrat")                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_commune;

CREATE TABLE atd16_espaces_verts.geo_commune 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    insee varchar(6) NOT NULL, --[ATD16] Code insee de la commune
    libelle varchar(254), --[ATD16] Nom de la commune
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_commune PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_commune OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_commune TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_commune IS '[ATD16] Communes charentaises permettant la création des objets non géographiques "contrat"';

COMMENT ON COLUMN atd16_espaces_verts.geo_commune.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.geo_commune.insee IS '[ATD16] Code insee de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_commune.libelle IS '[ATD16] Nom de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_commune.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

