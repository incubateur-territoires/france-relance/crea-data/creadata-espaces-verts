-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/28 : SL / Création du fichier sur Git
-- 2021/05/11 : SL / Modification du nom du fichier (préfixe 000_)
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                    SCHEMA : Gestion des espaces verts v2                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP SCHEMA IF EXISTS atd16_espaces_verts;

CREATE SCHEMA atd16_espaces_verts;
ALTER SCHEMA atd16_espaces_verts OWNER TO sditecgrp;
COMMENT ON SCHEMA atd16_espaces_verts IS 'Gestion des espaces verts';



-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/11 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout de la fonction f_maj_sup_m2()
-- 2021/09/07 : SL / Ajout de la fonction f_maj_perimetre()
--                 . Ajout de la fonction f_maj_XY_coord()
-- 2021/09/08 : SL / Ajout de la fonction f_maj_long_m()
-- 2021/12/01 : SL / Modification de la fonction f_maj_sup_m2()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_date_creation();

CREATE FUNCTION atd16_espaces_verts.f_date_creation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_creation = now(); -- À la création d'un objet, le champ date_creation prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_date_creation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_date_creation() IS 'Fonction trigger permettant l''initialisation du champ date_creation';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_date_maj();

CREATE FUNCTION atd16_espaces_verts.f_date_maj()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_maj = now(); -- À la modification d'un objet, le champ date_maj prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_date_maj() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_date_maj() IS 'Fonction trigger permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_sup_m2();

CREATE FUNCTION atd16_espaces_verts.f_maj_sup_m2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    IF NEW.sup_m2 IS NULL THEN
        NEW.sup_m2 = ST_Area(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- Calcul de la surface à partir de la géométrie 
        -- reprojection car en 3857 la surface est à peu près deux fois plus grande
    END IF;
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_sup_m2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_sup_m2() IS 'Fonction trigger permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ perimetre                                                       ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_perimetre();

CREATE FUNCTION atd16_espaces_verts.f_maj_perimetre()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.perimetre = ST_Perimeter(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- Calcul du périmètre à partir de la géométrie
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_perimetre() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_perimetre() IS 'Fonction trigger permettant la mise à jour du champ perimetre';


-- ##################################################################################################################################################
-- ###                                                 Récupération des coordonnées géographiques                                                 ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_XY_coord();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_XY_coord()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.x_l93 := ST_X(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le X, on le définit en 3857, puis on le met en 2154
    NEW.y_l93 := ST_Y(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le Y, on le définit en 3857, puis on le met en 2154
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_XY_coord() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_XY_coord() IS 
    'Fonction trigger permettant la récupération des coordonnées géographiques';


-- ##################################################################################################################################################
-- ###                                                  Mise à jour de la longueur (champ long_m)                                                 ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_long_m();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_long_m()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.long_m := ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); 
    -- Calcul de la longueur de la nouvelle géométrie (reprojection car en 3857 la longueur est à peu près 50% plus grande)
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_long_m() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_long_m() IS '[ATD16] Mise à jour de la longueur';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/05/04 : SL / Modification de la longueur du varchar de "valeur" (de 50 à 80)
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/11/03 : SL / Suppression de la valeur "Accotements de voies" : à gérer dans la gestion de la voirie

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur du type de site d'espaces verts                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_site;

CREATE TABLE atd16_espaces_verts.lst_type_site 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code du type de site intégrant les objets des espaces verts
    valeur varchar(80), --[ARC] Libellé du type de site intégrant les objets des espaces verts
    CONSTRAINT pk_lst_type_site PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_site OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_site TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_site IS '[ARC] Domaine de valeur des codes du type de site intégrant les objets des espaces verts';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_site.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_site.code IS '[ARC] Code du type de site intégrant les objets des espaces verts';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_site.valeur IS '[ARC] Libellé du type de site intégrant les objets des espaces verts';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_site 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Parc, jardin, square'),
    ('3','Accompagnement de bâtiments publics'),
    ('4','Accompagnement d''habitations'),
    ('5','Accompagnement d''établissements industriels et commerciaux'),
    ('6','Enceinte sportive'),
    ('7','Cimetière'),
    ('11','Espace naturel aménagé');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur du type d'arrivée d'eau                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_arrivee_d_eau;

CREATE TABLE atd16_espaces_verts.lst_type_arrivee_d_eau 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type d'arrivée d'eau
    valeur varchar(80), --[ARC_typ3] Libellé du type d'arrivée d'eau
    CONSTRAINT pk_lst_type_arrivee_d_eau PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_arrivee_d_eau OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_arrivee_d_eau TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_arrivee_d_eau IS '[ARC_typ3] Domaine de valeur des codes du type d''arrivée d''eau';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_arrivee_d_eau.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_arrivee_d_eau.code IS '[ARC_typ3] Code du type d''arrivée d''eau';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_arrivee_d_eau.valeur IS '[ARC_typ3] Libellé du type d''arrivée d''eau';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_arrivee_d_eau 
    (code,valeur)
VALUES
    ('311','Fontaine'),
    ('312','Robinet'),
    ('319','Autre arrivée d''eau');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/10/11 : SL / Modification des enregistrements

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                       Table non géographique : Domaine de valeur du type de clôture                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_cloture;

CREATE TABLE atd16_espaces_verts.lst_type_cloture 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type de clôture
    valeur varchar(80), --[ARC_typ3] Libellé du type de clôture
    CONSTRAINT pk_lst_type_cloture PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_cloture OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_cloture TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_cloture IS '[ARC_typ3] Domaine de valeur des codes du type de clôture';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_cloture.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_cloture.code IS '[ARC_typ3] Code du type de clôture';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_cloture.valeur IS '[ARC_typ3] Libellé du type de clôture';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_cloture 
    (code,valeur)
VALUES
    ('221','Mur'),
    ('222','Grillage'),
    ('223','Palissage'),
    ('229','Autre clôture');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table non géographique : Domaine de valeur du type de cours d'eau                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_cours_d_eau;

CREATE TABLE atd16_espaces_verts.lst_type_cours_d_eau 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type de cours d'eau
    valeur varchar(80), --[ARC_typ3] Libellé du type de cours d'eau
    CONSTRAINT pk_lst_type_cours_d_eau PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_cours_d_eau OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_cours_d_eau TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_cours_d_eau IS '[ARC_typ3] Domaine de valeur des codes du type de cours d''eau';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_cours_d_eau.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_cours_d_eau.code IS '[ARC_typ3] Code du type de cours d''eau';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_cours_d_eau.valeur IS '[ARC_typ3] Libellé du type de cours d''eau';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_cours_d_eau 
    (code,valeur)
VALUES
    ('321','Rivière'),
    ('322','Ru'),
    ('329','Autre espace en eau');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/11/30 : SL / Correction du nom du fichier (lst_type_etendue_d_eau) et correction de la valeur 'Mare'

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table non géographique : Domaine de valeur du type d'étendue d'eau                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_etendue_d_eau;

CREATE TABLE atd16_espaces_verts.lst_type_etendue_d_eau 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type de point d'eau
    valeur varchar(80), --[ARC_typ3] Libellé du type de point d'eau
    CONSTRAINT pk_lst_type_etendue_d_eau PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_etendue_d_eau OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_etendue_d_eau TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_etendue_d_eau IS '[ARC_typ3] Domaine de valeur des codes du type d''étendue d''eau';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_etendue_d_eau.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_etendue_d_eau.code IS '[ARC_typ3] Code du type d''étendue d''eau';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_etendue_d_eau.valeur IS '[ARC_typ3] Libellé du type d''étendue d''eau';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_etendue_d_eau 
    (code,valeur)
VALUES
    ('323','Bassin'),
    ('324','Mare'),
    ('325','Étang'),
    ('329','Autre espace en eau');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur du type de voie de circulation                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_voie_circu;

CREATE TABLE atd16_espaces_verts.lst_type_voie_circu 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type de voie de circulation
    valeur varchar(80), --[ARC_typ3] Libellé du type de voie de circulation
    CONSTRAINT pk_lst_type_voie_circu PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_voie_circu OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_voie_circu TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_voie_circu IS '[ARC_typ3] Domaine de valeur des codes du type de voie de circulation';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_voie_circu.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_voie_circu.code IS '[ARC_typ3] Code du type de voie de circulation';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_voie_circu.valeur IS '[ARC_typ3] Libellé du type de voie de circulation';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_voie_circu 
    (code,valeur)
VALUES
    ('211','Allée'),
    ('212','Piste cyclable'),
    ('219','Autre circulation');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur du type de zone de circulation                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_zone_circu;

CREATE TABLE atd16_espaces_verts.lst_type_zone_circu 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type de zone de circulation
    valeur varchar(80), --[ARC_typ3] Libellé du type de zone de circulation
    CONSTRAINT pk_lst_type_zone_circu PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_zone_circu OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_zone_circu TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_zone_circu IS '[ARC_typ3] Domaine de valeur des codes du type de zone de circulation';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_zone_circu.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_zone_circu.code IS '[ARC_typ3] Code du type de zone de circulation';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_zone_circu.valeur IS '[ARC_typ3] Libellé du type de zone de circulation';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_zone_circu 
    (code,valeur)
VALUES
    ('213','Parking matérialisé'),
    ('214','Espace de stationnement libre'),
    ('215','Parvis, place'),
    ('219','Autre circulation');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                          Table non géographique : Domaine de valeur caractérisant la dangerosité des arbres isolés                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_danger;

CREATE TABLE atd16_espaces_verts.lst_arbre_danger 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe de dangerosité des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe de dangerosité des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_danger PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_danger OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_danger TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_danger IS '[ARC] Liste permettant de décrire la classe de dangerosité des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_danger.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_danger.code IS '[ARC] Code de la classe de dangerosité des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_danger.valeur IS '[ARC] Valeur de la classe de dangerosité des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_danger 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Aucun'),
    ('2','Dangereux'),
    ('3','Moyenne dangereux'),
    ('4','Faiblement dangereux');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table non géographique : Domaine de valeur de la forme des arbres isolés                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_forme;

CREATE TABLE atd16_espaces_verts.lst_arbre_forme 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe de forme des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe de forme des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_forme PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_forme OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_forme TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_forme IS '[ARC] Liste permettant de décrire la classe de forme des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_forme.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_forme.code IS '[ARC] Code de la classe de forme des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_forme.valeur IS '[ARC] Valeur de la classe de forme des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_forme 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Rideau'),
    ('2','Taille de contrainte'),
    ('3','Taille douce'),
    ('4','Libre'),
    ('5','Tête de chat');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur de la hauteur des arbres isolés                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_hauteur;

CREATE TABLE atd16_espaces_verts.lst_arbre_hauteur 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe de hauteur des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe de hauteur des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_hauteur PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_hauteur OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_hauteur TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_hauteur IS '[ARC] Domaine de valeur permettant de décrire la classe de hauteur des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_hauteur.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_hauteur.code IS '[ARC] Code de la classe de hauteur des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_hauteur.valeur IS '[ARC] Valeur de la classe de hauteur des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_hauteur 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Moins de 1 mètre'),
    ('2','1 à 2 mètres'),
    ('3','2 à 5 mètres'),
    ('4','5 à 10 mètres'),
    ('5','10 à 15 mètres'),
    ('6','15 à 20 mètres'),
    ('7','Plus de 20 mètres');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                               Table non géographique : Domaine de valeur de l'implantation des arbres isolés                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_implantation;

CREATE TABLE atd16_espaces_verts.lst_arbre_implantation 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe d'implantation des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe d'implantation des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_implantation PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_implantation OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_implantation TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_implantation IS '[ARC] Liste permettant de décrire la classe d''implantation des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_implantation.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_implantation.code IS '[ARC] Code de la classe d''implantation des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_implantation.valeur IS '[ARC] Valeur de la classe d''implantation des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_implantation 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Alignement'),
    ('2','Groupe/Bosquet'),
    ('3','Solitaire');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                              Table non géographique : Domaine de valeur de la nature de sol des arbres isolés                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_sol;

CREATE TABLE atd16_espaces_verts.lst_arbre_sol 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe de nature de sol des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe de nature de sol des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_sol PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_sol OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_sol TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_sol IS '[ARC] Liste permettant de décrire la classe de nature de sol des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_sol.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_sol.code IS '[ARC] Code de la classe de nature de sol des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_sol.valeur IS '[ARC] Valeur de la classe de nature de sol des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_sol 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Gazon'),
    ('2','Minéral'),
    ('3','Paillage'),
    ('4','Synthétique'),
    ('5','Terre'),
    ('6','Végétalisé'),
    ('99','Autre');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                   Table non géographique : Domaine de valeur du type de saisie de la sous-classe de précision des haies                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_haie_type_saisie;

CREATE TABLE atd16_espaces_verts.lst_haie_type_saisie 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe du type de saisie de la sous-classe de précision des objets espace vert de type haie
    valeur varchar(80), --[ARC] Valeur de la classe du type de saisie de la sous-classe de précision des objets espace vert de type haie
    CONSTRAINT pk_lst_haie_type_saisie PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_haie_type_saisie OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_haie_type_saisie TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_haie_type_saisie IS 
    '[ARC] Liste permettant de décrire le type de saisie de la sous-classe de précision des objets espace vert de type haie';

COMMENT ON COLUMN atd16_espaces_verts.lst_haie_type_saisie.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_haie_type_saisie.code IS 
    '[ARC] Code de la classe du type de saisie de la sous-classe de précision des objets espace vert de type haie';
COMMENT ON COLUMN atd16_espaces_verts.lst_haie_type_saisie.valeur IS 
    '[ARC] Valeur de la classe du type de saisie de la sous-classe de précision des objets espace vert de type haie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_haie_type_saisie 
    (code,valeur)
VALUES
    ('10','Largeur à appliquer au centre du linéaire'),
    ('20','Largeur à appliquer dans le sens de saisie'),
    ('30','Largeur à appliquer dans le sens inverse de saisie');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                  Table non géographique : Domaine de valeur décrivant la position des objets "espace vert" de type végétal                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_position;

CREATE TABLE atd16_espaces_verts.lst_position 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe décrivant la position des objets "espace vert" de type végétal
    valeur varchar(50), --[ARC] Valeur de la classe décrivant la position des objets "espace vert" de type végétal
    CONSTRAINT pk_lst_position PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_position OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_position TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_position IS '[ARC] Liste des valeurs décrivant la position des objets "espace vert" de type végétal';

COMMENT ON COLUMN atd16_espaces_verts.lst_position.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_position.code IS '[ARC] Code de la classe décrivant la position des objets "espace vert" de type végétal';
COMMENT ON COLUMN atd16_espaces_verts.lst_position.valeur IS 
    '[ARC] Valeur de la classe décrivant la position des objets "espace vert" de type végétal';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_position 
    (code,valeur)
VALUES
    ('10','Sol'),
    ('20','Hors-sol (non précisé)'),
    ('21','Pot'),
    ('22','Bac'),
    ('23','Jardinière'),
    ('24','Suspension'),
    ('29','Autre');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/29 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Table non géographique : Domaine de valeur de la domanialité                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_domanialite;

CREATE TABLE atd16_espaces_verts.lst_domanialite 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code du type de domanialité
    valeur varchar(50), --[ARC] Valeur du type de domanialité
    CONSTRAINT pk_lst_domanialite PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_domanialite OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_domanialite TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_domanialite IS '[ARC] Domaine de valeur de la domanialité';

COMMENT ON COLUMN atd16_espaces_verts.lst_domanialite.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_domanialite.code IS '[ARC] Code du type de domanialité';
COMMENT ON COLUMN atd16_espaces_verts.lst_domanialite.valeur IS '[ARC] Valeur du type de domanialité';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_domanialite 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('10','Publique'),
    ('20','Privée (non déterminé)'),
    ('21','Privée (communale)'),
    ('22','Privée (autre organisme public, HLM, ...)'),
    ('23','Privée');
    
-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                     Table non géographique : Domaine de valeur sur la qualité de l''information liée à la domanialité                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_qualite_domanialite;

CREATE TABLE atd16_espaces_verts.lst_qualite_domanialite 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la qualité de l'information liée à la domanialité
    valeur varchar(50), --[ARC] Valeur de la qualité de l'information liée à la domanialité
    CONSTRAINT pk_lst_qualite_domanialite PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_qualite_domanialite OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_qualite_domanialite TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_qualite_domanialite IS '[ARC] Domaine de valeur de la domanialité';

COMMENT ON COLUMN atd16_espaces_verts.lst_qualite_domanialite.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_qualite_domanialite.code IS '[ARC] Code de la qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.lst_qualite_domanialite.valeur IS '[ARC] Valeur de la qualité de l''information liée à la domanialité';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_qualite_domanialite 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('10','Déduite'),
    ('20','Déclarative');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/13 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                          Table non géographique : Domaine de valeur sur le type d'entretien réalisé ou à réaliser                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_entretien;

CREATE TABLE atd16_espaces_verts.lst_type_entretien 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code du type d'entretien réalisé ou à réaliser
    valeur varchar(50), --[ARC] Valeur du type d'entretien réalisé ou à réaliser
    CONSTRAINT pk_lst_type_entretien PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_entretien OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_entretien TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_entretien IS '[ARC] Domaine de valeur ddu type d''entretien réalisé ou à réaliser';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_entretien.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_entretien.code IS '[ARC] Code du type d''entretien réalisé ou à réaliser';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_entretien.valeur IS '[ARC] Valeur du type d''entretien réalisé ou à réaliser';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_entretien 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Fauchage'),
    ('2','Débroussaillage'),
    ('3','Tonte'),
    ('4','Usage de produits phytosanitaires'),
    ('5','Taille'),
    ('6','Plantation'),
    ('7','Collecte de déchets'),
    ('8','Ramassage des branches et feuilles'),
    ('9','Recépage'),
    ('10','Élagage'),
    ('99','Autre');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/13 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur sur la nature de l'intervention                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_nature_intervention;

CREATE TABLE atd16_espaces_verts.lst_nature_intervention 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la nature de l'intervention
    valeur varchar(50), --[ARC] Valeur de la nature de l'intervention
    CONSTRAINT pk_lst_nature_intervention PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_nature_intervention OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_nature_intervention TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_nature_intervention IS '[ARC] Domaine de valeur de la nature de l''intervention';

COMMENT ON COLUMN atd16_espaces_verts.lst_nature_intervention.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_nature_intervention.code IS '[ARC] Code de la nature de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.lst_nature_intervention.valeur IS '[ARC] Valeur de la nature de l''intervention';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_nature_intervention 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('10','Programmée'),
    ('20','Urgente');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/12/06 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur du type de mobilier faunistique                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_mobilier_faunistique;

CREATE TABLE atd16_espaces_verts.lst_mobilier_faunistique 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du mobilier faunistique
    valeur varchar(50), --[ATD16] Valeur du mobilier faunistique
    CONSTRAINT pk_lst_mobilier_faunistique PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_mobilier_faunistique OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_mobilier_faunistique TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_mobilier_faunistique IS '[ATD16] Domaine de valeur du type de mobilier faunistique';

COMMENT ON COLUMN atd16_espaces_verts.lst_mobilier_faunistique.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_mobilier_faunistique.code IS '[ATD16] Code du mobilier faunistique';
COMMENT ON COLUMN atd16_espaces_verts.lst_mobilier_faunistique.valeur IS '[ATD16] Valeur du mobilier faunistique';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_mobilier_faunistique 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Ruche'),
    ('02','Dorlotoire'),
    ('03','Nichoir à oiseaux');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/11/28 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table non géographique : Domaine de valeur du type d'alignement d'arbres                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_alignement_arbres;

CREATE TABLE atd16_espaces_verts.lst_type_alignement_arbres 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du type d'alignement d'arbres
    valeur varchar(50), --[ATD16] Valeur du type d'alignement d'arbres
    CONSTRAINT pk_lst_type_alignement_arbres PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_alignement_arbres OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_alignement_arbres TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_alignement_arbres IS '[ATD16] Domaine de valeur du type d''alignement d''arbres';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_alignement_arbres.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_alignement_arbres.code IS '[ATD16] Code du type d''alignement d''arbres';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_alignement_arbres.valeur IS '[ATD16] Valeur du type d''alignement d''arbres';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_alignement_arbres 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Tige'),
    ('02','Pyramide'),
    ('03','Scion'),
    ('04','Gobelet'),
    ('05','Palmette'),
    ('06','U simple'),
    ('07','U double'),
    ('08','Cordon'),
    ('09','Cordon double');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/11/28 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                         Table non géographique : Domaine de valeur du type d'arbre                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_arbre;

CREATE TABLE atd16_espaces_verts.lst_type_arbre 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du type d'arbre
    valeur varchar(50), --[ATD16] Valeur du type d'arbre
    CONSTRAINT pk_lst_type_arbre PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_arbre OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_arbre TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_arbre IS '[ATD16] Domaine de valeur du type d''arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_arbre.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_arbre.code IS '[ATD16] Code du type d''arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_arbre.valeur IS '[ATD16] Valeur du type d''arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_arbre 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Arbre d''ornement'),
    ('02','Arbre fruitier');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/11/28 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                              Table non géographique : Domaine de valeur du type de forme des arbres fruitiers                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier;

CREATE TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du type de forme
    valeur varchar(50), --[ATD16] Valeur du type de forme
    CONSTRAINT pk_lst_type_forme_arbre_fruitier PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier IS '[ATD16] Domaine de valeur du type de forme des arbres fruitiers';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_forme_arbre_fruitier.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_forme_arbre_fruitier.code IS '[ATD16] Code du type de forme';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_forme_arbre_fruitier.valeur IS '[ATD16] Valeur du type de forme';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_forme_arbre_fruitier 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Haute-tige'),
    ('02','Demi-tige'),
    ('03','Basse-tige');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/11/28 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                         Table non géographique : Domaine de valeur du type de haie                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_haie;

CREATE TABLE atd16_espaces_verts.lst_type_haie 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du type de haie
    valeur varchar(50), --[ATD16] Valeur du type de haie
    CONSTRAINT pk_lst_type_haie PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_haie OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_haie TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_haie IS '[ATD16] Domaine de valeur du type de haie';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_haie.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_haie.code IS '[ATD16] Code du type de haie';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_haie.valeur IS '[ATD16] Valeur du type de haie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_haie 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Haie champêtre');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/05 : SL / Création du fichier sur Git
-- 2021/05/11 : SL / Ajout des champs date_creation et date_maj
--                 . Ajout des triggers t_before_i_init_date_creation et t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/06/09 : SL / Ajout du champ pk_commune nécessaire pour paramétrer sw_custom_fiche
--                 . Ajout du champ numero
--                 . Ajout de la vue v_ngeo_contrat_com_annee_en_cours

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table non géographique : Contrat entretien/gestion                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.ngeo_contrat_com;

CREATE TABLE atd16_espaces_verts.ngeo_contrat_com 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    ident varchar(254), --[ATD16] Nom de l'entreprise
    numero varchar(254), --[ATD16] Numéro du contrat
    date_debut date, --[ATD16] Date de début du contrat
    date_fin date, --[ATD16] Date de fin du contrat
    insee varchar(6), --[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    pk_commune integer, --[FK][ATD16] Code de la commune (gid) (nécessaire pour paramétrer sw_custom_fiche)
    CONSTRAINT pk_ngeo_contrat_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.ngeo_contrat_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.ngeo_contrat_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.ngeo_contrat_com IS '[ATD16] Table non géographique listant les contrats de gestion des espaces verts';

COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.gid IS '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.ident IS '[ATD16] Nom de l''entreprise';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.numero IS '[ATD16] Numéro du contrat';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.date_debut IS '[ATD16] Date de début du contrat';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.date_fin IS '[ATD16] Date de fin du contrat';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.insee IS '[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.pk_commune IS 
    '[FK][ATD16] Code de la commune (gid) (nécessaire pour paramétrer sw_custom_fiche)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###           v_ngeo_contrat_com_annee_en_cours : vue listant les contrats communaux dans l'année en cours (inutile pour l'instant)            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_ngeo_contrat_com_annee_en_cours;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_ngeo_contrat_com_annee_en_cours 
    AS 
    SELECT 
        a.gid,
        a.insee,
        CONCAT(a.ident,' ','-',' ',a.numero) AS ident_numero -- Concaténation donnant la valeur visible dans la liste liée dans X'MAP
    FROM atd16_espaces_verts.ngeo_contrat_com a
    WHERE EXTRACT(YEAR FROM a.date_debut) <= EXTRACT(YEAR FROM NOW()) 
    --Où l'année de la date de début du contrat est inférieure ou égale à l'année courante
    AND EXTRACT(YEAR FROM a.date_fin) >= EXTRACT(YEAR FROM NOW());
    -- Et où l'année de la date de fin est supérieure ou égale à l'année courante

ALTER TABLE atd16_espaces_verts.v_ngeo_contrat_com_annee_en_cours OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_ngeo_contrat_com_annee_en_cours IS 
    '[ATD16] Vue listant les contrats communaux dans l''année en cours (inutile pour l''instant)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_contrat_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.ngeo_contrat_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_contrat_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_contrat_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.ngeo_contrat_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_contrat_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/04 : SL / Création du fichier sur Git
--                 . Ajout des triggers t_before_i_init_date_creation et t_before_u_date_maj

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                         Table non géographique : Liste des entretiens types sur les zones de gestion de la commune                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com;

CREATE TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)
    ident varchar(254), --[ATD16] Identifiant de l'entretien
    type_entretien integer, --[FK][ATD16] Code du type d'entretien
    periodicite varchar, --[ATD16] Périodicité moyenne de l'entretien en jours
    periodicite_annuelle integer, --[ATD16] Périodicité annuelle de l'entretien en jours
    id_zone_gestion integer, --[FK][ATD16] Code de la zone de gestion (gid) (nécessaire pour paramétrer sw_custom_fiche)
    tps_passe_moyen double precision, --[ATD16] Durée moyenne de l'entretien en heure
    descript varchar(254), --[ATD16] Description de l'entretien, outils/moyens utilisés
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    CONSTRAINT pk_ngeo_entretien_type_zone_gestion_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    '[ATD16] Table non géographique listant les entretiens types effectués sur les espaces verts par zone de gestion';

COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.gid IS 
    '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.insee IS 
    '[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.ident IS '[ATD16] Identifiant de l''entretien';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.type_entretien IS '[FK][ATD16] Code du type d''entretien';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.periodicite IS 
    '[ATD16] Périodicité moyenne de l''entretien en jours';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.periodicite_annuelle IS 
    '[ATD16] Périodicité annuelle de l''entretien en jours';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.id_zone_gestion IS 
    '[FK][ATD16] Code de la zone de gestion (gid) (nécessaire pour paramétrer sw_custom_fiche)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.tps_passe_moyen IS '[ATD16] Durée moyenne de l''entretien en heure';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.descript IS 
    '[ATD16] Description de l''entretien, outils/moyens utilisés';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com.date_maj IS 
    '[ATD16] Date de dernière mise à jour de l''objet (automatique)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/13 : SL / Création du fichier sur Git
--                 . Ajout des triggers t_before_i_init_date_creation et t_before_u_date_maj
-- 2021/11/03 : SL / Modification du nom du fichier en 050_ngeo_intervention_zone_gestion_com.sql
--                 . Modification de l'ensemble du fichier (entretien -> intervention)
--                 . Ajout du champ duree_h qui permet de noter la durée de l'intervetion en heure

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                Table non géographique : Intervention sur les zones de gestion de la commune                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com;

CREATE TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)
    ident varchar(254), --[ATD16] Identifiant de l'intervention
    type_entretien integer, --[FK][ATD16] Code du type d'entretien pratiqué lors de l'intervention
    date_debut date, --[ATD16] Date de début de l'intervention
    date_fin date, --[ATD16] Date de fin de l'intervention
    id_zone_gestion integer, --[FK][ATD16] Code de la zone de gestion (gid) (nécessaire pour paramétrer sw_custom_fiche)
    periodicite integer, --[ATD16] Périodicité de l'intervention en jours (cela détermine une nouvelle intervention)
    duree_h double precision, --[ATD16] Durée de l'intervention en heure
    descript varchar(254), --[ATD16] Description de l'intervention, outils/moyens utilisés
    observation varchar(254), --[ATD16] Observation sur l'intervention
    nature_interv integer, --[FK][ATD16] Code de la nature de l'intervention
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    CONSTRAINT pk_ngeo_intervention_zone_gestion_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    '[ATD16] Table non géographique listant les interventions effectués sur les espaces verts';

COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.gid IS 
    '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.insee IS 
    '[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.ident IS '[ATD16] Identifiant de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.type_entretien IS 
    '[FK][ATD16] Code du type d''entretien pratiqué lors de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.date_debut IS '[ATD16] Date de début de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.date_fin IS '[ATD16] Date de fin de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.id_zone_gestion IS 
    '[FK][ATD16] Code de la zone de gestion (gid) (nécessaire pour paramétrer sw_custom_fiche)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.periodicite IS 
    '[ATD16] Périodicité de l''intervention en jours (cela détermine une nouvelle intervention)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.duree_h IS '[ATD16] Durée de l''intervention en heure';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.descript IS 
    '[ATD16] Description de l''intervention, outils/moyens utilisés';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.observation IS '[ATD16] Observation sur l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.nature_interv IS '[FK][ATD16] Code de la nature de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_intervention_zone_gestion_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/05/12 : SL / Modification de la structure de la table (devient une table géographique)
--                 . Modification du nom de la table
--                 . Ajout du champ the_geom
--                 . Suppression de l'INSERT des données
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                    Table géographique : Communes de Charente (permet la création des objets non géographiques "contrat")                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_commune;

CREATE TABLE atd16_espaces_verts.geo_commune 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    insee varchar(6) NOT NULL, --[ATD16] Code insee de la commune
    libelle varchar(254), --[ATD16] Nom de la commune
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_commune PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_commune OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_commune TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_commune IS '[ATD16] Communes charentaises permettant la création des objets non géographiques "contrat"';

COMMENT ON COLUMN atd16_espaces_verts.geo_commune.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.geo_commune.insee IS '[ATD16] Code insee de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_commune.libelle IS '[ATD16] Nom de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_commune.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/05 : SL / Création du fichier sur Git
-- 2021/05/11 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout du trigger t_before_iu_maj_sup_m2
-- 2021/09/16 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_zone_gestion_com
--                 . Ajout de la vue v_geo_zone_gestion_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_zone_gestion_com
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_zone_gestion_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_zone_gestion_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_zone_gestion_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_zone_gestion_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_zone_gestion_com
-- 2021/11/03 : SL / Modification des 4 vues (entretien -> intervention)
--                 . Ajout des champs tps_planifie_estime et tps_intervention_passe
--                 . Ajout du champ nb_intervention_an
-- 2021/11/04 : SL / Modification des champs tps_intervention_passe_n et nb_intervention_n
--                 . Ajout des champs tps_intervention_passe_n_1 et nb_intervention_n_1
--                 . Ajout du champ duree_h dans les vues v_geo_zone_gestion_com_interv_en_cours et v_geo_alert_interv_a_venir_30_zone_gestion_com
-- 2021/11/08 : SL / Ajout de la vue v_geo_alert_interv_sans_duree_h_zone_gestion_com
--                 . Ajout de la vue v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com
--                 . Ajout de la vue v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com
-- 2021/12/01 : SL / Ajout de la vue v_geo_alert_interv_a_venir_7_zone_gestion_com
-- 2022/02/02 : SL / Modification de la vue v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com (échange des tables jointes)
-- 2022/02/04 : SL / Modification de la vue v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com (échange des tables jointes)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Zone de gestion (commune)                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_zone_gestion_com;

CREATE TABLE atd16_espaces_verts.geo_zone_gestion_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(50), --[SIRAP] Identifiant SIRAP / Nom de la zone de gestion
    nom_zone varchar(50), --[ARC] Nom de la zone de gestion
    sup_m2 integer, --[ARC] Superficie en m²
    type_site integer, --[FK][ARC] Identifiant du type de site
    insee_contrat varchar(6), --[FK][ATD16] Code insee de la commune permettant de sélectionner les contrats de la commune concernée (liste liée)
    idcontrat integer, --[FK][ARC_objet] Identifiant du contrat
    tps_planifie_estime double precision, --[ATD16] Temps planifié estimé de tous les entretiens dans une année en heures
    tps_intervention_passe_n double precision, --[ATD16] Somme du temps d'intervention passé dans l'année en heures
    tps_intervention_passe_n_1 double precision, --[ATD16] Somme du temps d'intervention passé dans l'année n-1 en heures
    nb_intervention_n integer, --[ATD16] Somme du nombre d'intervention passé dans l'année
    nb_intervention_n_1 integer, --[ATD16] Somme du nombre d'intervention passé dans l'année n-1
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_gestion_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_zone_gestion_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_zone_gestion_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_zone_gestion_com IS 
    '[ARC] Table contenant la géométrie des zones de gestion des espaces vert (fusion avec les site de [ARC])';

COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.ident IS '[SIRAP] Identifiant SIRAP / Nom de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.nom_zone IS '[ARC] Nom de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.sup_m2 IS '[ARC] Superficie en m²';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.type_site IS '[FK][ARC_site] Identifiant du type de site';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.insee_contrat IS 
    '[FK][ATD16] Code insee de la commune permettant de sélectionner les contrats de la commune concernée (liste liée)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.idcontrat IS '[FK][ARC_objet] Identifiant du contrat';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.tps_planifie_estime IS 
    '[ATD16] Temps planifié estimé de tous les entretiens dans une année en heures';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.tps_intervention_passe_n IS 
    '[ATD16] Somme du temps d''intervention passé dans l''année en heures';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.tps_intervention_passe_n_1 IS 
    '[ATD16] Somme du temps d''intervention passé dans l''année n-1 en heures';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.nb_intervention_n IS '[ATD16] Somme du nombre d''intervention passé dans l''année';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.nb_intervention_n_1 IS '[ATD16] Somme du nombre d''intervention passé dans l''année n-1';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_gestion_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###  v_geo_alert_interv_sans_date_debut_zone_gestion_com : vue des interventions des zones de gestion communales n'ayant pas de date de début  ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_sans_date_debut_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_sans_date_debut_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    WHERE b.date_debut IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_sans_date_debut_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_sans_date_debut_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales n''ayant pas de date de début';


-- ##################################################################################################################################################
-- ###               v_geo_zone_gestion_com_interv_en_cours : vue listant les interventions des zones de gestion communales en cours              ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_zone_gestion_com_interv_en_cours;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_zone_gestion_com_interv_en_cours 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        c.valeur AS label_type_entretien,
        b.ident,
        b.type_entretien,
        b.date_debut,
        b.date_fin,
        b.duree_h,
        b.descript,
        b.observation,
        b.nature_interv,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    LEFT JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.date_debut <= current_date AND b.date_fin >= current_date
    OR b.date_debut <= current_date AND b.date_fin IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_zone_gestion_com_interv_en_cours OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_zone_gestion_com_interv_en_cours IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales en cours';


-- ##################################################################################################################################################
-- ###   v_geo_alert_interv_a_venir_7_zone_gestion_com : vue listant les interventions des zones de gestion communales à venir dans les 7 jours   ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_7_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_7_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        b.ident,
        b.type_entretien,
        c.valeur AS label_type_entretien,
        b.date_debut,
        b.date_fin,
        b.duree_h,
        b.descript,
        b.observation,
        b.nature_interv,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    LEFT JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.date_debut > current_date AND b.date_debut < current_date+8
    ORDER BY b.date_debut DESC;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_a_venir_7_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_7_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales à venir dans les 7 jours';


-- ##################################################################################################################################################
-- ###  v_geo_alert_interv_a_venir_30_zone_gestion_com : vue listant les interventions des zones de gestion communales à venir dans les 30 jours  ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_30_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_30_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        b.ident,
        b.type_entretien,
        c.valeur AS label_type_entretien,
        b.date_debut,
        b.date_fin,
        b.duree_h,
        b.descript,
        b.observation,
        b.nature_interv,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    LEFT JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.date_debut > current_date AND b.date_debut < current_date+31
    ORDER BY b.date_debut DESC;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_a_venir_30_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_a_venir_30_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales à venir dans les 30 jours';


-- ##################################################################################################################################################
-- ###v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com : vue des interventions des zones de gestion communales dont date_fin < date_debut###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    WHERE b.date_fin < b.date_debut AND b.date_fin IS NOT NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_datefin_inf_datedebut_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions des zones de gestion communales dont date_fin < date_debut';


-- ##################################################################################################################################################
-- ### v_geo_alert_interv_sans_duree_h_zone_gestion_com : vue des interventions passées des z. de gestion communales dont duree_h n'est pas rempli###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_interv_sans_duree_h_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_interv_sans_duree_h_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_intervention,
        c.valeur AS label_type_entretien,
        b.ident,
        b.type_entretien,
        b.date_debut,
        b.date_fin,
        b.duree_h,
        b.descript,
        b.observation,
        b.nature_interv,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_gestion_com a
    LEFT JOIN atd16_espaces_verts.ngeo_intervention_zone_gestion_com b ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.date_fin <= current_date
    AND b.duree_h IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_interv_sans_duree_h_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_interv_sans_duree_h_zone_gestion_com IS 
    '[ATD16] Vue listant les interventions passées des zones de gestion communales dont le champ duree_h n''est pas renseigné';


-- ##################################################################################################################################################
-- ###   v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com : vue des entretiens dont le champ periodicite_annuelle n'est pas rempli   ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_entretien,
        c.valeur AS label_type_entretien,
        b.ident,
        b.type_entretien,
        b.periodicite,
        b.periodicite_annuelle,
        b.tps_passe_moyen,
        b.descript,
        a.the_geom
    FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com b
    LEFT JOIN atd16_espaces_verts.geo_zone_gestion_com a ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.periodicite_annuelle IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_periodicite_annuelle_z_gestion_com IS 
    '[ATD16] Vue listant les entretiens des zones de gestion communales dont le champ periodicite_annuelle n''est pas renseigné';


-- ##################################################################################################################################################
-- ###      v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com : vue des entretiens dont le champ tps_passe_moyen n'est pas rempli       ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com 
    AS 
    SELECT 
        a.gid,
        a.insee,
        b.gid AS id_entretien,
        c.valeur AS label_type_entretien,
        b.ident,
        b.type_entretien,
        b.periodicite,
        b.periodicite_annuelle,
        b.tps_passe_moyen,
        b.descript,
        a.the_geom
    FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com b
    LEFT JOIN atd16_espaces_verts.geo_zone_gestion_com a ON a.gid = b.id_zone_gestion 
    LEFT JOIN atd16_espaces_verts.lst_type_entretien c ON b.type_entretien = c.code 
    WHERE b.tps_passe_moyen IS NULL;

ALTER TABLE atd16_espaces_verts.v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com IS 
    '[ATD16] Vue listant les entretiens des zones de gestion communales dont le champ tps_passe_moyen n''est pas renseigné';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_zone_gestion_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_zone_gestion_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_zone_gestion_com;

CREATE TRIGGER t_before_iu_maj_sup_m2
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_zone_gestion_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_sup_m2();
    
COMMENT ON TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/06 : SL / Création du fichier sur Git
-- 2021/09/06 : SL / Ajout de la fonction f_maj_idzone()
-- 2021/11/04 : SL / Modification du nom de la fonction f_maj_idzone_com()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) dépendante(s)                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_idzone_com();

CREATE FUNCTION atd16_espaces_verts.f_maj_idzone_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    SELECT gid -- on sélectionne le champ gid
    FROM atd16_espaces_verts.geo_zone_gestion_com AS zgest -- de la table geo_zone_gestion_com
    WHERE ST_Intersects(NEW.the_geom, zgest.the_geom) -- qui intersecte le nouvel objet créé
    INTO NEW.idzone; -- et on insère la valeur de gid dans le nouveau champ idzone
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_idzone_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_idzone_com() IS 'Fonction trigger permettant la mise à jour du champ idzone';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout du trigger t_before_iu_maj_sup_m2
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_perimetre
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_zone_boisee_com
--                 . Ajout de la vue v_geo_zone_boisee_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_zone_boisee_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_zone_boisee_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_zone_boisee_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_zone_boisee_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_zone_boisee_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_zone_boisee_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/02 : SL / Ajout de la vue v_geo_zone_boisee_mellifere_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                  Table géographique : Zone boisée (commune)                                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_zone_boisee_com;

CREATE TABLE atd16_espaces_verts.geo_zone_boisee_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    sup_m2 integer, --[ARC_poly] Surface en m² (automatique)
    perimetre integer, --[ARC_poly] Périmètre en mètre (automatique)
    position integer, --[FK][ARC_vegetal] Position de la zone boisée
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Zone boisée"
    flore_mellifere boolean, --[ATD16] Caractère mellifère de la zone boisée
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_boisee_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_zone_boisee_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_zone_boisee_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_zone_boisee_com IS '[ATD16] Table géographique contenant les zones boisées';

COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.sup_m2 IS '[ARC_poly] Surface en m² (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.perimetre IS '[ARC_poly] Périmètre en mètre (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.position IS '[ARC_vegetal] Position de la zone boisée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Zone boisée"';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.flore_mellifere IS '[ATD16] Caractère mellifère de la zone boisée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                     v_geo_zone_boisee_mellifere_com : vue des zones boisées mellifères                                     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_zone_boisee_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_zone_boisee_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_zone_boisee_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_zone_boisee_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_zone_boisee_mellifere_com IS 
    '[ATD16] Vue listant les zones boisées mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_zone_boisee_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_zone_boisee_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_zone_boisee_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_zone_boisee_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_zone_boisee_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_zone_boisee_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_zone_boisee_com;

CREATE TRIGGER t_before_iu_maj_sup_m2
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_zone_boisee_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_sup_m2();
    
COMMENT ON TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_zone_boisee_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_zone_boisee_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_zone_boisee_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_zone_boisee_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ perimetre                                                       ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_perimetre ON atd16_espaces_verts.geo_zone_boisee_com;

CREATE TRIGGER t_before_iu_maj_perimetre
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_zone_boisee_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_perimetre();
    
COMMENT ON TRIGGER t_before_iu_maj_perimetre ON atd16_espaces_verts.geo_zone_boisee_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ perimetre';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/08 : SL / Ajout du trigger t_before_iu_maj_long_m
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_alignement_arbres_com
--                 . Ajout de la vue v_geo_alignement_arbres_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_alignement_arbres_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_align_arbres_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alignement_arbres_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_alignement_arbres_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_alignement_arbres_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_align_arbres_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/03 : SL / Ajout de la vue v_geo_alignement_arbres_mellifere_com
-- 2022/11/28 : SL / Ajout du champ type_alignement_arbres

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table géographique : Alignement d'arbres (commune)                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_alignement_arbres_com;

CREATE TABLE atd16_espaces_verts.geo_alignement_arbres_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    larg_cm integer, --[ARC_line] Largeur de l'alignement d'arbres en centimètre
    long_m integer, --[ARC_line] Longueur de l'alignement d'arbres en mètre
    position integer, --[FK][ARC_vegetal] Position de l'alignement d'arbres
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Alignement d'arbres"
    type_alignement_arbres integer, --[FK][ATD16] Type d'alignement d'arbres
    flore_mellifere boolean, --[ATD16] Caractère mellifère de l'alignement d'arbres
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_alignement_arbres_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_alignement_arbres_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_alignement_arbres_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_alignement_arbres_com IS '[ATD16] Table géographique contenant les alignements d''arbres (au moins 5)';

COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.gid IS 
    '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.larg_cm IS '[ARC_line] Largeur de l''alignement d''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.long_m IS '[ARC_line] Longueur de l''alignement d''arbre en mètre';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.position IS '[ARC_vegetal] Position de l''alignement d''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Alignement d''arbre"';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.type_alignement_arbres IS '[FK][ATD16] Type de l''alignement d''arbres';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.flore_mellifere IS '[ATD16] Caractère mellifère de l''alignement d''arbres';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_alignement_arbres_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                               v_geo_alignement_arbres_mellifere_com : vue des alignements d'arbres mellifères                              ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_alignement_arbres_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_alignement_arbres_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.type_alignement_arbres,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_alignement_arbres_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_alignement_arbres_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_alignement_arbres_mellifere_com IS 
    '[ATD16] Vue listant les alignements d''arbres mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_alignement_arbres_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_alignement_arbres_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_alignement_arbres_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_alignement_arbres_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_alignement_arbres_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_alignement_arbres_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_alignement_arbres_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_alignement_arbres_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_alignement_arbres_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ long_m                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_long_m ON atd16_espaces_verts.geo_alignement_arbres_com;

CREATE TRIGGER t_before_iu_maj_long_m
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_alignement_arbres_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_long_m();
    
COMMENT ON TRIGGER t_before_iu_maj_long_m ON atd16_espaces_verts.geo_alignement_arbres_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ long_m';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/04 : SL / Création du fichier sur Git
-- 2021/05/05 : SL / Suppression du champ idcontrat
--                 . Changement du nom de la table
-- 2021/05/06 : SL / Suppression du champ idsite
--                 . Ajout du champ insee_contrat
--                 . Ajout du champ idcontrat
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_arbre_isole_com
--                 . Ajout de la vue v_geo_arbre_isole_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_arbre_isole_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_arbre_isole_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_arbre_isole_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_arbre_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_arbre_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_arbre_isole_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/11/30 : SL / Ajout des champs age et date_plantation
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/02 : SL / Ajout de la vue v_geo_arbre_isole_mellifere_com
-- 2022/11/28 : SL / Ajout des champs type_arbre et type_forme_arbre_fr

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                 Table géographique : Arbre isolé (commune)                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_arbre_isole_com;

CREATE TABLE atd16_espaces_verts.geo_arbre_isole_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    position integer, --[FK][ARC_vegetal] Position de l'arbre
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Arbre isolé" / Champ nom pour [ARC_arbre]
    type_arbre integer, --[FK][ATD16] Type d'arbre
    type_forme_arbre_fr integer, --[FK][ATD16] Type de forme pour l'arbre fruitier
    flore_mellifere boolean, --[ATD16] Caractère mellifère de l'arbre
    genre varchar(20), --[ARC_arbre] 
    espece varchar(20), --[ARC_arbre] 
    age varchar(20), --[ATD16] Age de l'arbre
    date_plantation date, --[ATD16] Date de plantation de l'arbre
    hauteur integer, --[FK][ARC_arbre] Hauteur de l'arbre
    circonf double precision, --[ARC_arbre] 
    forme integer, --[FK][ARC_arbre] Forme de l'arbre
    etat_gen varchar(30), --[ARC_arbre] 
    implant integer, --[FK][ARC_arbre] Implantation de l'arbre
    remarq varchar(3), --[ARC_arbre] 
    malad_obs varchar(3), --[ARC_arbre] 
    malad_nom varchar(80), --[ARC_arbre] 
    danger integer, --[FK][ARC_arbre] Dangerosité
    natur_sol integer, --[FK][ARC_arbre] Nature du sol
    envnmt_obs varchar(254), --[ARC_arbre] 
    utilis_obs varchar(254), --[ARC_arbre] 
    cplt_fic_1 varchar(254), --[ARC_arbre] 
    cplt_fic_2 varchar(254), --[ARC_arbre] 
    gps_date date, --[ARC_arbre] 
    gnss_heigh double precision, --[ARC_arbre]
    vert_prec double precision, --[ARC_arbre]
    horz_prec double precision, --[ARC_arbre]
    northing double precision, --[ARC_arbre]
    easting double precision, --[ARC_arbre]
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_arbre_isole_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_arbre_isole_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_arbre_isole_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_arbre_isole_com IS '[ATD16] Table géographique contenant les arbres isolés';

COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.position IS '[ARC_vegetal] Position de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.x_l93 IS '[ARC_pct] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.y_l93 IS '[ARC_pct] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.ident IS 
    '[SIRAP] Identificaion de l''objet / Par défaut "Arbre isolé" / Champ nom pour [ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.type_arbre IS '[FK][ATD16] Type d''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.type_forme_arbre_fr IS '[FK][ATD16] Type de forme pour l''arbre fruitier';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.flore_mellifere IS '[ATD16] Caractère mellifère de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.genre IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.espece IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.age IS '[ATD16] Age de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.date_plantation IS '[ATD16] Date de plantation de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.hauteur IS '[FK][ARC_arbre] Hauteur de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.circonf IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.forme IS '[FK][ARC_arbre] Forme de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.etat_gen IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.implant IS '[FK][ARC_arbre] Implantation de l''arbre';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.remarq IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.malad_obs IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.malad_nom IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.danger IS '[FK][ARC_arbre] Dangerosité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.natur_sol IS '[FK][ARC_arbre] Nature du sol';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.envnmt_obs IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.utilis_obs IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.cplt_fic_1 IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.cplt_fic_2 IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.gps_date IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.gnss_heigh IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.vert_prec IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.horz_prec IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.northing IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.easting IS '[ARC_arbre]';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbre_isole_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                     v_geo_arbre_isole_mellifere_com : vue des arbres isolés mellifères                                     ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_arbre_isole_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_arbre_isole_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.type_arbre,
        a.type_forme_arbre_fr,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.genre,
        a.espece,
        a.age,
        a.date_plantation,
        a.hauteur,
        a.circonf,
        a.forme,
        a.etat_gen,
        a.implant,
        a.danger,
        a.natur_sol,
        a.remarq,
        a.the_geom
    FROM atd16_espaces_verts.geo_arbre_isole_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_arbre_isole_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_arbre_isole_mellifere_com IS 
    '[ATD16] Vue listant les arbres isolés mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_arbre_isole_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_arbre_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_arbre_isole_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_arbre_isole_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_arbre_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_arbre_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_arbre_isole_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_arbre_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_arbre_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_arbre_isole_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_arbre_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_arbre_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout du trigger t_before_iu_maj_sup_m2
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_perimetre
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_massif_arbustif_com
--                 . Ajout de la vue v_geo_massif_arbustif_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_massif_arbustif_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_massif_arbustif_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_massif_arbustif_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_massif_arbustif_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_massif_arbustif_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_massif_arbustif_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/03 : SL / Ajout de la vue v_geo_massif_arbustif_mellifere_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                Table géographique : Massif arbustif (commune)                                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_massif_arbustif_com;

CREATE TABLE atd16_espaces_verts.geo_massif_arbustif_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    sup_m2 integer, --[ARC_poly] Surface en m² (automatique)
    perimetre integer, --[ARC_poly] Périmètre en mètre (automatique)
    position integer, --[FK][ARC_vegetal] Position du massif arbustif
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Massif arbustif"
    flore_mellifere boolean, --[ATD16] Caractère mellifère du massif
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_massif_arbustif_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_massif_arbustif_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_massif_arbustif_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_massif_arbustif_com IS '[ATD16] Table géographique contenant les massifs arbustifs (surface > 5m²)';

COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.sup_m2 IS '[ARC_poly] Surface en m² (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.perimetre IS '[ARC_poly] Périmètre en mètre (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.position IS '[ARC_vegetal] Position du massif arbustif';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Massif arbustif"';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.flore_mellifere IS '[ATD16] Caractère mellifère du massif';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_arbustif_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                 v_geo_massif_arbustif_mellifere_com : vue des massifs arbustifs mellifères                                 ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_massif_arbustif_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_massif_arbustif_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_massif_arbustif_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_massif_arbustif_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_massif_arbustif_mellifere_com IS 
    '[ATD16] Vue listant les massifs arbustifs mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_massif_arbustif_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_massif_arbustif_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_massif_arbustif_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_massif_arbustif_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_massif_arbustif_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_massif_arbustif_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_massif_arbustif_com;

CREATE TRIGGER t_before_iu_maj_sup_m2
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_massif_arbustif_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_sup_m2();
    
COMMENT ON TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_massif_arbustif_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_massif_arbustif_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_massif_arbustif_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_massif_arbustif_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ perimetre                                                       ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_perimetre ON atd16_espaces_verts.geo_massif_arbustif_com;

CREATE TRIGGER t_before_iu_maj_perimetre
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_massif_arbustif_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_perimetre();
    
COMMENT ON TRIGGER t_before_iu_maj_perimetre ON atd16_espaces_verts.geo_massif_arbustif_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ perimetre';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/08 : SL / Ajout du trigger t_before_iu_maj_long_m
-- 2021/09/23 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_haie_com
--                 . Ajout de la vue v_geo_haie_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_haie_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_haie_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_haie_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_haie_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_haie_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_haie_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/03 : SL / Ajout de la vue v_geo_haie_mellifere_com
-- 2022/11/28 : SL / Ajout du champ type_haie

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                      Table géographique : Haie (commune)                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_haie_com;

CREATE TABLE atd16_espaces_verts.geo_haie_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    larg_cm integer, --[ARC_line] Largeur de la haie en centimètre
    long_m integer, --[ARC_line] Longueur de la haie en mètre
    position integer, --[FK][ARC_vegetal] Position de la haie
    typsai integer, --[FK][ARC_haie] Type de saisie de la sous-classe de précision des haies
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Haie"
    type_haie integer, --[FK][ATD16] Type de la haie
    flore_mellifere boolean, --[ATD16] Caractère mellifère de la haie
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_haie_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_haie_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_haie_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_haie_com IS '[ATD16] Table géographique contenant les haies d''arbustes (au moins 5)';

COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.srcgeom_sai IS '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.larg_cm IS '[ARC_line] Largeur de la haie en centimètre';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.long_m IS '[ARC_line] Longueur de la haie en mètre';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.position IS '[ARC_vegetal] Position de la haie';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.typsai IS '[FK][ARC_haie] Type de saisie de la sous-classe de précision des haies';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Haie"';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.type_haie IS '[FK][ATD16] Type de la haie';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.flore_mellifere IS '[ATD16] Caractère mellifère de la haie';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_haie_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                            v_geo_haie_mellifere_com : vue des haies mellifères                                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_haie_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_haie_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.type_haie,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_haie_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_haie_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_haie_mellifere_com IS 
    '[ATD16] Vue listant les haies mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_haie_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_haie_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_haie_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_haie_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_haie_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_haie_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_haie_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_haie_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_haie_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ long_m                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_long_m ON atd16_espaces_verts.geo_haie_com;

CREATE TRIGGER t_before_iu_maj_long_m
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_haie_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_long_m();
    
COMMENT ON TRIGGER t_before_iu_maj_long_m ON atd16_espaces_verts.geo_haie_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ long_m';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_arbuste_isole_com
--                 . Ajout de la vue v_geo_arbuste_isole_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_arbuste_isole_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_arbuste_isole_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_arbuste_isole_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_arbuste_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_arbuste_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_arbuste_isole_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/02 : SL / Ajout de la vue v_geo_arbuste_isole_mellifere_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                Table géographique : Arbuste isolé (commune)                                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_arbuste_isole_com;

CREATE TABLE atd16_espaces_verts.geo_arbuste_isole_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    position integer, --[FK][ARC_vegetal] Position de l'arbuste
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Arbuste isolé"
    flore_mellifere boolean, --[ATD16] Caractère mellifère de l'arbuste
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_arbuste_isole_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_arbuste_isole_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_arbuste_isole_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_arbuste_isole_com IS '[ATD16] Table géographique contenant les arbustes isolés';

COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.gid IS 
    '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.position IS '[ARC_vegetal] Position de l''arbuste';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.x_l93 IS '[ARC_pct] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.y_l93 IS '[ARC_pct] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Arbuste isolé"';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.flore_mellifere IS '[ATD16] Caractère mellifère de l''arbuste';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_arbuste_isole_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                   v_geo_arbuste_isole_mellifere_com : vue des arbustes isolés mellifères                                   ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_arbuste_isole_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_arbuste_isole_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_arbuste_isole_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_arbuste_isole_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_arbuste_isole_mellifere_com IS 
    '[ATD16] Vue listant les arbustes isolés mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_arbuste_isole_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_arbuste_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_arbuste_isole_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_arbuste_isole_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_arbuste_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_arbuste_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_arbuste_isole_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_arbuste_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_arbuste_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_arbuste_isole_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_arbuste_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_arbuste_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout du trigger t_before_iu_maj_sup_m2
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_perimetre
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_espace_enherbe_com
--                 . Ajout de la vue v_geo_espace_enherbe_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_espace_enherbe_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_espace_enherbe_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_espace_enherbe_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_espace_enherbe_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_espace_enherbe_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_espace_enherbe_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                Table géographique : Espace enherbé (commune)                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_espace_enherbe_com;

CREATE TABLE atd16_espaces_verts.geo_espace_enherbe_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    sup_m2 integer, --[ARC_poly] Surface en m² (automatique)
    perimetre integer, --[ARC_poly] Périmètre en mètre (automatique)
    position integer, --[FK][ARC_vegetal] Position de l'espace enherbé
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Espace enherbé"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_espace_enherbe_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_espace_enherbe_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_espace_enherbe_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_espace_enherbe_com IS '[ATD16] Table géographique contenant les espaces enherbés (surface > 5m²)';

COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.sup_m2 IS '[ARC_poly] Surface en m² (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.perimetre IS '[ARC_poly] Périmètre en mètre (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.position IS '[ARC_vegetal] Position de l''espace enherbé';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Espace enherbé"';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_enherbe_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_espace_enherbe_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_espace_enherbe_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_espace_enherbe_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_espace_enherbe_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_espace_enherbe_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_espace_enherbe_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_espace_enherbe_com;

CREATE TRIGGER t_before_iu_maj_sup_m2
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_espace_enherbe_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_sup_m2();
    
COMMENT ON TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_espace_enherbe_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_espace_enherbe_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_espace_enherbe_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_espace_enherbe_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ perimetre                                                       ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_perimetre ON atd16_espaces_verts.geo_espace_enherbe_com;

CREATE TRIGGER t_before_iu_maj_perimetre
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_espace_enherbe_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_perimetre();
    
COMMENT ON TRIGGER t_before_iu_maj_perimetre ON atd16_espaces_verts.geo_espace_enherbe_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ perimetre';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout du trigger t_before_iu_maj_sup_m2
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_perimetre
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_massif_fleuri_com
--                 . Ajout de la vue v_geo_massif_fleuri_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_massif_fleuri_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_massif_fleuri_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_massif_fleuri_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_massif_fleuri_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_massif_fleuri_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_massif_fleuri_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/03 : SL / Ajout de la vue v_geo_massif_fleuri_mellifere_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                 Table géographique : Massif fleuri (commune)                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_massif_fleuri_com;

CREATE TABLE atd16_espaces_verts.geo_massif_fleuri_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    sup_m2 integer, --[ARC_poly] Surface en m² (automatique)
    perimetre integer, --[ARC_poly] Périmètre en mètre (automatique)
    position integer, --[FK][ARC_vegetal] Position du massif fleuri
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Massif fleuri"
    flore_mellifere boolean, --[ATD16] Caractère mellifère du massif
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_massif_fleuri_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_massif_fleuri_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_massif_fleuri_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_massif_fleuri_com IS '[ATD16] Table géographique contenant les massifs fleuris (surface > 5m²)';

COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.gid IS 
    '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.sup_m2 IS '[ARC_poly] Surface en m² (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.perimetre IS '[ARC_poly] Périmètre en mètre (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.position IS '[ARC_vegetal] Position du massif fleuri';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Massif fleuri"';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.flore_mellifere IS '[ATD16] Caractère mellifère du massif';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_massif_fleuri_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                   v_geo_massif_fleuri_mellifere_com : vue des massifs fleuris mellifères                                   ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_massif_fleuri_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_massif_fleuri_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_massif_fleuri_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_massif_fleuri_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_massif_fleuri_mellifere_com IS 
    '[ATD16] Vue listant les massifs fleuris mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_massif_fleuri_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_massif_fleuri_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_massif_fleuri_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_massif_fleuri_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_massif_fleuri_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_massif_fleuri_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_massif_fleuri_com;

CREATE TRIGGER t_before_iu_maj_sup_m2
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_massif_fleuri_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_sup_m2();
    
COMMENT ON TRIGGER t_before_iu_maj_sup_m2 ON atd16_espaces_verts.geo_massif_fleuri_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_massif_fleuri_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_massif_fleuri_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_massif_fleuri_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';



-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ perimetre                                                       ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_perimetre ON atd16_espaces_verts.geo_massif_fleuri_com;

CREATE TRIGGER t_before_iu_maj_perimetre
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_massif_fleuri_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_perimetre();
    
COMMENT ON TRIGGER t_before_iu_maj_perimetre ON atd16_espaces_verts.geo_massif_fleuri_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ perimetre';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_fleuri_isole_com
--                 . Ajout de la vue v_geo_fleuri_isole_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_fleuri_isole_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_fleuri_isole_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_fleuri_isole_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_fleuri_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_fleuri_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_fleuri_isole_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat
-- 2021/12/01 : SL / Ajout du champ flore_mellifere
-- 2021/12/02 : SL / Ajout de la vue v_geo_fleuri_isole_mellifere_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                Table géographique : Fleuri isolé (commune)                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_fleuri_isole_com;

CREATE TABLE atd16_espaces_verts.geo_fleuri_isole_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    position integer, --[FK][ARC_vegetal] Position du fleuri
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Fleuri isolé"
    flore_mellifere boolean, --[ATD16] Caractère mellifère du fleuri
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_fleuri_isole_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_fleuri_isole_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_fleuri_isole_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_fleuri_isole_com IS '[ATD16] Table géographique contenant les fleuris isolés (surface < 5m²)';

COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.position IS '[ARC_vegetal] Position du fleuri';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.x_l93 IS '[ARC_pct] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.y_l93 IS '[ARC_pct] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Fleuri isolé"';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.flore_mellifere IS '[ATD16] Caractère mellifère du fleuri';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_fleuri_isole_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                    v_geo_fleuri_isole_mellifere_com : vue des fleuris isolés mellifères                                    ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_fleuri_isole_mellifere_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_fleuri_isole_mellifere_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.flore_mellifere,
        a.position,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_fleuri_isole_com a
    WHERE a.flore_mellifere = 'true';

ALTER TABLE atd16_espaces_verts.v_geo_fleuri_isole_mellifere_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_fleuri_isole_mellifere_com IS 
    '[ATD16] Vue listant les fleuris isolés mellifères';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_fleuri_isole_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_fleuri_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_fleuri_isole_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_fleuri_isole_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_fleuri_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_fleuri_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_fleuri_isole_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_fleuri_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_fleuri_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_fleuri_isole_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_fleuri_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_fleuri_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/11 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/23 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_etendue_d_eau_com
--                 . Ajout de la vue v_geo_etendue_d_eau_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_etendue_d_eau_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_etendue_d_eau_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_etendue_d_eau_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_etendue_d_eau_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_etendue_d_eau_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_etendue_d_eau_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                 Table géographique : Étendue d'eau (commune)                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_etendue_d_eau_com;

CREATE TABLE atd16_espaces_verts.geo_etendue_d_eau_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    type_etendue_d_eau integer, --[FK][ARC_typ3] Type d'étendue d'eau
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Espace de loisirs"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_etendue_d_eau_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_etendue_d_eau_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_etendue_d_eau_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_etendue_d_eau_com IS '[ATD16] Table géographique contenant les étendues d''eau (surface > 5m²)';

COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.type_etendue_d_eau IS '[FK][ARC_typ3] Type d''étendue d''eau';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Étendue d''eau"';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_etendue_d_eau_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_etendue_d_eau_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_etendue_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_etendue_d_eau_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_etendue_d_eau_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_etendue_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_etendue_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_etendue_d_eau_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_etendue_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_etendue_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/11 : SL / Création du fichier sur Git
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_cours_d_eau_com
--                 . Ajout de la vue v_geo_cours_d_eau_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_cours_d_eau_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_cours_d_eau_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_cours_d_eau_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_cours_d_eau_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_cours_d_eau_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_cours_d_eau_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                  Table géographique : Cours d'eau (commune)                                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_cours_d_eau_com;

CREATE TABLE atd16_espaces_verts.geo_cours_d_eau_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    larg_cm integer, --[ARC_line] Largeur du cours d'eau en centimètre
    type_cours_d_eau integer, --[ARC_typ3] Type de cours d'eau
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Cours d'eau"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_cours_d_eau_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_cours_d_eau_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_cours_d_eau_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_cours_d_eau_com IS '[ATD16] Table géographique contenant les cours d''eau';

COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.larg_cm IS '[ARC_line] Largeur du cours d''eau en centimètre';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.type_cours_d_eau IS '[ARC_typ3] Type de cours d''eau';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Cours d''eau"';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_cours_d_eau_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_cours_d_eau_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_cours_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_cours_d_eau_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_cours_d_eau_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_cours_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_cours_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_cours_d_eau_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_cours_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_cours_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/11 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_point_d_eau_com
--                 . Ajout de la vue v_geo_point_d_eau_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_point_d_eau_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_point_d_eau_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_point_d_eau_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_point_d_eau_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_point_d_eau_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_point_d_eau_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                 Table géographique : Point d'eau (commune)                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_point_d_eau_com;

CREATE TABLE atd16_espaces_verts.geo_point_d_eau_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    type_point_d_eau integer, --[ARC_typ3] Type de point d'eau
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Point d'eau"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_point_d_eau_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_point_d_eau_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_point_d_eau_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_point_d_eau_com IS '[ATD16] Table géographique contenant les points d''eau';

COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.x_l93 IS '[ARC_pct] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.y_l93 IS '[ARC_pct] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.type_point_d_eau IS '[ARC_typ3] Type de points d''eau';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Point d''eau"';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_point_d_eau_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_point_d_eau_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_point_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_point_d_eau_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_point_d_eau_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_point_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_point_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_point_d_eau_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_point_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_point_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_point_d_eau_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_point_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_point_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
-- 2021/09/23 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_arrivee_d_eau_com
--                 . Ajout de la vue v_geo_arrivee_d_eau_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_arrivee_d_eau_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_arrivee_d_eau_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_arrivee_d_eau_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_arrivee_d_eau_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_arrivee_d_eau_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_arrivee_d_eau_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                Table géographique : Arrivée d'eau (commune)                                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_arrivee_d_eau_com;

CREATE TABLE atd16_espaces_verts.geo_arrivee_d_eau_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    type_arrivee_d_eau integer, --[ARC_typ3] Type d'arrivée d'eau
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Arrivée d'eau"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_arrivee_d_eau_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_arrivee_d_eau_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_arrivee_d_eau_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_arrivee_d_eau_com IS '[ATD16] Table géographique contenant les arrivées d''eau';

COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.x_l93 IS '[ARC_pct] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.y_l93 IS '[ARC_pct] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.type_arrivee_d_eau IS '[ARC_typ3] Type d''arrivée d''eau';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Arrivée d''eau"';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_arrivee_d_eau_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_arrivee_d_eau_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_arrivee_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_arrivee_d_eau_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_arrivee_d_eau_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_arrivee_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_arrivee_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_arrivee_d_eau_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_arrivee_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_arrivee_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_arrivee_d_eau_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_arrivee_d_eau_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_arrivee_d_eau_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_espace_loisir_com
--                 . Ajout de la vue v_geo_espace_loisir_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_espace_loisir_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_espace_loisir_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_espace_loisir_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_espace_loisir_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_espace_loisir_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_espace_loisir_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Espace de loisirs (commune)                                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_espace_loisir_com;

CREATE TABLE atd16_espaces_verts.geo_espace_loisir_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Espace de loisirs"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_espace_loisir_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_espace_loisir_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_espace_loisir_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_espace_loisir_com IS '[ATD16] Table géographique contenant les espaces de loisirs';

COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Espace de loisirs"';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_espace_loisir_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_espace_loisir_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_espace_loisir_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_espace_loisir_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_espace_loisir_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_espace_loisir_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_espace_loisir_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_espace_loisir_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_espace_loisir_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_espace_loisir_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
-- 2021/09/22 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_loisir_isole_com
--                 . Ajout de la vue v_geo_loisir_isole_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_loisir_isole_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_loisir_isole_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_loisir_isole_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_loisir_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_loisir_isole_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_loisir_isole_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                Table géographique : Loisir isolé (commune)                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_loisir_isole_com;

CREATE TABLE atd16_espaces_verts.geo_loisir_isole_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Loisir isolé"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_loisir_isole_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_loisir_isole_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_loisir_isole_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_loisir_isole_com IS '[ATD16] Table géographique contenant les loisirs isolés';

COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.x_l93 IS '[ARC_pct] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.y_l93 IS '[ARC_pct] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Loisir isolé"';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_loisir_isole_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_loisir_isole_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_loisir_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_loisir_isole_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_loisir_isole_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_loisir_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_loisir_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_loisir_isole_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_loisir_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_loisir_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_loisir_isole_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_loisir_isole_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_loisir_isole_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/06 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/23 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_zone_circu_com
--                 . Ajout de la vue v_geo_zone_circu_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_zone_circu_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_zone_circu_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_zone_circu_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_zone_circu_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_zone_circu_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_zone_circu_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table géographique : Zone de circulation (commune)                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_zone_circu_com;

CREATE TABLE atd16_espaces_verts.geo_zone_circu_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    type_zone_circu integer, --[ARC_typ3] Type de zone de circulation
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Zone de circulation"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_zone_circu_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_zone_circu_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_zone_circu_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_zone_circu_com IS '[ATD16] Table géographique contenant les zones de circulation';

COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.type_zone_circu IS '[ARC_typ3] Type de zone du circulation';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Zone de circulation"';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_circu_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_zone_circu_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_zone_circu_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_zone_circu_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_zone_circu_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_zone_circu_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_zone_circu_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';



-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_zone_circu_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_zone_circu_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_zone_circu_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/17 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/23 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_voie_circu_com
--                 . Ajout de la vue v_geo_voie_circu_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_voie_circu_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_voie_circu_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_voie_circu_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_voie_circu_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_voie_circu_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_voie_circu_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table géographique : Voie de circulation (commune)                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_voie_circu_com;

CREATE TABLE atd16_espaces_verts.geo_voie_circu_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    larg_cm integer, --[ARC_line] Largeur de la voie de circulation en centimètre
    type_voie_circu integer, --[ARC_typ3] Type de voie de circulation
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Voie de circulation"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_voie_circu_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_voie_circu_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_voie_circu_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_voie_circu_com IS '[ATD16] Table géographique contenant les voies de circulation';

COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.larg_cm IS '[ARC_line] Largeur de la voie de circulation en centimètre';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.type_voie_circu IS '[ARC_typ3] Type de voie de circulation';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Voie de circulation"';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_voie_circu_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_voie_circu_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_voie_circu_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_voie_circu_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_voie_circu_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_voie_circu_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_voie_circu_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_voie_circu_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_voie_circu_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_voie_circu_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/07 : SL / Création du fichier sur Git
-- 2021/05/18 : SL / Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/09/07 : SL / Ajout du trigger t_before_iu_maj_idzone
-- 2021/09/23 : SL / Ajout de la vue v_geo_alert_entretien_sans_date_debut_cloture_com
--                 . Ajout de la vue v_geo_cloture_com_entretien_en_cours
--                 . Ajout de la vue v_geo_alert_entretien_a_venir_30_cloture_com
--                 . Ajout de la vue v_geo_alert_entretien_datefin_inf_datedebut_cloture_com
-- 2021/10/19 : SL / Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_cloture_com_entretien_en_cours
--                 . Ajout du champ c.valeur (label du type d'entretien) dans la vue v_geo_alert_entretien_a_venir_30_cloture_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_sans_date_debut_cloture_com
--                 . Correction de la jointure de la vue v_geo_alert_entretien_datefin_inf_datedebut_cloture_com
-- 2021/10/29 : SL / Suppression des vues d'entretien
-- 2021/11/16 : SL / Suppression des champs insee_contrat et idcontrat

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                    Table géographique : Clôture (commune)                                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_cloture_com;

CREATE TABLE atd16_espaces_verts.geo_cloture_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ARC_objet] Libellé du quartier
    domanialite integer, --[FK][ARC_objet] Domanialité
    qualdoma integer, --[FK][ARC_objet] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ARC_objet] Observations diverses
    larg_cm integer, --[ARC_line] Largeur de la clôture en centimètre
    type_cloture integer, --[ARC_typ3] Type de clôture
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Clôture"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_cloture_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_cloture_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_cloture_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_cloture_com IS '[ATD16] Table géographique contenant les clôtures';

COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.quartier IS '[ARC_objet] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.domanialite IS '[FK][ARC_objet] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.qualdoma IS '[FK][ARC_objet] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.srcgeom_sai IS 
    '[FK][ARC_objet] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.srcdate_sai IS 
    '[ARC_objet] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.observ IS '[ARC_objet] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.larg_cm IS '[ARC_line] Largeur de la clôture en centimètre';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.type_cloture IS '[ARC_typ3] Type de clôture';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Clôture"';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_cloture_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_cloture_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_cloture_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_cloture_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_cloture_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_cloture_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_cloture_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_cloture_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_cloture_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_cloture_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/12/06 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
--                 . Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
--                 . Ajout de la vue v_geo_mobilier_faunistique_ruche_com
--                 . Ajout de la vue v_geo_mobilier_faunistique_dorlotoire_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table géographique : Mobilier faunistique (commune)                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TABLE atd16_espaces_verts.geo_mobilier_faunistique_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ATD16] Libellé du quartier
    domanialite integer, --[FK][ATD16] Domanialité
    qualdoma integer, --[FK][ATD16] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ATD16] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ATD16] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ATD16] Observations diverses
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    type_mobilier integer, --[ATD16] Type de mobilier faunistique
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Mobilier faunistique"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_mobilier_faunistique_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_mobilier_faunistique_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_mobilier_faunistique_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_mobilier_faunistique_com IS '[ATD16] Table géographique contenant les mobiliers faunistiques';

COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.gid IS 
    '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.quartier IS '[ATD16] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.domanialite IS '[FK][ATD16] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.qualdoma IS '[FK][ATD16] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.srcgeom_sai IS 
    '[FK][ATD16] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.srcdate_sai IS 
    '[ATD16] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.observ IS '[ATD16] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.x_l93 IS '[ATD16] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.y_l93 IS '[ATD16] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.type_mobilier IS '[ATD16] Type de mobilier faunistique';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Mobilier faunistique"';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                            v_geo_mobilier_faunistique_ruche_com : vue des ruches                                           ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_ruche_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_ruche_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.type_mobilier,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_mobilier_faunistique_com a
    WHERE a.type_mobilier = '01';

ALTER TABLE atd16_espaces_verts.v_geo_mobilier_faunistique_ruche_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_ruche_com IS 
    '[ATD16] Vue listant les ruches';


-- ##################################################################################################################################################
-- ###                                       v_geo_mobilier_faunistique_dorlotoire_com : vue des dorlotoires                                      ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_dorlotoire_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_dorlotoire_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.type_mobilier,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_mobilier_faunistique_com a
    WHERE a.type_mobilier = '02';

ALTER TABLE atd16_espaces_verts.v_geo_mobilier_faunistique_dorlotoire_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_dorlotoire_com IS 
    '[ATD16] Vue listant les dorlotoires';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_mobilier_faunistique_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_mobilier_faunistique_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_mobilier_faunistique_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_mobilier_faunistique_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_mobilier_faunistique_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_mobilier_faunistique_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_mobilier_faunistique_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_mobilier_faunistique_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/06/09 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_insee() et du trigger associé t_before_iu_maj_insee
-- 2021/09/13 : SL / Transfert de la fonction f_maj_insee() dans le fichier 101_function_trigger_depend_geo_commune.sql
-- 2021/09/14 : SL / Ajout de la fonction f_maj_insee_contrat_com() et du trigger associé t_before_iu_maj_insee
-- 2021/11/16 : SL / Ajout de la fonction Ajout de la fonction f_supp_idcontrat_com() et du trigger associé t_after_d_maj_idcontrat_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                   Fonctions triggers et triggers spécifiques à la table ngeo_contrat_com                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                         Mise à jour du champ insee                                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_insee_contrat_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_insee_contrat_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    SELECT com.insee
    FROM atd16_espaces_verts.geo_commune AS com
    WHERE NEW.pk_commune = com.gid
    INTO NEW.insee;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_insee_contrat_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_insee_contrat_com() IS 
    'Fonction trigger permettant la mise à jour du champ insee à partir de la table geo_commune';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_insee 
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.ngeo_contrat_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_insee_contrat_com();
    
COMMENT ON TRIGGER t_before_iu_maj_insee ON atd16_espaces_verts.ngeo_contrat_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ insee à partir de la table geo_commune';


-- ##################################################################################################################################################
-- ###                                        Suppression du lien idcontrat lorsqu'un contrat est supprimé                                        ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_supp_idcontrat_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_supp_idcontrat_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    UPDATE atd16_espaces_verts.geo_zone_gestion_com AS a
    SET idcontrat = NULL
    WHERE a.idcontrat = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_supp_idcontrat_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_supp_idcontrat_com() IS 
	'[ATD16] Suppression de la valeur du champ idcontrat lorsqu''un contrat est supprimé';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_maj_idcontrat_com
    AFTER DELETE
    ON atd16_espaces_verts.ngeo_contrat_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_supp_idcontrat_com();
    
COMMENT ON TRIGGER t_after_d_maj_idcontrat_com ON atd16_espaces_verts.ngeo_contrat_com IS 
    '[ATD16] Suppression de la valeur du champ idcontrat lorsqu''un contrat est supprimé';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/04 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_insee_entretien_type_zone_gestion_com() et du trigger associé t_before_iu_maj_insee
--                 . Ajout de la fonction f_maj_tps_planifie_estime_zone_gestion_com() et du trigger associé t_after_iud_maj_tps_planifie_estime_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                          Fonctions triggers et triggers spécifiques à la table ngeo_entretien_type_zone_gestion_com                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                         Mise à jour du champ insee                                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    SELECT com.insee
    FROM atd16_espaces_verts.geo_zone_gestion_com AS com
    WHERE NEW.id_zone_gestion = com.gid
    INTO NEW.insee;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ insee à partir de la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_insee 
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com();
    
COMMENT ON TRIGGER t_before_iu_maj_insee ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ insee à partir de la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                                     Mise à jour du champ tps_planifie_estime dans geo_zone_gestion_com                                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_planifie_estime = -- Le champ tps_planifie_estime prend la valeur
	        (
                SELECT SUM(b.tps_passe_moyen * b.periodicite_annuelle) 
                FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
            ) 
	    -- de la somme des durées moyennes fois la periodicité annuelle des différents entretiens ayant un id_zone_gestion identique
        -- à celui qui est supprimé
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_planifie_estime = -- Le champ tps_planifie_estime prend la valeur
	        (
                SELECT SUM(b.tps_passe_moyen * b.periodicite_annuelle) 
                FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
            ) 
	    -- de la somme des durées moyennes fois la periodicité annuelle des différents entretiens ayant un id_zone_gestion identique
        -- à celui qui est modifié ou créé
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ tps_planifie_estime dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_tps_planifie_estime_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_tps_planifie_estime_com ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ tps_planifie_estime dans la table geo_zone_gestion_com';


-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/13 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_iu_maj_insee
-- 2021/09/14 : SL / Ajout de la fonction f_maj_insee_entretien_zone_gestion_com()
-- 2021/09/16 : SL / Ajout de la fonction f_insert_entretien_periodique_zone_gestion_com() et du trigger associé t_before_iu_insert_entretien_periodique
-- 2021/11/03 : SL / Modification du nom du fichier sql en 250_trigger_ngeo_intervention_zone_gestion_com.sql
--                 . Modification des deux fonctions-triggers (entretien -> intervention)
--                 . Ajout de la fonction f_maj_tps_intervention_passe_zone_gestion_com() et du trigger associé t_after_iud_maj_tps_intervention_passe_com
-- 2021/11/04 : SL / Modification de la fonction f_maj_tps_intervention_passe_n_zone_gestion_com() et du trigger associé t_after_iud_maj_tps_intervention_passe_n_com
--                 . Ajout de la fonction f_maj_tps_intervention_passe_n_1_zone_gestion_com() et du trigger associé t_after_iud_maj_tps_intervention_passe_n_1_com
--                 . Ajout de la fonction f_maj_nb_intervention_n_zone_gestion_com() et du trigger associé t_after_iud_maj_nb_intervention_n_com
--                 . Ajout de la fonction f_maj_nb_intervention_n_1_zone_gestion_com() et du trigger associé t_after_iud_maj_nb_intervention_n_1_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                           Fonctions triggers et triggers spécifiques à la table ngeo_intervention_zone_gestion_com                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                         Mise à jour du champ insee                                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    SELECT com.insee
    FROM atd16_espaces_verts.geo_zone_gestion_com AS com
    WHERE NEW.id_zone_gestion = com.gid
    INTO NEW.insee;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ insee à partir de la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_insee 
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com();
    
COMMENT ON TRIGGER t_before_iu_maj_insee ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ insee à partir de la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                               Création d'une nouvelle intervention lorsque le champ periodicite est renseigné                              ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'INSERT') THEN
		IF NEW.periodicite IS NOT NULL THEN
			INSERT INTO atd16_espaces_verts.ngeo_intervention_zone_gestion_com (ident, date_debut, insee, id_zone_gestion, type_entretien)
			VALUES (NEW.ident, NEW.date_debut+NEW.periodicite, NEW.insee, NEW.id_zone_gestion, NEW.type_entretien);
		END IF;
	ELSE
		IF OLD.periodicite IS NULL AND NEW.periodicite IS NOT NULL THEN
			INSERT INTO atd16_espaces_verts.ngeo_intervention_zone_gestion_com (ident, date_debut, insee, id_zone_gestion, type_entretien)
			VALUES (NEW.ident, NEW.date_debut+NEW.periodicite, NEW.insee, NEW.id_zone_gestion, NEW.type_entretien);
		END IF;		
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com() IS 
    'Fonction trigger permettant la création d''une nouvelle intervention lorsque le champ periodicite est renseigné';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_insert_intervention_periodique 
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com();
    
COMMENT ON TRIGGER t_before_iu_insert_intervention_periodique ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la création d''une nouvelle intervention lorsque le champ periodicite est renseigné';


-- ##################################################################################################################################################
-- ###                                   Mise à jour du champ tps_intervention_passe_n dans geo_zone_gestion_com                                  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_intervention_passe_n = -- Le champ tps_intervention_passe_n prend la valeur
	        (
                SELECT SUM(b.duree_h) 
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = date_part('year'::text, 'now'::text::date)
            ) 
	    -- de la somme des durées des différentes interventions ayant un id_zone_gestion identique à celui qui est supprimé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_intervention_passe_n = -- Le champ tps_intervention_passe_n prend la valeur
	        (
                SELECT SUM(b.duree_h) 
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = date_part('year'::text, 'now'::text::date)
            ) 
	    -- de la somme des durées des différentes interventions ayant un id_zone_gestion identique à celui qui est créé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ tps_intervention_passe_n dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_tps_intervention_passe_n_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_tps_intervention_passe_n_com ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ tps_intervention_passe_n dans la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                                  Mise à jour du champ tps_intervention_passe_n_1 dans geo_zone_gestion_com                                 ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_intervention_passe_n_1 = -- Le champ tps_intervention_passe_n_1 prend la valeur
	        (
                SELECT SUM(b.duree_h) 
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = (date_part('year'::text, 'now'::text::date) - 1)
            ) 
	    -- de la somme des durées des différentes interventions ayant un id_zone_gestion identique à celui qui est supprimé
        -- et dont l'année est égal à l'année dernière
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_intervention_passe_n_1 = -- Le champ tps_intervention_passe_n_1 prend la valeur
	        (
                SELECT SUM(b.duree_h) 
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = (date_part('year'::text, 'now'::text::date) - 1)
            ) 
	    -- de la somme des durées des différentes interventions ayant un id_zone_gestion identique à celui qui est créé
        -- et dont l'année est égal à l'année dernière
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ tps_intervention_passe_n_1 dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_tps_intervention_passe_n_1_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_tps_intervention_passe_n_1_com ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ tps_intervention_passe_n_1 dans la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                                      Mise à jour du champ nb_intervention_n dans geo_zone_gestion_com                                      ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET nb_intervention_n = -- Le champ nb_intervention_n prend la valeur
	        (
                SELECT COUNT(*)
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = date_part('year'::text, 'now'::text::date)
            ) 
	    -- du nombre d'enregistrements des différentes interventions ayant un id_zone_gestion identique à celui qui est supprimé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET nb_intervention_n = -- Le champ nb_intervention_n prend la valeur
	        (
                SELECT COUNT(*)
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = date_part('year'::text, 'now'::text::date)
            ) 
	    -- du nombre d'enregistrements des différentes interventions ayant un id_zone_gestion identique à celui qui est créé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ nb_intervention_n dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_nb_intervention_n_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_nb_intervention_n_com ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ nb_intervention_n dans la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                                     Mise à jour du champ nb_intervention_n_1 dans geo_zone_gestion_com                                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET nb_intervention_n_1 = -- Le champ nb_intervention_n_1 prend la valeur
	        (
                SELECT COUNT(*)
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = (date_part('year'::text, 'now'::text::date) - 1)
            ) 
	    -- du nombre d'enregistrements des différentes interventions ayant un id_zone_gestion identique à celui qui est supprimé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET nb_intervention_n_1 = -- Le champ nb_intervention_n_1 prend la valeur
	        (
                SELECT COUNT(*)
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = (date_part('year'::text, 'now'::text::date) - 1)
            ) 
	    -- du nombre d'enregistrements des différentes interventions ayant un id_zone_gestion identique à celui qui est créé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ nb_intervention_n_1 dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_nb_intervention_n_1_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_nb_intervention_n_1_com ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ nb_intervention_n_1 dans la table geo_zone_gestion_com';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/07 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idzone_2() et du trigger associé t_after_iud_maj_idzone_2
-- 2021/11/04 : SL / Modification du nom de la fonction f_maj_idzone_2_com() et du trigger associé t_after_iud_maj_idzone_2_com
--                 . Ajout de la fonction f_supp_intervention_com() et du trigger associé t_after_d_supp_intervention_com
--                 . Ajout de la fonction f_supp_entretien_type_com() et du trigger associé t_after_d_supp_entretien_type_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Fonctions triggers et triggers spécifiques à la table geo_zone_gestion_com                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                           Mise à jour du lien idzone lorsqu'une zone de gestion est créée, modifiée ou supprimée                           ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_idzone_2_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_idzone_2_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_espaces_verts.geo_zone_boisee_com AS a SET idzone = NULL WHERE st_intersects(OLD.the_geom, a.the_geom);
		UPDATE atd16_espaces_verts.geo_alignement_arbres_com AS b SET idzone = NULL WHERE st_intersects(OLD.the_geom, b.the_geom);
		UPDATE atd16_espaces_verts.geo_arbre_isole_com AS c SET idzone = NULL WHERE st_intersects(OLD.the_geom, c.the_geom);
		UPDATE atd16_espaces_verts.geo_massif_arbustif_com AS d SET idzone = NULL WHERE st_intersects(OLD.the_geom, d.the_geom);
		UPDATE atd16_espaces_verts.geo_haie_com AS e SET idzone = NULL WHERE st_intersects(OLD.the_geom, e.the_geom);
		UPDATE atd16_espaces_verts.geo_arbuste_isole_com AS f SET idzone = NULL WHERE st_intersects(OLD.the_geom, f.the_geom);
		UPDATE atd16_espaces_verts.geo_espace_enherbe_com AS g SET idzone = NULL WHERE st_intersects(OLD.the_geom, g.the_geom);
		UPDATE atd16_espaces_verts.geo_massif_fleuri_com AS h SET idzone = NULL WHERE st_intersects(OLD.the_geom, h.the_geom);
		UPDATE atd16_espaces_verts.geo_fleuri_isole_com AS i SET idzone = NULL WHERE st_intersects(OLD.the_geom, i.the_geom);
		UPDATE atd16_espaces_verts.geo_etendue_d_eau_com AS j SET idzone = NULL WHERE st_intersects(OLD.the_geom, j.the_geom);
		UPDATE atd16_espaces_verts.geo_cours_d_eau_com AS k SET idzone = NULL WHERE st_intersects(OLD.the_geom, k.the_geom);
		UPDATE atd16_espaces_verts.geo_point_d_eau_com AS l SET idzone = NULL WHERE st_intersects(OLD.the_geom, l.the_geom);
		UPDATE atd16_espaces_verts.geo_arrivee_d_eau_com AS m SET idzone = NULL WHERE st_intersects(OLD.the_geom, m.the_geom);
		UPDATE atd16_espaces_verts.geo_espace_loisir_com AS n SET idzone = NULL WHERE st_intersects(OLD.the_geom, n.the_geom);
		UPDATE atd16_espaces_verts.geo_loisir_isole_com AS o SET idzone = NULL WHERE st_intersects(OLD.the_geom, o.the_geom);
		UPDATE atd16_espaces_verts.geo_zone_circu_com AS p SET idzone = NULL WHERE st_intersects(OLD.the_geom, p.the_geom);
		UPDATE atd16_espaces_verts.geo_voie_circu_com AS q SET idzone = NULL WHERE st_intersects(OLD.the_geom, q.the_geom);
		UPDATE atd16_espaces_verts.geo_cloture_com AS r SET idzone = NULL WHERE st_intersects(OLD.the_geom, r.the_geom);
        -- Mise à jour du champ idzone avec la valeur NULL lorsque l'objet est impacté (intersecté) par la suppression d'une zone de gestion
    ELSE
		UPDATE atd16_espaces_verts.geo_zone_boisee_com AS a SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, a.the_geom);
		UPDATE atd16_espaces_verts.geo_alignement_arbres_com AS b SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, b.the_geom);
		UPDATE atd16_espaces_verts.geo_arbre_isole_com AS c SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, c.the_geom);
		UPDATE atd16_espaces_verts.geo_massif_arbustif_com AS d SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, d.the_geom);
		UPDATE atd16_espaces_verts.geo_haie_com AS e SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, e.the_geom);
		UPDATE atd16_espaces_verts.geo_arbuste_isole_com AS f SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, f.the_geom);
		UPDATE atd16_espaces_verts.geo_espace_enherbe_com AS g SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, g.the_geom);
		UPDATE atd16_espaces_verts.geo_massif_fleuri_com AS h SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, h.the_geom);
		UPDATE atd16_espaces_verts.geo_fleuri_isole_com AS i SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, i.the_geom);
		UPDATE atd16_espaces_verts.geo_etendue_d_eau_com AS j SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, j.the_geom);
		UPDATE atd16_espaces_verts.geo_cours_d_eau_com AS k SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, k.the_geom);
		UPDATE atd16_espaces_verts.geo_point_d_eau_com AS l SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, l.the_geom);
		UPDATE atd16_espaces_verts.geo_arrivee_d_eau_com AS m SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, m.the_geom);
		UPDATE atd16_espaces_verts.geo_espace_loisir_com AS n SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, n.the_geom);
		UPDATE atd16_espaces_verts.geo_loisir_isole_com AS o SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, o.the_geom);
		UPDATE atd16_espaces_verts.geo_zone_circu_com AS p SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, p.the_geom);
		UPDATE atd16_espaces_verts.geo_voie_circu_com AS q SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, q.the_geom);
		UPDATE atd16_espaces_verts.geo_cloture_com AS r SET idzone = NEW.gid WHERE st_intersects(NEW.the_geom, r.the_geom);
        -- Mise à jour du champ idzone avec la valeur gid (zone de gestion) lorsque l'objet est impacté (intersecté) 
        -- par la création ou la modification d'une zone de gestion
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_idzone_2_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_idzone_2_com() IS 
	'[ATD16] Mise à jour du champ idzone lorsqu''une zone de gestion est créée, modifiée ou supprimée';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_idzone_2_com
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.geo_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_2_com();
    
COMMENT ON TRIGGER t_after_iud_maj_idzone_2_com ON atd16_espaces_verts.geo_zone_gestion_com IS 
    '[ATD16] Mise à jour du champ idzone lorsqu''une zone de gestion est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                                   Suppression des interventions lorsqu'une zone de gestion est supprimée                                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_supp_intervention_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_supp_intervention_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com
	WHERE id_zone_gestion = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_supp_intervention_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_supp_intervention_com() IS 
	'[ATD16] Suppression des interventions lorsqu''une zone de gestion est supprimée';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_supp_intervention_com
    AFTER DELETE
    ON atd16_espaces_verts.geo_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_supp_intervention_com();
    
COMMENT ON TRIGGER t_after_d_supp_intervention_com ON atd16_espaces_verts.geo_zone_gestion_com IS 
    '[ATD16] Suppression des interventions lorsqu''une zone de gestion est supprimée';


-- ##################################################################################################################################################
-- ###                                  Suppression des entretiens types lorsqu'une zone de gestion est supprimée                                 ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_supp_entretien_type_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_supp_entretien_type_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    DELETE FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com
	WHERE id_zone_gestion = OLD.gid;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_supp_entretien_type_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_supp_entretien_type_com() IS 
	'[ATD16] Suppression des interventions lorsqu''une zone de gestion est supprimée';

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_d_supp_entretien_type_com
    AFTER DELETE
    ON atd16_espaces_verts.geo_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_supp_entretien_type_com();
    
COMMENT ON TRIGGER t_after_d_supp_entretien_type_com ON atd16_espaces_verts.geo_zone_gestion_com IS 
    '[ATD16] Suppression des interventions lorsqu''une zone de gestion est supprimée';

