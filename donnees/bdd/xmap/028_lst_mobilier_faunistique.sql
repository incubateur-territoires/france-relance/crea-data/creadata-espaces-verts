-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/12/06 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur du type de mobilier faunistique                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_mobilier_faunistique;

CREATE TABLE atd16_espaces_verts.lst_mobilier_faunistique 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du mobilier faunistique
    valeur varchar(50), --[ATD16] Valeur du mobilier faunistique
    CONSTRAINT pk_lst_mobilier_faunistique PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_mobilier_faunistique OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_mobilier_faunistique TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_mobilier_faunistique IS '[ATD16] Domaine de valeur du type de mobilier faunistique';

COMMENT ON COLUMN atd16_espaces_verts.lst_mobilier_faunistique.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_mobilier_faunistique.code IS '[ATD16] Code du mobilier faunistique';
COMMENT ON COLUMN atd16_espaces_verts.lst_mobilier_faunistique.valeur IS '[ATD16] Valeur du mobilier faunistique';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_mobilier_faunistique 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Ruche'),
    ('02','Dorlotoire'),
    ('03','Nichoir à oiseaux');

