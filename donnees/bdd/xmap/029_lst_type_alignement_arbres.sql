-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/11/28 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table non géographique : Domaine de valeur du type d'alignement d'arbres                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_alignement_arbres;

CREATE TABLE atd16_espaces_verts.lst_type_alignement_arbres 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du type d'alignement d'arbres
    valeur varchar(50), --[ATD16] Valeur du type d'alignement d'arbres
    CONSTRAINT pk_lst_type_alignement_arbres PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_alignement_arbres OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_alignement_arbres TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_alignement_arbres IS '[ATD16] Domaine de valeur du type d''alignement d''arbres';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_alignement_arbres.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_alignement_arbres.code IS '[ATD16] Code du type d''alignement d''arbres';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_alignement_arbres.valeur IS '[ATD16] Valeur du type d''alignement d''arbres';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_alignement_arbres 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Tige'),
    ('02','Pyramide'),
    ('03','Scion'),
    ('04','Gobelet'),
    ('05','Palmette'),
    ('06','U simple'),
    ('07','U double'),
    ('08','Cordon'),
    ('09','Cordon double');

