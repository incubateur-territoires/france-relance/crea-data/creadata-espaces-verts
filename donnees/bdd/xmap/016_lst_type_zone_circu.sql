-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur du type de zone de circulation                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_zone_circu;

CREATE TABLE atd16_espaces_verts.lst_type_zone_circu 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type de zone de circulation
    valeur varchar(80), --[ARC_typ3] Libellé du type de zone de circulation
    CONSTRAINT pk_lst_type_zone_circu PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_zone_circu OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_zone_circu TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_zone_circu IS '[ARC_typ3] Domaine de valeur des codes du type de zone de circulation';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_zone_circu.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_zone_circu.code IS '[ARC_typ3] Code du type de zone de circulation';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_zone_circu.valeur IS '[ARC_typ3] Libellé du type de zone de circulation';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_zone_circu 
    (code,valeur)
VALUES
    ('213','Parking matérialisé'),
    ('214','Espace de stationnement libre'),
    ('215','Parvis, place'),
    ('219','Autre circulation');

