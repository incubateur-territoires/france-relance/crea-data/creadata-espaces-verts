-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/05 : SL / Création du fichier sur Git
-- 2021/05/11 : SL / Ajout des champs date_creation et date_maj
--                 . Ajout des triggers t_before_i_init_date_creation et t_before_u_date_maj
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)
-- 2021/06/09 : SL / Ajout du champ pk_commune nécessaire pour paramétrer sw_custom_fiche
--                 . Ajout du champ numero
--                 . Ajout de la vue v_ngeo_contrat_com_annee_en_cours

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table non géographique : Contrat entretien/gestion                                            ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.ngeo_contrat_com;

CREATE TABLE atd16_espaces_verts.ngeo_contrat_com 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    ident varchar(254), --[ATD16] Nom de l'entreprise
    numero varchar(254), --[ATD16] Numéro du contrat
    date_debut date, --[ATD16] Date de début du contrat
    date_fin date, --[ATD16] Date de fin du contrat
    insee varchar(6), --[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    pk_commune integer, --[FK][ATD16] Code de la commune (gid) (nécessaire pour paramétrer sw_custom_fiche)
    CONSTRAINT pk_ngeo_contrat_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.ngeo_contrat_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.ngeo_contrat_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.ngeo_contrat_com IS '[ATD16] Table non géographique listant les contrats de gestion des espaces verts';

COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.gid IS '[ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.ident IS '[ATD16] Nom de l''entreprise';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.numero IS '[ATD16] Numéro du contrat';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.date_debut IS '[ATD16] Date de début du contrat';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.date_fin IS '[ATD16] Date de fin du contrat';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.insee IS '[ATD16] Code INSEE de la commune (nécessaire pour paramétrer une liste liée)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.ngeo_contrat_com.pk_commune IS 
    '[FK][ATD16] Code de la commune (gid) (nécessaire pour paramétrer sw_custom_fiche)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###           v_ngeo_contrat_com_annee_en_cours : vue listant les contrats communaux dans l'année en cours (inutile pour l'instant)            ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_ngeo_contrat_com_annee_en_cours;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_ngeo_contrat_com_annee_en_cours 
    AS 
    SELECT 
        a.gid,
        a.insee,
        CONCAT(a.ident,' ','-',' ',a.numero) AS ident_numero -- Concaténation donnant la valeur visible dans la liste liée dans X'MAP
    FROM atd16_espaces_verts.ngeo_contrat_com a
    WHERE EXTRACT(YEAR FROM a.date_debut) <= EXTRACT(YEAR FROM NOW()) 
    --Où l'année de la date de début du contrat est inférieure ou égale à l'année courante
    AND EXTRACT(YEAR FROM a.date_fin) >= EXTRACT(YEAR FROM NOW());
    -- Et où l'année de la date de fin est supérieure ou égale à l'année courante

ALTER TABLE atd16_espaces_verts.v_ngeo_contrat_com_annee_en_cours OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_ngeo_contrat_com_annee_en_cours IS 
    '[ATD16] Vue listant les contrats communaux dans l''année en cours (inutile pour l''instant)';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_contrat_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.ngeo_contrat_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.ngeo_contrat_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_contrat_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.ngeo_contrat_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.ngeo_contrat_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';

