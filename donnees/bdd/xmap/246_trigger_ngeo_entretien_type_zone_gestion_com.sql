-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/04 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_insee_entretien_type_zone_gestion_com() et du trigger associé t_before_iu_maj_insee
--                 . Ajout de la fonction f_maj_tps_planifie_estime_zone_gestion_com() et du trigger associé t_after_iud_maj_tps_planifie_estime_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                          Fonctions triggers et triggers spécifiques à la table ngeo_entretien_type_zone_gestion_com                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                         Mise à jour du champ insee                                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    SELECT com.insee
    FROM atd16_espaces_verts.geo_zone_gestion_com AS com
    WHERE NEW.id_zone_gestion = com.gid
    INTO NEW.insee;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ insee à partir de la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_insee 
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_insee_entretien_type_zone_gestion_com();
    
COMMENT ON TRIGGER t_before_iu_maj_insee ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ insee à partir de la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                                     Mise à jour du champ tps_planifie_estime dans geo_zone_gestion_com                                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_planifie_estime = -- Le champ tps_planifie_estime prend la valeur
	        (
                SELECT SUM(b.tps_passe_moyen * b.periodicite_annuelle) 
                FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
            ) 
	    -- de la somme des durées moyennes fois la periodicité annuelle des différents entretiens ayant un id_zone_gestion identique
        -- à celui qui est supprimé
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_planifie_estime = -- Le champ tps_planifie_estime prend la valeur
	        (
                SELECT SUM(b.tps_passe_moyen * b.periodicite_annuelle) 
                FROM atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
            ) 
	    -- de la somme des durées moyennes fois la periodicité annuelle des différents entretiens ayant un id_zone_gestion identique
        -- à celui qui est modifié ou créé
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ tps_planifie_estime dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_tps_planifie_estime_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_tps_planifie_estime_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_tps_planifie_estime_com ON atd16_espaces_verts.ngeo_entretien_type_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ tps_planifie_estime dans la table geo_zone_gestion_com';


