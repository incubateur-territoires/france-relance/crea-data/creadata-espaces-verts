-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/11/28 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                              Table non géographique : Domaine de valeur du type de forme des arbres fruitiers                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier;

CREATE TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du type de forme
    valeur varchar(50), --[ATD16] Valeur du type de forme
    CONSTRAINT pk_lst_type_forme_arbre_fruitier PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_forme_arbre_fruitier IS '[ATD16] Domaine de valeur du type de forme des arbres fruitiers';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_forme_arbre_fruitier.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_forme_arbre_fruitier.code IS '[ATD16] Code du type de forme';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_forme_arbre_fruitier.valeur IS '[ATD16] Valeur du type de forme';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_forme_arbre_fruitier 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Haute-tige'),
    ('02','Demi-tige'),
    ('03','Basse-tige');

