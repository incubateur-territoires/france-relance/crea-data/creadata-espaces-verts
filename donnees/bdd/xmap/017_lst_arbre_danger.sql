-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                          Table non géographique : Domaine de valeur caractérisant la dangerosité des arbres isolés                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_danger;

CREATE TABLE atd16_espaces_verts.lst_arbre_danger 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe de dangerosité des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe de dangerosité des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_danger PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_danger OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_danger TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_danger IS '[ARC] Liste permettant de décrire la classe de dangerosité des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_danger.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_danger.code IS '[ARC] Code de la classe de dangerosité des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_danger.valeur IS '[ARC] Valeur de la classe de dangerosité des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_danger 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Aucun'),
    ('2','Dangereux'),
    ('3','Moyenne dangereux'),
    ('4','Faiblement dangereux');

