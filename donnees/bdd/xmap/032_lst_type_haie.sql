-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/11/28 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                         Table non géographique : Domaine de valeur du type de haie                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_haie;

CREATE TABLE atd16_espaces_verts.lst_type_haie 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du type de haie
    valeur varchar(50), --[ATD16] Valeur du type de haie
    CONSTRAINT pk_lst_type_haie PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_haie OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_haie TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_haie IS '[ATD16] Domaine de valeur du type de haie';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_haie.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_haie.code IS '[ATD16] Code du type de haie';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_haie.valeur IS '[ATD16] Valeur du type de haie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_haie 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Haie champêtre');

