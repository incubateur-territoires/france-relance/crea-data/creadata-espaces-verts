-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/12/06 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_i_init_date_creation
--                 . Ajout du trigger t_before_u_date_maj
--                 . Ajout du trigger t_before_iu_maj_idzone
--                 . Ajout du trigger t_before_iu_maj_xy_coord
--                 . Ajout de la vue v_geo_mobilier_faunistique_ruche_com
--                 . Ajout de la vue v_geo_mobilier_faunistique_dorlotoire_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                              Table géographique : Mobilier faunistique (commune)                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TABLE atd16_espaces_verts.geo_mobilier_faunistique_com
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idzone integer, --[FK][ARC_objet] Identifiant de la zone de gestion
    insee varchar(6), --[ATD16] Code INSEE de la commune
    quartier varchar(80), --[ATD16] Libellé du quartier
    domanialite integer, --[FK][ATD16] Domanialité
    qualdoma integer, --[FK][ATD16] Qualité de linformation liée à la domanialité
    srcgeom_sai integer, --[FK][ATD16] Référentiel géographique utilisé pour la saisie de l'objet
    srcdate_sai integer, --[ATD16] Année du référentiel géographique utilisé pour la saisie de l'objet
    observ varchar(255), --[ATD16] Observations diverses
    x_l93 numeric(10,3), --[ARC_pct] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ARC_pct] Coordonnées Y en Lambert 93 (automatique)
    type_mobilier integer, --[ATD16] Type de mobilier faunistique
    ident varchar(80), --[SIRAP] Identificaion de l'objet / Par défaut "Mobilier faunistique"
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    datesig date, --[ATD16] Date d'intégration de la donnée
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_mobilier_faunistique_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.geo_mobilier_faunistique_com OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.geo_mobilier_faunistique_com TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.geo_mobilier_faunistique_com IS '[ATD16] Table géographique contenant les mobiliers faunistiques';

COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.gid IS 
    '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.idzone IS '[FK][ARC_objet] Identifiant de la zone de gestion';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.quartier IS '[ATD16] Libellé du quartier';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.domanialite IS '[FK][ATD16] Domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.qualdoma IS '[FK][ATD16] Qualité de l''information liée à la domanialité';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.srcgeom_sai IS 
    '[FK][ATD16] Référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.srcdate_sai IS 
    '[ATD16] Année du référentiel géographique utilisé pour la saisie de l''objet';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.observ IS '[ATD16] Observations diverses';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.x_l93 IS '[ATD16] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.y_l93 IS '[ATD16] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.type_mobilier IS '[ATD16] Type de mobilier faunistique';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.ident IS '[SIRAP] Identificaion de l''objet / Par défaut "Mobilier faunistique"';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.datesig IS '[ATD16] Date d''intégration de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN atd16_espaces_verts.geo_mobilier_faunistique_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                            v_geo_mobilier_faunistique_ruche_com : vue des ruches                                           ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_ruche_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_ruche_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.type_mobilier,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_mobilier_faunistique_com a
    WHERE a.type_mobilier = '01';

ALTER TABLE atd16_espaces_verts.v_geo_mobilier_faunistique_ruche_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_ruche_com IS 
    '[ATD16] Vue listant les ruches';


-- ##################################################################################################################################################
-- ###                                       v_geo_mobilier_faunistique_dorlotoire_com : vue des dorlotoires                                      ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_dorlotoire_com;

CREATE OR REPLACE VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_dorlotoire_com 
    AS 
    SELECT 
        a.gid,
        a.idzone,
        a.insee,
        a.ident,
        a.type_mobilier,
        a.domanialite,
        a.observ,
        a.the_geom
    FROM atd16_espaces_verts.geo_mobilier_faunistique_com a
    WHERE a.type_mobilier = '02';

ALTER TABLE atd16_espaces_verts.v_geo_mobilier_faunistique_dorlotoire_com OWNER TO sditecgrp;

COMMENT ON VIEW atd16_espaces_verts.v_geo_mobilier_faunistique_dorlotoire_com IS 
    '[ATD16] Vue listant les dorlotoires';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON atd16_espaces_verts.geo_mobilier_faunistique_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON atd16_espaces_verts.geo_mobilier_faunistique_com IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON atd16_espaces_verts.geo_mobilier_faunistique_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON atd16_espaces_verts.geo_mobilier_faunistique_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TRIGGER t_before_iu_maj_idzone
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_mobilier_faunistique_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_idzone_com();
    
COMMENT ON TRIGGER t_before_iu_maj_idzone ON atd16_espaces_verts.geo_mobilier_faunistique_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idzone';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_mobilier_faunistique_com;

CREATE TRIGGER t_before_iu_maj_xy_coord
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.geo_mobilier_faunistique_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_xy_coord();
    
COMMENT ON TRIGGER t_before_iu_maj_xy_coord ON atd16_espaces_verts.geo_mobilier_faunistique_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';

