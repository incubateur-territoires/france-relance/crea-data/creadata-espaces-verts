-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/29 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Table non géographique : Domaine de valeur de la domanialité                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_domanialite;

CREATE TABLE atd16_espaces_verts.lst_domanialite 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code du type de domanialité
    valeur varchar(50), --[ARC] Valeur du type de domanialité
    CONSTRAINT pk_lst_domanialite PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_domanialite OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_domanialite TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_domanialite IS '[ARC] Domaine de valeur de la domanialité';

COMMENT ON COLUMN atd16_espaces_verts.lst_domanialite.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_domanialite.code IS '[ARC] Code du type de domanialité';
COMMENT ON COLUMN atd16_espaces_verts.lst_domanialite.valeur IS '[ARC] Valeur du type de domanialité';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_domanialite 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('10','Publique'),
    ('20','Privée (non déterminé)'),
    ('21','Privée (communale)'),
    ('22','Privée (autre organisme public, HLM, ...)'),
    ('23','Privée');
    
