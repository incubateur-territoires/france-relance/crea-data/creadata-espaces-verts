-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                   Table non géographique : Domaine de valeur du type de saisie de la sous-classe de précision des haies                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_haie_type_saisie;

CREATE TABLE atd16_espaces_verts.lst_haie_type_saisie 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe du type de saisie de la sous-classe de précision des objets espace vert de type haie
    valeur varchar(80), --[ARC] Valeur de la classe du type de saisie de la sous-classe de précision des objets espace vert de type haie
    CONSTRAINT pk_lst_haie_type_saisie PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_haie_type_saisie OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_haie_type_saisie TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_haie_type_saisie IS 
    '[ARC] Liste permettant de décrire le type de saisie de la sous-classe de précision des objets espace vert de type haie';

COMMENT ON COLUMN atd16_espaces_verts.lst_haie_type_saisie.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_haie_type_saisie.code IS 
    '[ARC] Code de la classe du type de saisie de la sous-classe de précision des objets espace vert de type haie';
COMMENT ON COLUMN atd16_espaces_verts.lst_haie_type_saisie.valeur IS 
    '[ARC] Valeur de la classe du type de saisie de la sous-classe de précision des objets espace vert de type haie';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_haie_type_saisie 
    (code,valeur)
VALUES
    ('10','Largeur à appliquer au centre du linéaire'),
    ('20','Largeur à appliquer dans le sens de saisie'),
    ('30','Largeur à appliquer dans le sens inverse de saisie');

