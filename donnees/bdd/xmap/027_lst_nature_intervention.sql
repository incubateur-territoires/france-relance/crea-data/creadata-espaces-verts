-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/13 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur sur la nature de l'intervention                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_nature_intervention;

CREATE TABLE atd16_espaces_verts.lst_nature_intervention 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la nature de l'intervention
    valeur varchar(50), --[ARC] Valeur de la nature de l'intervention
    CONSTRAINT pk_lst_nature_intervention PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_nature_intervention OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_nature_intervention TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_nature_intervention IS '[ARC] Domaine de valeur de la nature de l''intervention';

COMMENT ON COLUMN atd16_espaces_verts.lst_nature_intervention.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_nature_intervention.code IS '[ARC] Code de la nature de l''intervention';
COMMENT ON COLUMN atd16_espaces_verts.lst_nature_intervention.valeur IS '[ARC] Valeur de la nature de l''intervention';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_nature_intervention 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('10','Programmée'),
    ('20','Urgente');

