-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/04/30 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                              Table non géographique : Domaine de valeur de la nature de sol des arbres isolés                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_arbre_sol;

CREATE TABLE atd16_espaces_verts.lst_arbre_sol 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC] Code de la classe de nature de sol des objets ponctuels arbre
    valeur varchar(50), --[ARC] Valeur de la classe de nature de sol des objets ponctuels arbre
    CONSTRAINT pk_lst_arbre_sol PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_arbre_sol OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_arbre_sol TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_arbre_sol IS '[ARC] Liste permettant de décrire la classe de nature de sol des objets ponctuels arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_sol.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_sol.code IS '[ARC] Code de la classe de nature de sol des objets ponctuels arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_arbre_sol.valeur IS '[ARC] Valeur de la classe de nature de sol des objets ponctuels arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_arbre_sol 
    (code,valeur)
VALUES
    ('0','Non renseigné'),
    ('1','Gazon'),
    ('2','Minéral'),
    ('3','Paillage'),
    ('4','Synthétique'),
    ('5','Terre'),
    ('6','Végétalisé'),
    ('99','Autre');

