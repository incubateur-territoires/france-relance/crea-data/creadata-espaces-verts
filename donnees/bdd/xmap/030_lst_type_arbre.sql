-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/11/28 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                         Table non géographique : Domaine de valeur du type d'arbre                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_arbre;

CREATE TABLE atd16_espaces_verts.lst_type_arbre 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ATD16] Code du type d'arbre
    valeur varchar(50), --[ATD16] Valeur du type d'arbre
    CONSTRAINT pk_lst_type_arbre PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_arbre OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_arbre TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_arbre IS '[ATD16] Domaine de valeur du type d''arbre';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_arbre.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_arbre.code IS '[ATD16] Code du type d''arbre';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_arbre.valeur IS '[ATD16] Valeur du type d''arbre';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_arbre 
    (code,valeur)
VALUES
    ('99','Autre'),
    ('01','Arbre d''ornement'),
    ('02','Arbre fruitier');

