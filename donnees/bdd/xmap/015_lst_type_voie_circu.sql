-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/05/06 : SL / Création du fichier sur Git
-- 2021/06/03 : SL / Modification du nom du schéma (atd16_espaces_verts)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Domaine de valeur du type de voie de circulation                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_espaces_verts.lst_type_voie_circu;

CREATE TABLE atd16_espaces_verts.lst_type_voie_circu 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code integer NOT NULL, --[ARC_typ3] Code du type de voie de circulation
    valeur varchar(80), --[ARC_typ3] Libellé du type de voie de circulation
    CONSTRAINT pk_lst_type_voie_circu PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_espaces_verts.lst_type_voie_circu OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_espaces_verts.lst_type_voie_circu TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_espaces_verts.lst_type_voie_circu IS '[ARC_typ3] Domaine de valeur des codes du type de voie de circulation';

COMMENT ON COLUMN atd16_espaces_verts.lst_type_voie_circu.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_voie_circu.code IS '[ARC_typ3] Code du type de voie de circulation';
COMMENT ON COLUMN atd16_espaces_verts.lst_type_voie_circu.valeur IS '[ARC_typ3] Libellé du type de voie de circulation';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_espaces_verts.lst_type_voie_circu 
    (code,valeur)
VALUES
    ('211','Allée'),
    ('212','Piste cyclable'),
    ('219','Autre circulation');

