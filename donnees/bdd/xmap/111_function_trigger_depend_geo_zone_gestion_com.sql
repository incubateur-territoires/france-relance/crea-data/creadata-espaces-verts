-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/06 : SL / Création du fichier sur Git
-- 2021/09/06 : SL / Ajout de la fonction f_maj_idzone()
-- 2021/11/04 : SL / Modification du nom de la fonction f_maj_idzone_com()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) dépendante(s)                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idzone                                                         ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_idzone_com();

CREATE FUNCTION atd16_espaces_verts.f_maj_idzone_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    SELECT gid -- on sélectionne le champ gid
    FROM atd16_espaces_verts.geo_zone_gestion_com AS zgest -- de la table geo_zone_gestion_com
    WHERE ST_Intersects(NEW.the_geom, zgest.the_geom) -- qui intersecte le nouvel objet créé
    INTO NEW.idzone; -- et on insère la valeur de gid dans le nouveau champ idzone
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_idzone_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_idzone_com() IS 'Fonction trigger permettant la mise à jour du champ idzone';

