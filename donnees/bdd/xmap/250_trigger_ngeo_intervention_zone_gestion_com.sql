-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/13 : SL / Création du fichier sur Git
--                 . Ajout du trigger t_before_iu_maj_insee
-- 2021/09/14 : SL / Ajout de la fonction f_maj_insee_entretien_zone_gestion_com()
-- 2021/09/16 : SL / Ajout de la fonction f_insert_entretien_periodique_zone_gestion_com() et du trigger associé t_before_iu_insert_entretien_periodique
-- 2021/11/03 : SL / Modification du nom du fichier sql en 250_trigger_ngeo_intervention_zone_gestion_com.sql
--                 . Modification des deux fonctions-triggers (entretien -> intervention)
--                 . Ajout de la fonction f_maj_tps_intervention_passe_zone_gestion_com() et du trigger associé t_after_iud_maj_tps_intervention_passe_com
-- 2021/11/04 : SL / Modification de la fonction f_maj_tps_intervention_passe_n_zone_gestion_com() et du trigger associé t_after_iud_maj_tps_intervention_passe_n_com
--                 . Ajout de la fonction f_maj_tps_intervention_passe_n_1_zone_gestion_com() et du trigger associé t_after_iud_maj_tps_intervention_passe_n_1_com
--                 . Ajout de la fonction f_maj_nb_intervention_n_zone_gestion_com() et du trigger associé t_after_iud_maj_nb_intervention_n_com
--                 . Ajout de la fonction f_maj_nb_intervention_n_1_zone_gestion_com() et du trigger associé t_after_iud_maj_nb_intervention_n_1_com

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                           Fonctions triggers et triggers spécifiques à la table ngeo_intervention_zone_gestion_com                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                         Mise à jour du champ insee                                                         ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    SELECT com.insee
    FROM atd16_espaces_verts.geo_zone_gestion_com AS com
    WHERE NEW.id_zone_gestion = com.gid
    INTO NEW.insee;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ insee à partir de la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_maj_insee 
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_insee_intervention_zone_gestion_com();
    
COMMENT ON TRIGGER t_before_iu_maj_insee ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ insee à partir de la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                               Création d'une nouvelle intervention lorsque le champ periodicite est renseigné                              ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'INSERT') THEN
		IF NEW.periodicite IS NOT NULL THEN
			INSERT INTO atd16_espaces_verts.ngeo_intervention_zone_gestion_com (ident, date_debut, insee, id_zone_gestion, type_entretien)
			VALUES (NEW.ident, NEW.date_debut+NEW.periodicite, NEW.insee, NEW.id_zone_gestion, NEW.type_entretien);
		END IF;
	ELSE
		IF OLD.periodicite IS NULL AND NEW.periodicite IS NOT NULL THEN
			INSERT INTO atd16_espaces_verts.ngeo_intervention_zone_gestion_com (ident, date_debut, insee, id_zone_gestion, type_entretien)
			VALUES (NEW.ident, NEW.date_debut+NEW.periodicite, NEW.insee, NEW.id_zone_gestion, NEW.type_entretien);
		END IF;		
	END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com() IS 
    'Fonction trigger permettant la création d''une nouvelle intervention lorsque le champ periodicite est renseigné';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_before_iu_insert_intervention_periodique 
    BEFORE INSERT OR UPDATE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_insert_intervention_periodique_zone_gestion_com();
    
COMMENT ON TRIGGER t_before_iu_insert_intervention_periodique ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la création d''une nouvelle intervention lorsque le champ periodicite est renseigné';


-- ##################################################################################################################################################
-- ###                                   Mise à jour du champ tps_intervention_passe_n dans geo_zone_gestion_com                                  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_intervention_passe_n = -- Le champ tps_intervention_passe_n prend la valeur
	        (
                SELECT SUM(b.duree_h) 
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = date_part('year'::text, 'now'::text::date)
            ) 
	    -- de la somme des durées des différentes interventions ayant un id_zone_gestion identique à celui qui est supprimé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_intervention_passe_n = -- Le champ tps_intervention_passe_n prend la valeur
	        (
                SELECT SUM(b.duree_h) 
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = date_part('year'::text, 'now'::text::date)
            ) 
	    -- de la somme des durées des différentes interventions ayant un id_zone_gestion identique à celui qui est créé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ tps_intervention_passe_n dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_tps_intervention_passe_n_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_tps_intervention_passe_n_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_tps_intervention_passe_n_com ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ tps_intervention_passe_n dans la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                                  Mise à jour du champ tps_intervention_passe_n_1 dans geo_zone_gestion_com                                 ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_intervention_passe_n_1 = -- Le champ tps_intervention_passe_n_1 prend la valeur
	        (
                SELECT SUM(b.duree_h) 
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = (date_part('year'::text, 'now'::text::date) - 1)
            ) 
	    -- de la somme des durées des différentes interventions ayant un id_zone_gestion identique à celui qui est supprimé
        -- et dont l'année est égal à l'année dernière
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET tps_intervention_passe_n_1 = -- Le champ tps_intervention_passe_n_1 prend la valeur
	        (
                SELECT SUM(b.duree_h) 
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = (date_part('year'::text, 'now'::text::date) - 1)
            ) 
	    -- de la somme des durées des différentes interventions ayant un id_zone_gestion identique à celui qui est créé
        -- et dont l'année est égal à l'année dernière
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ tps_intervention_passe_n_1 dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_tps_intervention_passe_n_1_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_tps_intervention_passe_n_1_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_tps_intervention_passe_n_1_com ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ tps_intervention_passe_n_1 dans la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                                      Mise à jour du champ nb_intervention_n dans geo_zone_gestion_com                                      ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET nb_intervention_n = -- Le champ nb_intervention_n prend la valeur
	        (
                SELECT COUNT(*)
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = date_part('year'::text, 'now'::text::date)
            ) 
	    -- du nombre d'enregistrements des différentes interventions ayant un id_zone_gestion identique à celui qui est supprimé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET nb_intervention_n = -- Le champ nb_intervention_n prend la valeur
	        (
                SELECT COUNT(*)
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = date_part('year'::text, 'now'::text::date)
            ) 
	    -- du nombre d'enregistrements des différentes interventions ayant un id_zone_gestion identique à celui qui est créé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ nb_intervention_n dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_nb_intervention_n_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_nb_intervention_n_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_nb_intervention_n_com ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ nb_intervention_n dans la table geo_zone_gestion_com';


-- ##################################################################################################################################################
-- ###                                     Mise à jour du champ nb_intervention_n_1 dans geo_zone_gestion_com                                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com();

CREATE OR REPLACE FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET nb_intervention_n_1 = -- Le champ nb_intervention_n_1 prend la valeur
	        (
                SELECT COUNT(*)
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = OLD.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = (date_part('year'::text, 'now'::text::date) - 1)
            ) 
	    -- du nombre d'enregistrements des différentes interventions ayant un id_zone_gestion identique à celui qui est supprimé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = OLD.id_zone_gestion::text; -- Où l'ancien id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    ELSE
        UPDATE atd16_espaces_verts.geo_zone_gestion_com -- On met à jour la table geo_zone_gestion_com
	    SET nb_intervention_n_1 = -- Le champ nb_intervention_n_1 prend la valeur
	        (
                SELECT COUNT(*)
                FROM atd16_espaces_verts.ngeo_intervention_zone_gestion_com b 
                WHERE b.id_zone_gestion = NEW.id_zone_gestion
                AND date_part('year'::text, b.date_debut) = (date_part('year'::text, 'now'::text::date) - 1)
            ) 
	    -- du nombre d'enregistrements des différentes interventions ayant un id_zone_gestion identique à celui qui est créé
        -- et dont l'année est égal à l'année en cours
	    WHERE gid::text = NEW.id_zone_gestion::text; -- Où le nouvel id_zone_gestion est égal au gid de la table geo_zone_gestion_com
    END IF;
    RETURN NEW;
END;

    
$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com() IS 
    'Fonction trigger permettant la mise à jour du champ nb_intervention_n_1 dans la table geo_zone_gestion_com';


-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_after_iud_maj_nb_intervention_n_1_com 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_espaces_verts.f_maj_nb_intervention_n_1_zone_gestion_com();
    
COMMENT ON TRIGGER t_after_iud_maj_nb_intervention_n_1_com ON atd16_espaces_verts.ngeo_intervention_zone_gestion_com IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ nb_intervention_n_1 dans la table geo_zone_gestion_com';

