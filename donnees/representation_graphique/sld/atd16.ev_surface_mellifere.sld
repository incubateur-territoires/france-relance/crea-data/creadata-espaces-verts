<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des surfaces d'espaces vert mellifères
    appliquée à la couche atd16:v_geo_massif_fleuri_mellifere_com (Geoserver) ; Espaces verts\Commune\Inventaire\Flore mellifère\Massif fleuri (X'MAP)
	appliquée à la couche atd16:v_geo_massif_fleuri_mellifere_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Flore mellifère\Massif fleuri (X'MAP)
    appliquée à la couche atd16:v_geo_massif_arbustif_mellifere_com (Geoserver) ; Espaces verts\Commune\Inventaire\Flore mellifère\Massif arbustif (X'MAP)
	appliquée à la couche atd16:v_geo_massif_arbustif_mellifere_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Flore mellifère\Massif arbustif (X'MAP)
    appliquée à la couche atd16:v_geo_zone_boisee_mellifere_com (Geoserver) ; Espaces verts\Commune\Inventaire\Flore mellifère\Zone boisée (X'MAP)
	appliquée à la couche atd16:v_geo_zone_boisee_mellifere_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Flore mellifère\Zone boisée (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>surface_mellifere</se:Name>
        <UserStyle>
            <se:Name>surface_mellifere</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>surface_mellifere</se:Name>
                    <se:Description>
                        <se:Title>Flore mellifère</se:Title>
                    </se:Description>
					<se:PolygonSymbolizer>
						<se:Fill>
							<se:GraphicFill>
								<se:Graphic>
                                  	<se:ExternalGraphic>
                                    	<se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/espaces_verts/abeille.png"/>
                                      	<se:Format>image/png</se:Format>
                                    </se:ExternalGraphic>
									<se:Size>45</se:Size>
								</se:Graphic>
							</se:GraphicFill>
						</se:Fill>
						<se:VendorOption name="graphic-margin">0 0 0 0</se:VendorOption>
					</se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
