<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des massifs fleuris
    appliquée à la couche atd16:geo_massif_fleuri_com (Geoserver) ; Espaces verts\Inventaire\Commune\Massif fleuri (X'MAP)
	appliquée à la couche atd16:geo_massif_fleuri_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Massif fleuri (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>massif_fleuri</se:Name>
        <UserStyle>
            <se:Name>massif_fleuri</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>massif_fleuri</se:Name>
                    <se:Description>
                        <se:Title>Massif fleuri</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#E1EE21</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.4</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#E1EE21</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">0.4</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>star</se:WellKnownName>
                                        <se:Fill>
                                            <se:SvgParameter name="fill">#EE8E21</se:SvgParameter>
                                            <se:SvgParameter name="fill-opacity">0.8</se:SvgParameter>
                                        </se:Fill>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#EE8E21</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.8</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                    <se:Size>12</se:Size>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                        <se:VendorOption name="graphic-margin">0 12 12 0</se:VendorOption>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>circle</se:WellKnownName>
                                        <se:Fill>
                                            <se:SvgParameter name="fill">#E1EE21</se:SvgParameter>
                                            <se:SvgParameter name="fill-opacity">0.8</se:SvgParameter>
                                        </se:Fill>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#E1EE21</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.8</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                    <se:Size>6</se:Size>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                        <se:VendorOption name="graphic-margin">4 15 14 3</se:VendorOption>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
