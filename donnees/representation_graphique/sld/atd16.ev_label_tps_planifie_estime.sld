<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation textuelle des zones de gestions des espaces vert concernant les temps planifiés estimés
    appliquée à la couche atd16:geo_zone_gestion_com (Geoserver) ; Espaces verts\Commune\Texte\Durée planifiée annuelle estimée (X'MAP)
	appliquée à la couche atd16:geo_zone_gestion_cdc (Geoserver) ; Espaces verts\EPCI\Texte\Durée planifiée annuelle estimée (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>zone_gestion</se:Name>
        <UserStyle>
            <se:Name>zone_gestion</se:Name>
            <se:FeatureTypeStyle>
        		<se:Rule>
          			<se:TextSymbolizer>
						<se:Label>
                          	<ogc:PropertyName>tps_planifie_estime</ogc:PropertyName> heure(s)
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">15</se:SvgParameter>
         				</se:Font>
                      	<se:LabelPlacement>
                        	<se:PointPlacement>
                      			<se:AnchorPoint>
  									<se:AnchorPointX>0.5</se:AnchorPointX>
  									<se:AnchorPointY>0.5</se:AnchorPointY>
								</se:AnchorPoint>
              					<se:Displacement>
                					<se:DisplacementX>0</se:DisplacementX>
                					<se:DisplacementY>-20</se:DisplacementY>
              					</se:Displacement>
                          	</se:PointPlacement>
                        </se:LabelPlacement>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#0286FF</se:SvgParameter>
           				</se:Fill>
                      	<se:Graphic>
                          	<se:Mark>
                              	<se:WellKnownName>square</se:WellKnownName>
             					<se:Fill>
               						<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
               						<se:SvgParameter name="fill-opacity">0.6</se:SvgParameter>
             					</se:Fill>
             					<se:Stroke>
               						<se:SvgParameter name="stroke">#0286FF</se:SvgParameter>
               						<se:SvgParameter name="stroke-width">1</se:SvgParameter>
             					</se:Stroke>
                          	</se:Mark>
                      	</se:Graphic>
            			<se:VendorOption name="graphic-resize">stretch</se:VendorOption>
            			<se:VendorOption name="graphic-margin">5</se:VendorOption>
		   			</se:TextSymbolizer>
        		</se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
