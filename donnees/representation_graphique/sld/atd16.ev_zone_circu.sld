<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des zones de circulation
    appliquée à la couche atd16:geo_zone_circu_com (Geoserver) ; Espaces verts\Inventaire\Commune\Zone de circulation (X'MAP)
	appliquée à la couche atd16:geo_zone_circu_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Zone de circulation (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>zone_circu</se:Name>
        <UserStyle>
            <se:Name>zone_circu</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>zone_circu</se:Name>
                    <se:Description>
                        <se:Title>Zone de circulation</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#676767</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.20</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#676767</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
