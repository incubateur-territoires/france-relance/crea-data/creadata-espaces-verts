<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des ruches
    appliquée à la couche atd16:v_geo_mobilier_faunistique_ruche_com (Geoserver) ; Espaces verts\Commune\Inventaire\Ruche (X'MAP)
    appliquée à la couche atd16:v_geo_mobilier_faunistique_ruche_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Ruche (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>ruche</se:Name>
        <UserStyle>
            <se:Name>ruche</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>ruche</se:Name>
                    <se:Description>
                        <se:Title>Ruche</se:Title>
                    </se:Description>
                    <se:PointSymbolizer>
								<se:Graphic>
                                  	<se:ExternalGraphic>
                                    	<se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/espaces_verts/abeille5.png"/>
                                      	<se:Format>image/png</se:Format>
                                    </se:ExternalGraphic>
									<se:Size>40</se:Size>
								</se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
