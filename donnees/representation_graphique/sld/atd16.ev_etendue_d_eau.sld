<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des étendues d'eau
    appliquée à la couche atd16:geo_etendue_d_eau_com (Geoserver) ; Espaces verts\Inventaire\Commune\Étendue d'eau (X'MAP)
	appliquée à la couche atd16:geo_etendue_d_eau_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Étendue d'eau (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>etendue_d_eau</se:Name>
        <UserStyle>
            <se:Name>etendue_d_eau</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>etendue_d_eau</se:Name>
                    <se:Description>
                        <se:Title>Étendue d'eau</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#2188EE</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.80</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#2188EE</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">0.8</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
