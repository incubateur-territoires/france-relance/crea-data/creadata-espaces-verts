<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des interventions sans durée (surf)
    appliquée à la couche atd16:v_geo_alert_interv_sans_duree_h_zone_gestion_com (Geoserver) ; Espaces verts\Commune\Alertes (X'MAP)
	appliquée à la couche atd16:v_geo_alert_interv_sans_duree_h_zone_gestion_com (Geoserver) ; Espaces vertse\EPCI\Alertes (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>interv_sans_duree_h</se:Name>
        <UserStyle>
            <se:Name>interv_sans_duree_h</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>interv_sans_duree_h</se:Name>
                    <se:Description>
                        <se:Title>Intervention passée sans durée</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#640001</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.5</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#640001</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">3</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">1</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
