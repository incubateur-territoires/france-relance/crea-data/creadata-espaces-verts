<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des fleuris isolés
    appliquée à la couche atd16:geo_fleuri_isole_com (Geoserver) ; Espaces verts\Inventaire\Commune\Fleuri isolé (X'MAP)
    appliquée à la couche atd16:geo_fleuri_isole_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Fleuri isolé (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>fleuri_isole</se:Name>
        <UserStyle>
            <se:Name>fleuri_isole</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>fleuri_isole</se:Name>
                    <se:Description>
                        <se:Title>Fleuri isolé</se:Title>
                    </se:Description>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>star</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#EE8E21</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#EE8E21</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>12</se:Size>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#E1EE21</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#E1EE21</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>6</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>1</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
