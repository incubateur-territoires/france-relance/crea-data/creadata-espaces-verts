<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des interventions en cours (surf)
    appliquée à la couche atd16:v_geo_zone_gestion_com_entretien_en_cours (Geoserver) ; Espaces verts\Intervention\Intervention en cours (X'MAP)
	appliquée à la couche atd16:v_geo_zone_gestion_cdc_entretien_en_cours (Geoserver) ; Espaces verts\Intervention\Intervention en cours (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>interv_en_cours</se:Name>
        <UserStyle>
            <se:Name>interv_en_cours</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>interv_en_cours</se:Name>
                    <se:Description>
                        <se:Title>Intervention en cours</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#DA235B</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.1</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#DA235B</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">3</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">1</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
        		<se:Rule>
          			<se:TextSymbolizer>
						<se:Label>
							<ogc:PropertyName>label_type_entretien</ogc:PropertyName>
						</se:Label>
            			<se:Font>
           					<se:SvgParameter name="font-size">15</se:SvgParameter>
         				</se:Font>
          				<se:LabelPlacement>
            				<se:PointPlacement>
              					<se:AnchorPoint>
                					<se:AnchorPointX>0.5</se:AnchorPointX>
                					<se:AnchorPointY>0.0</se:AnchorPointY>
              					</se:AnchorPoint>
              					<se:Displacement>
                					<se:DisplacementX>0</se:DisplacementX>
                					<se:DisplacementY>0</se:DisplacementY>
              					</se:Displacement>
            				</se:PointPlacement>
          				</se:LabelPlacement>
            			<se:Halo>
           					<se:Radius><ogc:Literal>1.5</ogc:Literal></se:Radius>
           					<se:Fill>
             					<se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
           					</se:Fill>
         				</se:Halo>
           				<se:Fill>
             				<se:SvgParameter name="fill">#DA235B</se:SvgParameter>
           				</se:Fill>
		   			</se:TextSymbolizer>
        		</se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
