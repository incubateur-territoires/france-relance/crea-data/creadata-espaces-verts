<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des espaces de loisirs
    appliquée à la couche atd16:geo_espace_loisir_com (Geoserver) ; Espaces verts\Inventaire\Commune\Espace de loisirs (X'MAP)
	appliquée à la couche atd16:geo_espace_loisir_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Espace de loisirs (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>espace_loisir</se:Name>
        <UserStyle>
            <se:Name>espace_loisir</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>espace_loisir</se:Name>
                    <se:Description>
                        <se:Title>Espace de loisirs</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#EE4321</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.20</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#EE4321</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
