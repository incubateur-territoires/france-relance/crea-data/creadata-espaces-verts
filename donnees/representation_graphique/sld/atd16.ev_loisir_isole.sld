<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des loisirs isolés
    appliquée à la couche atd16:geo_loisir_isole_com (Geoserver) ; Espaces verts\Inventaire\Commune\Loisir isolé (X'MAP)
    appliquée à la couche atd16:geo_loisir_isole_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Loisir isolé (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>loisir_isole</se:Name>
        <UserStyle>
            <se:Name>loisir_isole</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>loisir_isole</se:Name>
                    <se:Description>
                        <se:Title>Loisir isolé</se:Title>
                    </se:Description>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>star</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#EE4321</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>16</se:Size>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
