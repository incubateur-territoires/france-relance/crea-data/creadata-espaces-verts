<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des alignements d'arbres
    appliquée à la couche atd16:geo_alignement_arbres_com (Geoserver) ; Espaces verts\Inventaire\Commune\Alignement d'arbres (X'MAP)
    appliquée à la couche atd16:geo_alignement_arbres_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Alignement d'arbres (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>alignement_arbres</se:Name>
        <UserStyle>
            <se:Name>alignement_arbres</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>alignement_arbres</se:Name>
                    <se:Description>
                        <se:Title>Alignement d'arbres</se:Title>
                    </se:Description>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">3</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>palmette</se:Name>
                    <se:Description>
                        <se:Title>Palmette</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>type_alignement_arbres</ogc:PropertyName>
                            <ogc:Literal>5</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#f5f50a</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">6</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                          	<se:SvgParameter name="stroke-dasharray">10 20</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
