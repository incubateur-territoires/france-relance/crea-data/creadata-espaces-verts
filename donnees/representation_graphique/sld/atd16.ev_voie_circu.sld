<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des voies de circulation
    appliquée à la couche atd16:geo_voie_circu_com (Geoserver) ; Espaces verts\Inventaire\Commune\Voie de circulation (X'MAP)
    appliquée à la couche atd16:geo_cours_d_eau_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Voie de circulation (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>voie_circu</se:Name>
        <UserStyle>
            <se:Name>voie_circu</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>voie_circu</se:Name>
                    <se:Description>
                        <se:Title>Voie de circulation</se:Title>
                    </se:Description>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#676767</se:SvgParameter>
                          	<se:SvgParameter name="stroke-opacity">0.4</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">3</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
