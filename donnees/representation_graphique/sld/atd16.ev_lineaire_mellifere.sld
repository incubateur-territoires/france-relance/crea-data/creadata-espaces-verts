<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des linéaires mellifères
    appliquée à la couche atd16:v_geo_alignement_arbres_mellifere_com (Geoserver) ; Espaces verts\Commune\Inventaire\Flore mellifère\Alignement d'arbres (X'MAP)
    appliquée à la couche atd16:v_geo_alignement_arbres_mellifere_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Flore mellifère\Alignement d'arbres (X'MAP)
    appliquée à la couche atd16:v_geo_haie_mellifere_com (Geoserver) ; Espaces verts\Commune\Inventaire\Flore mellifère\Haie (X'MAP)
    appliquée à la couche atd16:v_geo_haie_mellifere_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Flore mellifère\Haie (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>lineaire_mellifere</se:Name>
        <UserStyle>
            <se:Name>lineaire_mellifere</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>lineaire_mellifere</se:Name>
                    <se:Description>
                        <se:Title>Flore mellifère</se:Title>
                    </se:Description>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#EE8E21</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">3</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                          	<se:SvgParameter name="stroke-dasharray">10 20</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
