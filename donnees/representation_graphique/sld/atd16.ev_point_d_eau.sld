<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des points d'eau
    appliquée à la couche atd16:geo_point_d_eau_com (Geoserver) ; Espaces verts\Inventaire\Commune\Point d'eau (X'MAP)
    appliquée à la couche atd16:geo_point_d_eau_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Point d'eau (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>point_d_eau</se:Name>
        <UserStyle>
            <se:Name>point_d_eau</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>point_d_eau</se:Name>
                    <se:Description>
                        <se:Title>Point d'eau</se:Title>
                    </se:Description>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#2188EE</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#2188EE</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>16</se:Size>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
