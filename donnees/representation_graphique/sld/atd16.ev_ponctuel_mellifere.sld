<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des ponctuels d'espaces vert mellifères
    appliquée à la couche atd16:geo_fleuri_isole_com (Geoserver) ; Espaces verts\Commune\Inventaire\Flore mellifère\Fleuri isolé (X'MAP)
    appliquée à la couche atd16:geo_fleuri_isole_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Flore Mellifère\Fleuri isolé (X'MAP)
    appliquée à la couche atd16:geo_arbuste_isole_com (Geoserver) ; Espaces verts\Commune\Inventaire\Flore mellifère\Arbuste isolé (X'MAP)
    appliquée à la couche atd16:geo_arbuste_isole_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Flore Mellifère\Arbuste isolé (X'MAP)
    appliquée à la couche atd16:geo_arbre_isole_com (Geoserver) ; Espaces verts\Commune\Inventaire\Flore mellifère\Arbre isolé (X'MAP)
    appliquée à la couche atd16:geo_arbre_isole_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Flore Mellifère\Arbre isolé (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>ponctuel_mellifere</se:Name>
        <UserStyle>
            <se:Name>ponctuel_mellifere</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>ponctuel_mellifere</se:Name>
                    <se:Description>
                        <se:Title>Flore mellifère</se:Title>
                    </se:Description>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#EE8E21</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#E1EE21</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>8</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>0</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
