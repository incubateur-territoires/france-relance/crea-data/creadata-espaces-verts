<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des zones de gestions des espaces vert
    appliquée à la couche atd16:geo_zone_gestion_com (Geoserver) ; Espaces verts\Commune\Thématique\Type de zone de gestion (X'MAP)
	appliquée à la couche atd16:geo_zone_gestion_cdc (Geoserver) ; Espaces verts\EPCI\Thématique\Type de zone de gestion (X'MAP)
-->


<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>Zone de gestion</se:Name>
        <UserStyle>
            <se:Name>Zone de gestion</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Description>
                        <se:Title>Parc, jardin, square</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type_site</ogc:PropertyName>
              				<ogc:Literal>1</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#A8E460</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#A8E460</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Description>
                        <se:Title>Enceinte sportive</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type_site</ogc:PropertyName>
              				<ogc:Literal>6</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#FF8700</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#FF8700</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Description>
                        <se:Title>Cimetière</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type_site</ogc:PropertyName>
              				<ogc:Literal>7</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#1E1E1E</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#1E1E1E</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Description>
                        <se:Title>Espace naturel aménagé</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type_site</ogc:PropertyName>
              				<ogc:Literal>11</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#17630B</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#17630B</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Description>
                        <se:Title>Accompagnement de bâtiments publics</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type_site</ogc:PropertyName>
              				<ogc:Literal>3</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#0059FF</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#0059FF</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Description>
                        <se:Title>Accompagnement d'habitations</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type_site</ogc:PropertyName>
              				<ogc:Literal>4</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#FFBB00</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#FFBB00</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Description>
                        <se:Title>Accompagnement d'établissements industriels et commerciaux</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type_site</ogc:PropertyName>
              				<ogc:Literal>5</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#BE5BFF</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#BE5BFF</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Description>
                        <se:Title>Non renseigné</se:Title>
                    </se:Description>
                  	<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            			<ogc:PropertyIsEqualTo>
              				<ogc:PropertyName>type_site</ogc:PropertyName>
              				<ogc:Literal>0</ogc:Literal>
            			</ogc:PropertyIsEqualTo>
          			</ogc:Filter>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#A8A8A8</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#A8A8A8</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
      		</se:FeatureTypeStyle>
    	</UserStyle>
  	</NamedLayer>
</StyledLayerDescriptor>
