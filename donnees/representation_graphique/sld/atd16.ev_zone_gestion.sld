<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des zones de gestions des espaces vert
    appliquée à la couche atd16:geo_massif_fleuri_com (Geoserver) ; Espaces verts\Inventaire\Commune\Zone de gestion (X'MAP)
	appliquée à la couche atd16:geo_massif_fleuri_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Zone de gestion (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>Zone de gestion</se:Name>
        <UserStyle>
            <se:Name>Zone de gestion</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Description>
                        <se:Title>Zone de gestion</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:GraphicFill>
                                <se:Graphic>
                                    <se:Mark>
                                        <se:WellKnownName>shape://vertline</se:WellKnownName>
                                        <se:Stroke>
                                            <se:SvgParameter name="stroke">#17630B</se:SvgParameter>
                                          	<se:SvgParameter name="stroke-opacity">0.2</se:SvgParameter>
                                        </se:Stroke>
                                    </se:Mark>
                                </se:Graphic>
                            </se:GraphicFill>
                        </se:Fill>
                    </se:PolygonSymbolizer>
                    <se:PolygonSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#17630B</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
