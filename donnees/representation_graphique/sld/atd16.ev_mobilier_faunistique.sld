<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des mobiliers faunistiques
    appliquée à la couche atd16:geo_mobilier_faunistique_com (Geoserver) ; Espaces verts\Commune\Inventaire\Mobilier faunistique (X'MAP)
    appliquée à la couche atd16:geo_mobilier_faunistique_cdc (Geoserver) ; Espaces verts\Intercommunale\Inventaire\Mobilier faunistique (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>mobilier_faunistique</se:Name>
        <UserStyle>
            <se:Name>mobilier_faunistique</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>mobilier_faunistique</se:Name>
                    <se:Description>
                        <se:Title>Mobilier faunistique</se:Title>
                    </se:Description>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>square</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#d5b889</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#744700</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>20</se:Size>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
