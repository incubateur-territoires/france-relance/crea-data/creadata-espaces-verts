<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des arbres isolés
    appliquée à la couche atd16:geo_arbre_isole_com (Geoserver) ; Espaces verts\Inventaire\Commune\Arbre isolé (X'MAP)
    appliquée à la couche atd16:geo_arbre_isole_cdc (Geoserver) ; Espaces verts\Inventaire\Intercommunale\Arbre isolé (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>arbre_isole</se:Name>
        <UserStyle>
            <se:Name>arbre_isole</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>haute_tige</se:Name>
                    <se:Description>
                        <se:Title>Haute-tige et autres</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                      	<ogc:Or>
                        	<ogc:PropertyIsEqualTo>
                            	<ogc:PropertyName>type_forme_arbre_fr</ogc:PropertyName>
                            	<ogc:Literal>1</ogc:Literal>
                        	</ogc:PropertyIsEqualTo>
                          	<ogc:PropertyIsEqualTo>
                            	<ogc:PropertyName>type_forme_arbre_fr</ogc:PropertyName>
                            	<ogc:Literal>99</ogc:Literal>
                        	</ogc:PropertyIsEqualTo>
                          	<ogc:PropertyIsNull>
                            	<ogc:PropertyName>type_forme_arbre_fr</ogc:PropertyName>
                        	</ogc:PropertyIsNull>
                      	</ogc:Or>
                    </ogc:Filter>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>24</se:Size>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>star</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>32</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>-1</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>star</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>32</se:Size>
                          	<se:Rotation>180</se:Rotation>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>1</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>demi_tige</se:Name>
                    <se:Description>
                        <se:Title>Demi-tige</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>type_forme_arbre_fr</ogc:PropertyName>
                            <ogc:Literal>2</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>20</se:Size>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>star</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>26</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>-1</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>star</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>26</se:Size>
                          	<se:Rotation>180</se:Rotation>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>1</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>basse_tige</se:Name>
                    <se:Description>
                        <se:Title>Basse-tige</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>type_forme_arbre_fr</ogc:PropertyName>
                            <ogc:Literal>3</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>16</se:Size>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>star</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>20</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>-1</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>star</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#256f00</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#256f00</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>20</se:Size>
                          	<se:Rotation>180</se:Rotation>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>1</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>arbre_fruitier</se:Name>
                    <se:Description>
                        <se:Title>Arbre fruitier</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>type_arbre</ogc:PropertyName>
                            <ogc:Literal>2</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#ff0000</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#ff0000</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>3</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>8</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#ff0000</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#ff0000</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>3</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>-8</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#ff0000</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#ff0000</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>3</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>8</se:DisplacementX>
                              	<se:DisplacementY>0</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#ff0000</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#ff0000</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>3</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>-8</se:DisplacementX>
                              	<se:DisplacementY>0</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>arbre_ornement</se:Name>
                    <se:Description>
                        <se:Title>Arbre d'ornement</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>type_arbre</ogc:PropertyName>
                            <ogc:Literal>1</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <se:PointSymbolizer>
                        <se:Graphic>
                            <se:Mark>
                                <se:WellKnownName>circle</se:WellKnownName>
                                <se:Fill>
                                    <se:SvgParameter name="fill">#0000ff</se:SvgParameter>
                                </se:Fill>
                                <se:Stroke>
                                    <se:SvgParameter name="stroke">#0000ff</se:SvgParameter>
                                </se:Stroke>
                            </se:Mark>
                            <se:Size>12</se:Size>
                          	<se:Displacement>
                              	<se:DisplacementX>0</se:DisplacementX>
                              	<se:DisplacementY>0</se:DisplacementY>
                          	</se:Displacement>
                        </se:Graphic>
                    </se:PointSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
