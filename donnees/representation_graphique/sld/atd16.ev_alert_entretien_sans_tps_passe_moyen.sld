<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des entretiens sans temps passé moyen (surf)
    appliquée à la couche atd16:v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com (Geoserver) ; Espaces verts\Commune\Alertes (X'MAP)
	appliquée à la couche atd16:v_geo_alert_entretien_sans_tps_passe_moyen_zone_gestion_com (Geoserver) ; Espaces vertse\EPCI\Alertes (X'MAP)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>entretien_sans_tps_passe_moyen</se:Name>
        <UserStyle>
            <se:Name>entretien_sans_tps_passe_moyen</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>entretien_sans_tps_passe_moyen</se:Name>
                    <se:Description>
                        <se:Title>Entretien sans durée moyenne</se:Title>
                    </se:Description>
                    <se:PolygonSymbolizer>
                        <se:Fill>
                            <se:SvgParameter name="fill">#676767</se:SvgParameter>
                            <se:SvgParameter name="fill-opacity">0.5</se:SvgParameter>
                        </se:Fill>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#676767</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">3</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-opacity">1</se:SvgParameter>
                        </se:Stroke>
                    </se:PolygonSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
